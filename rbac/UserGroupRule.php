<?php
namespace app\rbac;

use Yii;
use yii\rbac\Rule;
use app\models\User;

/**
 * Checks if user group matches
 */
class UserGroupRule extends Rule
{
    public $name = 'userGroup';

    public function execute($user, $item, $params)
    {
        if (!Yii::$app->user->isGuest) {
            $group = Yii::$app->user->identity->group;
            if ($item->name === 'admin') {
                return $group == User::GROUP_ADMIN;
            } elseif ($item->name === 'jury') {
                return $group == User::GROUP_JURY;
            } elseif ($item->name === 'manager') {
                return $group == User::GROUP_ADMIN || $group == User::GROUP_MANAGER;
            } elseif ($item->name === 'participant') {
                return $group == User::GROUP_PARTICIPANT;
            }
        }
        return false;
    }
}