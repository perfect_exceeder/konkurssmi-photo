/**
 * Created by h8every1 on 01.07.2016.
 */
jQuery(document).ready(function ($) {
	$('.image-link').magnificPopup({
		type: 'image',
		gallery: {
			enabled: true
		}
	});
});