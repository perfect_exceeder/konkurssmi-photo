/**
 * Created by h8every1 on 19.06.2015.
 */
yii.validation2 = (function ($) {
	var pub = {
		imgDim: function (attribute, messages, options, deferred) {
			var files = getUploadedFiles(attribute, messages, options);

			$.each(files, function (i, file) {

				// Skip image validation if FileReader API is not available
				if (typeof FileReader === "undefined") {
					return;
				}

				var def = $.Deferred(),
					fr = new FileReader(),
					img = new Image();

				img.onload = function () {
					if (options.minDimension && (this.width < options.minDimension || this.height < options.minDimension)) {
						messages.push(options.underDimension.replace(/\{file\}/g, file.name));
					}

					if (options.maxDimension && (this.width > options.maxDimension || this.height > options.maxDimension)) {
						messages.push(options.overDimension.replace(/\{file\}/g, file.name));
					}
					def.resolve();
				};

				fr.onload = function () {
					img.src = fr.result;
				};

				// Resolve deferred if there was error while reading data
				fr.onerror = function () {
					def.resolve();
				};

				fr.readAsDataURL(file);

				deferred.push(def);
			});

		}
	};

	function getUploadedFiles(attribute, messages, options) {
		// Skip validation if File API is not available
		if (typeof File === "undefined") {
			return [];
		}

		var files = $(attribute.input).get(0).files;
		if (!files) {
			messages.push(options.message);
			return [];
		}

		if (files.length === 0) {
			if (!options.skipOnEmpty) {
				messages.push(options.uploadRequired);
			}
			return [];
		}

		if (options.maxFiles && options.maxFiles < files.length) {
			messages.push(options.tooMany);
			return [];
		}

		return files;
	}

	return pub;
})(jQuery);