<?php

use yii\db\Schema;
use yii\db\Migration;

class m151005_204847_add_article_image extends Migration
{
    public function up()
    {
        $this->addColumn( '{{%article}}', 'image', Schema::TYPE_STRING . '(36)' );
    }

    public function down()
    {
        $this->dropColumn( '{{%article}}', 'image' );
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
