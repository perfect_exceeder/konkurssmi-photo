<?php

use yii\db\Schema;
use yii\db\Migration;

class m150723_084419_article_add_excerpt_field extends Migration
{
    public function up()
    {
        $this->addColumn('{{%article}}','excerpt',Schema::TYPE_TEXT);
    }

    public function down()
    {
        $this->dropColumn('{{%article}}','excerpt');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
