<?php

use yii\db\Schema;
use yii\db\Migration;

class m150716_225423_file_download_url extends Migration
{
    public function up()
    {

        $this->addColumn('{{%work}}', 'download_url', Schema::TYPE_STRING.'(255)');
    }

    public function down()
    {
        $this->dropColumn('{{%work}}', 'download_url');

        return true;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
