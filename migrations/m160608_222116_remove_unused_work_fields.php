<?php

use yii\db\Migration;
use yii\db\Schema;

class m160608_222116_remove_unused_work_fields extends Migration
{
    public function up()
    {
        $this->dropColumn('{{%work}}', 'file_conception');
        $this->dropColumn('{{%work}}', 'file_schedule');
        $this->dropColumn('{{%work}}', 'date');
        $this->dropColumn('{{%work}}', 'download_url');
        $this->dropColumn('{{%work}}', 'on_remote');
        $this->dropColumn('{{%work}}', 'remote_id');
    }

    public function down()
    {
        $this->addColumn( '{{%work}}', 'remote_id', Schema::TYPE_STRING . '(50)' );
        $this->addColumn( '{{%work}}', 'on_remote', Schema::TYPE_BOOLEAN . ' DEFAULT 0' );
        $this->addColumn( '{{%work}}', 'download_url', Schema::TYPE_STRING . '(255)' );
        $this->addColumn( '{{%work}}', 'date', Schema::TYPE_DATE . ' NOT NULL' );
        $this->addColumn( '{{%work}}', 'file_conception', Schema::TYPE_STRING . '(36)' );
        $this->addColumn( '{{%work}}', 'file_schedule', Schema::TYPE_STRING . '(36)' );

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
