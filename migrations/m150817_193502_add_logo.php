<?php

use yii\db\Schema;
use yii\db\Migration;

class m150817_193502_add_logo extends Migration
{
    public function up()
    {
        $this->addColumn( '{{%work}}', 'logo', Schema::TYPE_STRING . '(36)' );
    }

    public function down()
    {
        $this->dropColumn( '{{%work}}', 'logo' );
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
