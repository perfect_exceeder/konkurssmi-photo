<?php

use yii\db\Schema;
use yii\db\Migration;
use app\models\User;

class m150603_205753_initial extends Migration {
    public function safeUp() {

        $tableOptions = null;
        if ( $this->db->driverName === 'mysql' ) {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable( '{{%work_category}}', [
            'id'              => Schema::TYPE_PK,
            'title'           => Schema::TYPE_STRING . '(64) NOT NULL',
            'max_file_size'   => Schema::TYPE_INTEGER . '(6) NULL',
            'file_extensions' => Schema::TYPE_TEXT . ' NULL',
            'file_text'       => Schema::TYPE_TEXT . ' NULL',
        ], $tableOptions );

        $this->batchInsert( '{{%work_category}}', [ 'title', 'max_file_size', 'file_extensions', 'file_text' ],
            [
                [ 'Печатные издания', 60, 'pdf,rtf,doc,docx', '' ],
                [
                    'Радио',
                    200,
                    'mp3, wav',
                    'Битрейт – не ниже 128 Кбит/с.
Продолжительность – не более 60 минут.'
                ],
                [
                    'Телевидение',
                    600,
                    'avi,mpg,mp4,wmv',
                    'Качество звука – не менее 128 Кбит/с,
Качество видео – не более 1200 Кбит/с,
Продолжительность – не более 60 минут.'
                ],
            ] );


        $this->createTable( '{{%user}}', [
            'id'                   => Schema::TYPE_PK,
            'auth_key'             => Schema::TYPE_STRING . '(32) NOT NULL',
            'password_hash'        => Schema::TYPE_STRING . ' NOT NULL',
            'password_reset_token' => Schema::TYPE_STRING,
            'activation_key'       => Schema::TYPE_STRING . '(32)',
            'email'                => Schema::TYPE_STRING . ' NOT NULL',
            'status'               => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT ' . User::STATUS_INACTIVE,
            'group'                => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT ' . User::GROUP_PARTICIPANT,
            'created_at'           => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at'           => Schema::TYPE_INTEGER . ' NOT NULL',
        ], $tableOptions );

        $this->insert('{{%user}}',[
            'password_hash' => Yii::$app->security->generatePasswordHash( '123456' ),
            'auth_key' => Yii::$app->security->generateRandomString(),
            'email' => 'anton@elustro.ru',
            'status' => User::STATUS_ACTIVE,
            'group' => User::GROUP_ADMIN,
            'created_at' => time(),
            'updated_at' => time(),
        ]);

        $this->createTable( '{{%konkurs}}', [
            'id'          => Schema::TYPE_PK,
            'title'       => Schema::TYPE_TEXT.' NOT NULL',
            'description' => Schema::TYPE_TEXT,
        ], $tableOptions );

        $this->batchInsert( '{{%konkurs}}', [ 'title' ],
            [
                [ '2014 год' ],
            ] );

        $this->createTable( '{{%participant}}', [
            'id'              => Schema::TYPE_PK,
            'user_id'         => Schema::TYPE_INTEGER . ' NOT NULL',
            'konkurs_id'      => Schema::TYPE_INTEGER . ' NOT NULL',
            'type'            => Schema::TYPE_SMALLINT . '(1) NOT NULL',
            'name'            => Schema::TYPE_STRING . '(255) NOT NULL',
            'smi_name'        => Schema::TYPE_STRING . '(255) NOT NULL',
            'smi_type'        => Schema::TYPE_SMALLINT . '(2) NOT NULL',
            'smi_field'       => Schema::TYPE_STRING . '(255) NULL',
            'smi_url'         => Schema::TYPE_STRING . '(255) NOT NULL',
            'smi_editor'      => Schema::TYPE_STRING . '(255) NULL',
            'smi_head'        => Schema::TYPE_STRING . '(255) NULL',
            'smi_periodity'   => Schema::TYPE_STRING . '(255) NULL',
            'smi_area'        => Schema::TYPE_TEXT . ' NULL',
            'smi_status'      => Schema::TYPE_STRING . '(255) NULL',
            'smi_jur_address' => Schema::TYPE_TEXT . ' NULL',
            'occupation'      => Schema::TYPE_STRING . '(255) NOT NULL',
            'post_address'    => Schema::TYPE_TEXT . ' NOT NULL',
            'city_phone'      => Schema::TYPE_STRING . '(50) NOT NULL',
            'mobile_phone'    => Schema::TYPE_STRING . '(50) NOT NULL',
            'add_time'        => Schema::TYPE_INTEGER . ' NOT NULL',
        ], $tableOptions );
        $this->addForeignKey( 'fk_konkurs_participant', '{{%participant}}', 'konkurs_id', '{{%konkurs}}', 'id',
            'CASCADE', 'CASCADE' );
        $this->addForeignKey( 'fk_user_participant', '{{%participant}}', 'user_id', '{{%user}}', 'id', 'CASCADE',
            'CASCADE' );


        $this->createTable( '{{%work}}', [
            'id'             => Schema::TYPE_PK,
            'konkurs_id'     => Schema::TYPE_INTEGER . ' NOT NULL',
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'category_id'    => Schema::TYPE_INTEGER . ' NOT NULL',
            'file_name'      => Schema::TYPE_STRING . '(32) NOT NULL',
            'file_type'      => Schema::TYPE_STRING . '(10) NOT NULL',
            'title'          => Schema::TYPE_TEXT . ' NOT NULL',
            'date'           => Schema::TYPE_DATE . ' NOT NULL',
            'picture'        => Schema::TYPE_STRING . '(36) NOT NULL',
        ], $tableOptions );
        $this->addForeignKey( 'fk_work_category_work', '{{%work}}', 'category_id', '{{%work_category}}', 'id',
            'CASCADE', 'CASCADE' );
        $this->addForeignKey( 'fk_konkurs_work', '{{%work}}', 'konkurs_id', '{{%konkurs}}', 'id', 'CASCADE',
            'CASCADE' );
        $this->addForeignKey( 'fk_user_work', '{{%work}}', 'user_id', '{{%user}}', 'id', 'CASCADE',
            'CASCADE' );


        $this->createTable( '{{%mark_category}}', [
            'id'   => Schema::TYPE_PK,
            'name' => Schema::TYPE_TEXT . ' NOT NULL',
            'max'  => Schema::TYPE_INTEGER . '(3) NOT NULL DEFAULT 10',
        ], $tableOptions );

        $this->batchInsert( '{{%mark_category}}', [ 'name' ],
            [
                [ 'Актуальность материалов, их соответствие основным направлениям развития и модернизации системы профессионального образования в России' ],
                [ 'Достоверность и информационная насыщенность' ],
                [ 'Соответствие стиля и формы подачи материала её целевым аудиториям' ],
                [ 'Глубина раскрытия темы' ],
                [ 'Оригинальность и выразительность подачи материала' ],
            ] );

        $this->createTable( '{{%jury}}', [
            'id'         => Schema::TYPE_PK,
            'user_id'    => Schema::TYPE_INTEGER . ' NULL',
            'konkurs_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'name'       => Schema::TYPE_STRING . '(200) NOT NULL',
            'activity'   => Schema::TYPE_STRING . '(200) NULL',
        ], $tableOptions );
        $this->addForeignKey( 'fk_konkurs_jury', '{{%jury}}', 'konkurs_id', '{{%konkurs}}', 'id', 'CASCADE',
            'CASCADE' );
        $this->addForeignKey( 'fk_user_jury', '{{%jury}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE' );

        $this->createTable( '{{%work_mark}}', [
            'work_id'          => Schema::TYPE_INTEGER . ' NOT NULL',
            'user_id'          => Schema::TYPE_INTEGER . ' NOT NULL',
            'mark_category_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            0                  => 'PRIMARY KEY (`work_id`, `user_id`, `mark_category_id`)',
            'mark'             => Schema::TYPE_INTEGER . ' NOT NULL',
        ], $tableOptions );
        $this->addForeignKey( 'fk_user_work_mark', '{{%work_mark}}', 'user_id', '{{%user}}', 'id', 'CASCADE',
            'CASCADE' );
        $this->addForeignKey( 'fk_mark_category_work_mark', '{{%work_mark}}', 'mark_category_id', '{{%mark_category}}',
            'id', 'CASCADE', 'CASCADE' );
        $this->addForeignKey( 'fk_work_work_mark', '{{%work_mark}}', 'work_id', '{{%work}}', 'id', 'CASCADE',
            'CASCADE' );

        $this->createTable( '{{%jury_category}}', [
            'jury_id'     => Schema::TYPE_INTEGER . ' NOT NULL',
            'category_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'PRIMARY KEY (`jury_id`, `category_id`)',
        ], $tableOptions );
        $this->addForeignKey( 'fk_jury_jury_category', '{{%jury_category}}', 'jury_id', '{{%jury}}', 'id', 'CASCADE',
            'CASCADE' );
        $this->addForeignKey( 'fk_work_category_jury_category', '{{%jury_category}}', 'category_id',
            '{{%work_category}}', 'id', 'CASCADE', 'CASCADE' );


        $this->createTable( '{{%article}}', [
            'id'         => Schema::TYPE_PK,
            'title'      => Schema::TYPE_TEXT . ' NOT NULL',
            'slug'        => Schema::TYPE_STRING . '(150) NOT NULL',
            'content'    => Schema::TYPE_TEXT . ' NOT NULL',
            'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
            'created_by' => Schema::TYPE_INTEGER . ' NOT NULL',
            'category' => Schema::TYPE_INTEGER .' DEFAULT 0',
        ], $tableOptions );
        $this->addForeignKey( 'fk_user_article', '{{%article}}', 'created_by', '{{%user}}', 'id', 'CASCADE', 'CASCADE' );
    }

    public function safeDown() {

        $this->dropTable( '{{%article}}' );
        $this->dropTable( '{{%jury_category}}' );
        $this->dropTable( '{{%work_mark}}' );
        $this->dropTable( '{{%jury}}' );
        $this->dropTable( '{{%mark_category}}' );
        $this->dropTable( '{{%work}}' );
        $this->dropTable( '{{%participant}}' );
        $this->dropTable( '{{%konkurs}}' );
        $this->dropTable( '{{%user}}' );
        $this->dropTable( '{{%work_category}}' );

    }

}
