<?php

use yii\db\Schema;
use yii\db\Migration;

class m150723_090011_add_work_mark_category extends Migration
{
    public function up()
    {
        $this->addColumn('{{%mark_category}}','participant_type',Schema::TYPE_SMALLINT . '(1) NOT NULL DEFAULT '.\app\models\Participant::TYPE_JOURNALIST);

        $type_add = \app\models\Participant::TYPE_COMPANY;

        $this->batchInsert( '{{%mark_category}}', [ 'name', 'participant_type' ],
            [
                [ 'Актуальность и качество содержания', $type_add ],
                [ 'Жанровое разнообразие', $type_add ],
                [ 'Качество дизайна и внешний вид', $type_add ],
                [ 'Оригинальность иллюстраций', $type_add ],
                [ 'Качество верстки / монтажа', $type_add ],
            ] );
    }

    public function down()
    {

        $this->dropColumn('{{%mark_category}}','participant_type');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
