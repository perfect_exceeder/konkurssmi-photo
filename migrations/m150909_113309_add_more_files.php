<?php

use yii\db\Schema;
use yii\db\Migration;

class m150909_113309_add_more_files extends Migration {

    public function up() {
        $this->addColumn( '{{%work}}', 'file_conception', Schema::TYPE_STRING . '(36)' );
        $this->addColumn( '{{%work}}', 'file_schedule', Schema::TYPE_STRING . '(36)' );
    }

    public function down() {
        $this->dropColumn( '{{%work}}', 'file_conception' );
        $this->dropColumn( '{{%work}}', 'file_schedule' );
//        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
