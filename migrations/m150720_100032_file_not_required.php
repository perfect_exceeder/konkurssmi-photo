<?php

use yii\db\Schema;
use yii\db\Migration;

class m150720_100032_file_not_required extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%work}}', 'file_name', Schema::TYPE_STRING.'(32)');
        $this->alterColumn('{{%work}}', 'file_type', Schema::TYPE_STRING.'(10)');
        $this->alterColumn('{{%work}}', 'date', Schema::TYPE_DATE);
    }

    public function down()
    {
        echo "m150720_100032_file_not_required cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
