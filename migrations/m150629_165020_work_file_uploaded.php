<?php

use yii\db\Schema;
use yii\db\Migration;

class m150629_165020_work_file_uploaded extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ( $this->db->driverName === 'mysql' ) {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->addColumn('{{%work}}', 'on_remote', Schema::TYPE_BOOLEAN.' DEFAULT 0');
        $this->addColumn('{{%work}}', 'remote_id', Schema::TYPE_STRING.'(50)');

    }

    public function down()
    {
        $this->dropColumn('{{%work}}', 'on_remote');
        $this->dropColumn('{{%work}}', 'remote_id');

        return true;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
