<?php

use yii\db\Schema;
use yii\db\Migration;

class m150817_215659_unrequire_picture extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%work}}', 'picture', Schema::TYPE_STRING.'(36)');
    }

    public function down()
    {
        echo "m150817_215659_unrequire_picture cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
