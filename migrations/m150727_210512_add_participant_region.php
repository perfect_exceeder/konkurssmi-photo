<?php

use yii\db\Schema;
use yii\db\Migration;

class m150727_210512_add_participant_region extends Migration {
    public function up() {
        $this->addColumn( '{{%participant}}', 'region', Schema::TYPE_STRING . '(4)' );
    }

    public function down() {
        $this->dropColumn( '{{%participant}}', 'region' );
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
