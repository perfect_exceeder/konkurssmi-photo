<?php

use yii\db\Schema;
use yii\db\Migration;

class m150720_120536_delete_work_category extends Migration
{
    public function up()
    {
        $this->dropForeignKey('fk_work_category_work','{{%work}}');
        $this->dropColumn('{{%work}}', 'category_id');

    }

    public function down()
    {
        echo "m150720_120536_delete_work_category cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
