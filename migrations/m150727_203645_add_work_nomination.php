<?php

use yii\db\Schema;
use yii\db\Migration;

class m150727_203645_add_work_nomination extends Migration {
    public function up() {
        $this->addColumn( '{{%work}}', 'nomination', Schema::TYPE_INTEGER . '(5)' );
    }

    public function down() {

        $this->dropColumn( '{{%work}}', 'nomination' );
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
