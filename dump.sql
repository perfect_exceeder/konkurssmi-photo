-- MySQL dump 10.14  Distrib 5.5.41-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: web
-- ------------------------------------------------------
-- Server version	5.5.41-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `category` int(11) DEFAULT '0',
  `excerpt` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `fk_user_article` (`created_by`),
  CONSTRAINT `fk_user_article` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article`
--

LOCK TABLES `article` WRITE;
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` VALUES (1,'Положение о конкурсе \"PRO Образование - 2015\"','polozhenie','<h2>Введение</h2>\r\n<p>Данное положение регламентирует порядок проведения Всероссийского конкурса \"PRO Образование - 2015\", его цели и задачи, требования к участникам конкурса, правила и порядок участия, перечень номинаций, критерии и порядок оценки конкурсных работ, порядок определения победителей.\r\n</p>\r\n<h2>Цели и задачи</h2>\r\n<p>Конкурс \"PRO Образование\" проводится по инициативе Министерства образования и науки Российской Федерации с целью стимулирования, сбора и распространения лучшего опыта освещения государственных инициатив, направленных на модернизацию и развитие профессионального образования, а также с целью поддержки и поощрения студенческих СМИ.\r\n</p>\r\n<p>Среди задач, решаемых в ходе проведения конкурса: поддержка коммуникаций с журналистами и СМИ, освещающими тему профессионального образования; повышение мотивации журналистов и СМИ к глубокому и всестороннему освещению актуальных достижений и проблем в данной сфере; популяризация и привлечение молодежи к получению востребованных профессий, выявление, обобщение и популяризация лучших образцов журналистского творчества; повышение внимания и лояльности региональной и местной власти к журналистам и СМИ, включение СМИ в процесс формирования позитивного отношения к труду преподавателей и повышение престижа профессии педагога.\r\n</p>\r\n<p><strong></strong>\r\n</p>\r\n<h2>Правила участия в конкурсе<strong></strong></h2>\r\n<p>Участник имеет право подавать не более 1 конкурсной заявки в каждой номинации. Общее число поданных заявок не может превышать число номинаций конкурса.\r\n</p>\r\n<p>Участники обязаны указывать достоверную и актуальную информацию при регистрации и подаче конкурсной заявки (ФИО, почтовый и электронный адрес, контактные телефоны, название СМИ, название материала и проч.). Указанная информация будет использована в дипломе победителя и не подлежит изменению.\r\n</p>\r\n<p>Конкурсные работы принимаются в цифровом виде через конкурсный портал <a href=\"http://konkurssmi.ru/\">http://konkurssmi.ru</a> Для подачи работы необходимо заполнить регистрационную форму и подать конкурсную заявку, следуя требованиям системы.\r\n</p>\r\n<p>Конкурсные работы не принимаются по электронной почте или любым иным способом, кроме подачи через интернет-портал конкурса.\r\n</p>\r\n<p><strong></strong>\r\n</p>\r\n<h2>Регламент проведения конкурса<strong></strong></h2>\r\n<p>Конкурс проводится заочно в 1 этап. Конкурсные заявки принимаются до 18:00 (МСК) 15 сентября 2015 года. Конкурсные заявки подаются через сайт конкурса <a href=\"http://konkurssmi.ru/\">http://konkurssmi.ru</a>\r\n</p>\r\n<p>К участию в конкурсе допускаются материалы на русском языке, опубликованные / размещенные в СМИ в период с 01 января 2014 года.\r\n</p>\r\n<p>По завершении приема заявок они передаются на рассмотрение и оценку Экспертного совета (жюри) конкурса. Оценка материалов также осуществляется через сайт конкурса <a href=\"http://konkurssmi.ru/\">http://konkurssmi.ru</a>\r\n</p>\r\n<p>К участию в Конкурсе не допускаются научно-методические статьи, разъяснительные комментарии к нормативно-правовым актам, научные авторские исследования, методические разработки, а также издания, ориентированные на публикацию вышеперечисленных материалов.\r\n</p>\r\n<p>В каждой номинации может быть определено несколько победителей в категориях:\r\n</p>\r\n<ul>\r\n	<li>Газета.</li>\r\n	<li>Журнал.</li>\r\n	<li>Интернет-издание.</li>\r\n	<li>Информационное агентство.</li>\r\n	<li>Телевидение.</li>\r\n	<li>Радио.</li>\r\n</ul>\r\n<p><strong></strong>\r\n</p>\r\n<h2>Требования к участникам конкурса<strong></strong></h2>\r\n<p>К участию в конкурсе приглашаются:\r\n</p>\r\n<ul>\r\n	<li>Представители средств массовой информации всех типов, зарегистрированных на территории Российской Федерации.</li>\r\n	<li>Представители информационных изданий образовательных организаций профессионального образования (студенческие СМИ).</li>\r\n	<li>Индивидуальные журналисты.</li>\r\n	<li>Педагоги, публикующие в прессе материалы о своей работе и жизни образовательных учреждений.</li>\r\n	<li>Агентства и некоммерческие организации, разрабатывающие медийный контент, посвященный тематике профессионального образования в России (инфографика, видеоролики). </li>\r\n	<li>СМИ (участник представляет на конкурс результат работы издания)</li>\r\n	<li>Индивидуальные журналисты (участник представляет на конкурс личное творческое произведение, опубликованное в том или ином СМИ)</li>\r\n</ul>\r\n<p>В рамках конкурса участники подразделяются на два типа:\r\n</p>\r\n<h2>Порядок предоставления конкурсных материалов в номинациях для СМИ</h2>\r\n<h3>Печатные СМИ предоставляют:</h3>\r\n<ul>\r\n	<li>Подборку номеров издания в формате PDF, содержащую 3 различных выпуска (номера) издания, выбранных на усмотрение Участника, объединенные в один файл размером не более 20 Мб.</li>\r\n	<li>Адрес сайта СМИ в сети интернет (при его наличии).</li>\r\n</ul>\r\n<h3>Интернет-СМИ предоставляют:</h3>\r\n<ul>\r\n	<li>Адрес СМИ в сети интернет (ссылка).</li>\r\n	<li>Ссылки на отдельные материалы (страницы) ресурса, на которые, по мнению участника Конкурса, следует обратить особое внимание (до 5 ссылок). </li>\r\n	<li>Для интернет-изданий общего характера – ссылки на материалы образовательного характера, размещаемые на сайте (от 3 до 5 материалов).</li>\r\n	<li>Перечень ссылок подается в формате PDF (текстовый, распознаваемый), RTF или DOC.</li>\r\n</ul>\r\n<h3>Телевидение (телеканалы) и радио предоставляют:</h3>\r\n<ul>\r\n	<li>Краткое описание концепции специального проекта (1 страница), соответствующего тематике Конкурса (цели и задачи, основная идея, целевая аудитория, краткое описание реализации) в формате PDF (текстовый, распознаваемый), RTF или DOC.</li>\r\n	<li>Программу передач (сетку вещания) за период, отражающий время и частоту выходов спецпроекта в эфир (не менее 5 выходов) в формате PDF (текстовый, распознаваемый), RTF или DOC. Передачи спецпроекта необходимо выделить в программе любым цветом.</li>\r\n	<li>Одну программу (передачу), входящую в представляемый проект, выбранную участникам Конкурса самостоятельно, в формате AVI, MPEG-1, MPEG-2 или MPEG-4.</li>\r\n</ul>\r\n<p>Файл может быть выложен посредством открытых видеохостингов. В этом случае конкурсный материал должен содержать ссылку на выложенный на стороннем ресурсе файл.\r\n</p>\r\n<h3>Информационные агентства предоставляют:</h3>\r\n<ul>\r\n	<li>Адрес агентства в сети интернет (сайт).</li>\r\n	<li>Ссылки на отдельные материалы: информационные, новостные сообщения, интервью и другие материалы, освещающие тему развития образования, выбранные на усмотрение Участника (от 3 до 5 материалов). </li>\r\n	<li>Перечень ссылок подается в формате PDF (текстовый, распознаваемый), RTF или DOC.</li>\r\n</ul>\r\n<h2>Порядок предоставления конкурсных материалов в номинациях для журналистов</h2>\r\n<p><strong></strong>\r\n</p>\r\n<h3>В категории «Телевидение» «Радио» журналист предоставляет:</h3>\r\n<ul>\r\n	<li>Аудиовидеозапись передачи (программы, сюжета), соответствующую техническим требованиям Конкурса (см. выше), с указанием даты и времени их выхода в эфир и названия телерадиостанции.</li>\r\n	<li>Текст статьи/записи в блоге в формате PDF, (текстовый, распознанный), RTF или DOC, опубликованной в печатных или интернет СМИ, блогах с указанием даты публикаций и названия СМИ/блога (в т.ч. гиперссылки на блог).</li>\r\n	<li>Страницу печатного СМИ в формате PDF или ссылку и скриншот (для интернет СМИ и блогов) с опубликованным материалом в формате RTF или DOC.</li>\r\n	<li>Для категории «Блоги» также необходимо предоставить гиперссылку на статистику посещаемости, отражающую количество подписчиков блога. К конкурсу допускаются материалы, размещенные в блогах с не менее, чем 1 000 подписчиками.</li>\r\n</ul>\r\n<p>Файл может быть выложен посредством открытых видеохостингов. В этом случае конкурсный материал должен содержать ссылку на выложенный на стороннем ресурсе файл.\r\n</p>\r\n<h3>В категориях «Печатные и Интернет-СМИ», а также «Блоги» участник предоставляет:</h3>\r\n<h2>Требования к материалам<strong></strong></h2>\r\n<p>К конкурсным материалам обязательно прилагаются:\r\n</p>\r\n<ul>\r\n	<li>логотип СМИ (для юридических лиц) в формате JPG (размер файла не менее 1 Мбайт, разрешение не менее 100 dpi, размер по длинной стороне – не менее 1000 точек).</li>\r\n	<li>фото автора (для физических лиц) в формате в формате JPG (размер файла не менее 1 Мбайт, разрешение не менее 100 dpi, размер по длинной стороне – не менее 1000 точек).</li>\r\n</ul>\r\n<h3>Текстовые публикации (газеты, журналы, интернет-издания и блоги):</h3>\r\n<ul>\r\n	<li>Формат – PDF (текстовый, распознаваемый), RTF или DOC.</li>\r\n	<li>Размер файла не более 60 Мб.</li>\r\n</ul>\r\n<p>Файл должен содержать скриншот интернет-страницы издания с материалом, либо сканированную полосу печатного издания с колонтитулом, содержащим элементы оформления издания, логотип и дату выхода. В случае, если колонтитул не содержит дату, ее следует указать в тексте.\r\n</p>\r\n<h3>Аудиоматериалы (радиопрограммы):</h3>\r\n<ul>\r\n	<li>Формат – MP3 или WMA.</li>\r\n	<li>Качество звука – от 48 до 128 Кбит/с.+</li>\r\n	<li>Продолжительность – не более 60 минут.</li>\r\n</ul>\r\n<p>Размер файла – не более 150 МБ.\r\n</p>\r\n<p>Файл может быть выложен посредством открытых файлообменных серверов или видеохостингов. В этом случае конкурсный материал должен содержать ссылку на выложенный на стороннем ресурсе файл.\r\n</p>\r\n<h3>Видеоматериалы (телевизионные сюжеты):</h3>\r\n<ul>\r\n	<li>Формат – AVI, MPEG-1, MPEG-2, MPEG-4.</li>\r\n	<li>Продолжительность – не более 60 минут.</li>\r\n	<li>Размер файла – не более 750 Мб,</li>\r\n	<li>Качество звука – не менее 128 Кбит/с,</li>\r\n	<li>Качество видео – не более 1200 Кбит/с,</li>\r\n	<li>Частота кадров 25 кадров/с.</li>\r\n</ul>\r\n<p>Файл может быть выложен посредством открытых видеохостингов. В этом случае конкурсный материал должен содержать ссылку на выложенный на стороннем ресурсе файл.\r\n</p>\r\n<h2>Номинации конкурса<strong></strong></h2>\r\n<h3>Номинации для СМИ<strong></strong>\r\n</h3>\r\n<ol>\r\n	<li>Лучшее издание общего характера, освещающее тему профобразования.</li>\r\n	<li>Лучшее специализированное издание, посвященное образованию.</li>\r\n	<li>Лучшее издание образовательной организации учреждения (НПО /СПО, вуза).</li>\r\n</ol>\r\n<h3>Номинации для журналистов</h3>\r\n<ol>\r\n	<li>Тема. Лучший материал по теме модернизация профессионального образования.</li>\r\n	<li>Видео. Лучший видеосюжет о российском профессиональном образовании.</li>\r\n	<li>Радио. Лучший радиосюжет о российском профессиональном образовании.</li>\r\n	<li>Сеть. Лучший информационный проект для интернет-аудитории.</li>\r\n	<li>Графика. Лучший инфографический материал по теме профобразования</li>\r\n	<li>История успеха. Лучший материал, посвященный студентам и выпускникам учреждений системы профессионального образования, молодым рабочим.</li>\r\n	<li>Практика. Лучший материал о взаимодействии системы подготовки кадров, научного и производственного сектора.</li>\r\n	<li>Востребованность. Лучший материал о мастерах и наставниках на производстве.</li>\r\n	<li>Творчество. Лучший материал студенческого СМИ.</li>\r\n	<li>Профессия. Лучший материал о работе педагога профессионального образовательного учреждения.</li>\r\n	<li>Рынок. Лучший материал о проблемах трудоустройства молодых профессионалов.</li>\r\n	<li>Востребованность. Лучший материал о наиболее востребованных специальностях.</li>\r\n</ol>\r\n<h2>Критерии оценки конкурсных работ<strong></strong></h2>\r\n<p>В рамках конкурса приоритет отдаётся журналистским материалам, освещающим и разъясняющим читателям ход развития и модернизации системы профобразования в Российской Федерации.\r\n</p>\r\n<p>Поступившие от участников конкурсные работы становятся доступными членам Экспертного совета. Ознакомившись с содержанием работы, члены Экспертного совета выставляют баллы на основании следующих критериев.\r\n</p>\r\n<table class=\"table\">\r\n<tbody>\r\n<tr>\r\n	<td>\r\n		<p><strong>Критерии оценки конкурсных заявок   СМИ</strong>\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p><strong>Максимальное число баллов</strong>\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Актуальность   и качество содержания\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>10\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Жанровое   разнообразие\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>10\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Качество   дизайна и внешний вид\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>10\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Оригинальность   иллюстраций\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>10\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Качество   верстки / монтажа\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>10\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p><strong>Итоговое значение (просчитывается   автоматически)</strong>\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>Сумма   значений\r\n		</p>\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<table class=\"table\">\r\n<tbody>\r\n<tr>\r\n	<td>\r\n		<p><strong>Критерии оценки конкурсных заявок   журналистов</strong>\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p><strong>Максимальное число баллов</strong>\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Актуальность   материалов, соответствие основным направлениям развития профессионального   образования Российской Федерации\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>10\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Достоверность   и информационная насыщенность\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>10\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Соответствие   содержания материала потребностям целевых аудиторий\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>10\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Глубина   раскрытия темы\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>10\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Выразительность   материала\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>10\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p>Качество   и соответствие современным требованиям к журналистским материалам\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>10\r\n		</p>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>\r\n		<p><strong>Итоговое значение (просчитывается   автоматически)</strong>\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<p>Сумма   значений\r\n		</p>\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<h2>Порядок оценки работ и определения победителей<strong></strong></h2>\r\n<p>Члены Экспертного совета оценивают поступившие конкурсные заявки через личные кабинеты.\r\n</p>\r\n<p>Итоговой оценкой материала становится сумма значений, соответствующих каждому критерию. Победу в номинации одерживает материал, набравший большее число баллов. Апелляция результатов конкурса не предусмотрена.\r\n</p>\r\n<h2>Призы и дипломы</h2>\r\n<p>Победители конкурса получают почетные дипломы и призы. В случае, если участник не может лично получить приз и диплом в ходе церемонии награждения в Москве, приз будет отправлен ему на указанный при регистрации почтовый адрес. Организаторы не несут ответственности за недоставку приза в случае, если адрес доставки указан неверно.\r\n</p>\r\n<p><strong></strong>\r\n</p>\r\n<p>Для получения приза победитель конкурса обязан предоставить в оргкомитет по электронной почте следующие данные:\r\n</p>\r\n<p>•    Копию паспорта (первой страницы и страницы с указанием места регистрации)\r\n</p>\r\n<p>•    Копию СНИЛС.\r\n</p>\r\n<p>В случае, если победитель не имеет данных документов, их должен предоставить представитель победителя, получающий приз. Приз не может быть выдан без предоставления указанных данных. Оргкомитет гарантирует их конфиденциальность и нераспространение.\r\n</p>\r\n<p><strong></strong>\r\n</p>\r\n<h2>Авторские права<strong></strong></h2>\r\n<p>Ответственность за соблюдение авторских прав работы, участвующей в Конкурсе, несет участник, приславший данную работу на Конкурс. Присылая свою работу на Конкурс, авторы дают право оргкомитету конкурса на использование присланного материала в некоммерческих целях (размещение в Интернете, в печатных изданиях, на выставочных стендах).\r\n</p>\r\n<p><strong></strong>\r\n</p>\r\n<h2>Контактная информация<strong></strong></h2>\r\n<p>Официальная страница Конкурса в интернет: <a href=\"http://konkurssmi.ru/\">http://konkurssmi.ru</a> (размещение официальной информации, информации об участниках, сбор конкурсных материалов, освещение хода Конкурса, представление результатов).\r\n</p>',1436303386,1,0,NULL),(3,'Номинации конкурса','nominations','<h3>Номинации для СМИ<strong></strong></h3><ol>\r\n	<li>Лучшее издание общего характера, освещающее тему профобразования.</li>\r\n	<li>Лучшее специализированное издание, посвященное образованию.</li>\r\n	<li>Лучшее издание образовательной организации учреждения (НПО /СПО, вуза).</li>\r\n</ol><h3>Номинации для журналистов</h3><ol>\r\n	<li>Тема. Лучший материал по теме модернизация профессионального образования.</li>\r\n	<li>Видео. Лучший видеосюжет о российском профессиональном образовании.</li>\r\n	<li>Радио. Лучший радиосюжет о российском профессиональном образовании.</li>\r\n	<li>Сеть. Лучший информационный проект для интернет-аудитории.</li>\r\n	<li>Графика. Лучший инфографический материал по теме профобразования</li>\r\n	<li>История успеха. Лучший материал, посвященный студентам и выпускникам учреждений системы профессионального образования, молодым рабочим.</li>\r\n	<li>Практика. Лучший материал о взаимодействии системы подготовки кадров, научного и производственного сектора.</li>\r\n	<li>Востребованность. Лучший материал о мастерах и наставниках на производстве.</li>\r\n	<li>Творчество. Лучший материал студенческого СМИ.</li>\r\n	<li>Профессия. Лучший материал о работе педагога профессионального образовательного учреждения.</li>\r\n	<li>Рынок. Лучший материал о проблемах трудоустройства молодых профессионалов.</li>\r\n	<li>Востребованность. Лучший материал о наиболее востребованных специальностях.</li>\r\n</ol>',1437084093,1,0,''),(4,'​Всероссийский конкурс средств массовой информации \"PRO Образование - 2015\"','open','<p>Министерство образования и науки Российской Федерации объявляет о начале Всероссийского конкурса средств массовой информации \"PRO Образование - 2015\". Конкурс проводится с целью популяризации лучших журналистских работ по теме профессионального образования, а также поощрения лучшего журналистского опыта и поддержки студенческих СМИ.<br></p>    <p>К участию в конкурсе приглашаются представители средств массовой информации, зарегистрированные на территории Российской Федерации, индивидуальные журналисты, представители информационных изданий образовательных организаций (студенческие СМИ).</p>    <p>Прием конкурсных заявок будет осуществляться с 01 августа по 15 октября 2015 года на специальном портале конкурса <a href=\"http://konkurssmi.ru/\">http://konkurssmi.ru</a>. Регламент конкурса предусматривает, что и подача и экспертная оценка материалов соискателей членами жюри осуществляется на портале конкурса. Заявки, поданные любым другим способом, к участию не допускаются.</p>    <p>К участию в конкурсе допускаются материалы на русском языке, опубликованные/размещенные в СМИ с 01 января 2014 года. К участию в конкурсе не допускаются научно-методические статьи, разъяснительные комментарии к нормативно-правовым актам, научные авторские исследования, методические разработки, а также издания, ориентированные на публикацию вышеперечисленных материалов.</p>    <p>Традиционно, в конкурсе 2015 года, предусмотрено два блока номинаций: для СМИ и для индивидуальных журналистов.</p>    <p><strong>Номинации для СМИ</strong></p>  <ul><li>Лучшее печатное издание и интернет-СМИ общего характера, освещающее тему развития профобразования в Российской Федерации.</li><li>Лучшее информационное агентство, освещающее тему развития профобразования в Российской Федерации.</li><li>Лучший телеканал, освещающий тему развития профобразования в Российской Федерации.</li><li>Лучшая радиостанция, освещающая тему развития профобразования в Российской Федерации.</li><li>Лучшее специализированное издание, посвященное образованию.</li><li>Лучшее издание образовательного учреждения (НПО / СПО, вуза).</li><li>Лучший материал печатного и интернет-СМИ по теме модернизации профессионального образования в печатном и интернет-СМИ.</li><li>Лучший материал информационного агентства по теме модернизации профессионального образования.</li><li>Лучший телевизионный материал по теме модернизации профессионального образования информационного агентства</li><li>Лучший радиоматериал по теме модернизации профессионального образования информационного агентства. </li><li>Лучший материал студенческого печатного и интернет-СМИ. </li><li>Лучший материал студенческого телевидения.</li><li>Лучший материал студенческого радио.</li><li>Лучший инфографический материал по теме профобразования. </li><li>Лучший информационный проект по теме профобразования в интернет и социальных сетях.</li></ul>              <p><strong>Номинации для журналистов</strong></p>                    <p>В состав жюри конкурса войдут представители федеральных и региональных органов управления образованием, образовательных учреждений и медиасообщества.</p>    <p>Конкурс организуется по инициативе Минобрнауки России. Полная информация о конкурсе доступна на сайте <a href=\"http://konkurssmi.ru/\">http://konkurssmi.ru</a>. </p>    <p><strong>Оргкомитет конкурса СМИ</strong></p>  <p>\"PRO Образование - 2015\"</p>  <p>Ярослав Куплинов</p>  <p>+7 495 624 0301</p>  <p><a href=\"mailto:konkurssmi@agt-corp.ru\">konkurssmi@agt-corp.ru</a></p>  <p><a href=\"https://www.facebook.com/proobr\">https://www.facebook.com/proobr</a></p>',1437589053,1,1,'Министерство образования и науки Российской Федерации объявляет о начале Всероссийского конкурса средств массовой информации \"PRO Образование - 2015\". Конкурс проводится с целью популяризации лучших журналистских работ по теме профессионального образования, а также поощрения лучшего журналистского опыта и поддержки студенческих СМИ.');
/*!40000 ALTER TABLE `article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_assignment`
--

DROP TABLE IF EXISTS `auth_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_assignment`
--

LOCK TABLES `auth_assignment` WRITE;
/*!40000 ALTER TABLE `auth_assignment` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item`
--

DROP TABLE IF EXISTS `auth_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item`
--

LOCK TABLES `auth_item` WRITE;
/*!40000 ALTER TABLE `auth_item` DISABLE KEYS */;
INSERT INTO `auth_item` VALUES ('addNews',2,'Добавление/редактирование новостей',NULL,NULL,1436303216,1436303216),('addWork',2,'Добавление работы',NULL,NULL,1436303216,1436303216),('admin',1,NULL,'userGroup',NULL,1436303217,1436303217),('editWork',2,'Редактирование работы',NULL,NULL,1436303216,1436303216),('jury',1,NULL,'userGroup',NULL,1436303217,1436303217),('manageKonkurs',2,'Добавление/редактирование конкурсов',NULL,NULL,1436303216,1436303216),('manager',1,NULL,'userGroup',NULL,1436303217,1436303217),('manageUsers',2,'Управление пользователями',NULL,NULL,1436303216,1436303216),('participant',1,NULL,'userGroup',NULL,1436303217,1436303217),('rateWork',2,'Оценивание работы',NULL,NULL,1436303216,1436303216),('updateOwnProfile',2,'Update own profile','isProfileOwner',NULL,1436303216,1436303216);
/*!40000 ALTER TABLE `auth_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item_child`
--

DROP TABLE IF EXISTS `auth_item_child`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item_child`
--

LOCK TABLES `auth_item_child` WRITE;
/*!40000 ALTER TABLE `auth_item_child` DISABLE KEYS */;
INSERT INTO `auth_item_child` VALUES ('admin','editWork'),('admin','manageKonkurs'),('admin','manager'),('admin','manageUsers'),('editWork','addWork'),('jury','rateWork'),('jury','updateOwnProfile'),('manager','addNews'),('participant','addWork'),('participant','updateOwnProfile'),('updateOwnProfile','manageUsers');
/*!40000 ALTER TABLE `auth_item_child` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_rule`
--

DROP TABLE IF EXISTS `auth_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_rule`
--

LOCK TABLES `auth_rule` WRITE;
/*!40000 ALTER TABLE `auth_rule` DISABLE KEYS */;
INSERT INTO `auth_rule` VALUES ('isProfileOwner','O:23:\"app\\rbac\\OwnProfileRule\":3:{s:4:\"name\";s:14:\"isProfileOwner\";s:9:\"createdAt\";i:1436303216;s:9:\"updatedAt\";i:1436303216;}',1436303216,1436303216),('userGroup','O:22:\"app\\rbac\\UserGroupRule\":3:{s:4:\"name\";s:9:\"userGroup\";s:9:\"createdAt\";i:1436303216;s:9:\"updatedAt\";i:1436303216;}',1436303216,1436303216);
/*!40000 ALTER TABLE `auth_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jury`
--

DROP TABLE IF EXISTS `jury`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jury` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `konkurs_id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `activity` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_konkurs_jury` (`konkurs_id`),
  KEY `fk_user_jury` (`user_id`),
  CONSTRAINT `fk_user_jury` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_konkurs_jury` FOREIGN KEY (`konkurs_id`) REFERENCES `konkurs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jury`
--

LOCK TABLES `jury` WRITE;
/*!40000 ALTER TABLE `jury` DISABLE KEYS */;
INSERT INTO `jury` VALUES (1,4,1,'Константин Константинович Константинопольский',NULL);
/*!40000 ALTER TABLE `jury` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jury_category`
--

DROP TABLE IF EXISTS `jury_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jury_category` (
  `jury_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`jury_id`,`category_id`),
  KEY `fk_work_category_jury_category` (`category_id`),
  CONSTRAINT `fk_jury_jury_category` FOREIGN KEY (`jury_id`) REFERENCES `jury` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jury_category`
--

LOCK TABLES `jury_category` WRITE;
/*!40000 ALTER TABLE `jury_category` DISABLE KEYS */;
INSERT INTO `jury_category` VALUES (1,1);
/*!40000 ALTER TABLE `jury_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `konkurs`
--

DROP TABLE IF EXISTS `konkurs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `konkurs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `konkurs`
--

LOCK TABLES `konkurs` WRITE;
/*!40000 ALTER TABLE `konkurs` DISABLE KEYS */;
INSERT INTO `konkurs` VALUES (1,'2014 год',NULL);
/*!40000 ALTER TABLE `konkurs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mark_category`
--

DROP TABLE IF EXISTS `mark_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mark_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `max` int(3) NOT NULL DEFAULT '10',
  `participant_type` smallint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mark_category`
--

LOCK TABLES `mark_category` WRITE;
/*!40000 ALTER TABLE `mark_category` DISABLE KEYS */;
INSERT INTO `mark_category` VALUES (1,'Актуальность материалов, их соответствие основным направлениям развития и модернизации системы профессионального образования в России',10,1),(2,'Достоверность и информационная насыщенность',10,1),(3,'Соответствие стиля и формы подачи материала её целевым аудиториям',10,1),(4,'Глубина раскрытия темы',10,1),(5,'Оригинальность и выразительность подачи материала',10,1),(6,'Актуальность и качество содержания',10,2),(7,'Жанровое разнообразие',10,2),(8,'Качество дизайна и внешний вид',10,2),(9,'Оригинальность иллюстраций',10,2),(10,'Качество верстки / монтажа',10,2);
/*!40000 ALTER TABLE `mark_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` VALUES ('m000000_000000_base',1435137880),('m140506_102106_rbac_init',1435137884),('m150603_205753_initial',1436303314),('m150629_165020_work_file_uploaded',1436303314),('m150716_225423_file_download_url',1437382909),('m150720_100032_file_not_required',1437394238),('m150720_120536_delete_work_category',1437394238),('m150720_131133_delete_work_categories_at_all',1437587766),('m150723_084419_article_add_excerpt_field',1437641641),('m150723_090011_add_work_mark_category',1437643168),('m150727_203645_add_work_nomination',1438005265),('m150727_210512_add_participant_region',1438006308);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `participant`
--

DROP TABLE IF EXISTS `participant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `participant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `konkurs_id` int(11) NOT NULL,
  `type` smallint(1) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `smi_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `smi_type` smallint(2) NOT NULL,
  `smi_field` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `smi_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `smi_editor` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `smi_head` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `smi_periodity` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `smi_area` text COLLATE utf8_unicode_ci,
  `smi_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `smi_jur_address` text COLLATE utf8_unicode_ci,
  `occupation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `post_address` text COLLATE utf8_unicode_ci NOT NULL,
  `city_phone` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mobile_phone` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `add_time` int(11) NOT NULL,
  `region` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_konkurs_participant` (`konkurs_id`),
  KEY `fk_user_participant` (`user_id`),
  CONSTRAINT `fk_konkurs_participant` FOREIGN KEY (`konkurs_id`) REFERENCES `konkurs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_user_participant` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participant`
--

LOCK TABLES `participant` WRITE;
/*!40000 ALTER TABLE `participant` DISABLE KEYS */;
INSERT INTO `participant` VALUES (1,2,1,1,'Сюваев Антон Сергеевич','вести кузбасс',103,NULL,'http://vesti42.ru',NULL,NULL,NULL,NULL,NULL,NULL,'Программист','пр. Комсомольский\r\nд. 49, кв. 185','+7 923 491 6295','+7 923 491 6295',1436425872,NULL),(2,5,1,2,'Иванов Иван Арнольдович','Большая перемена',103,'Образование','http://новости-школ.рф','Иванов. И. А.','АГТ','','Россия','0',NULL,'Корреспондент','Москва, ул. Маросейка, д. 3/13','+7 495 624 0301','+7 926 286 6889',1436787144,NULL),(3,6,1,2,'Сергеев Максим Петрович','Моё СМИ',102,'общественно-политическое','http://smi.ru','Алиева Тамара Владимировна','ВГТРК','еженедельное','Западная Сибирь','1',NULL,'ведущий','Россия, г. Кемерово, пр. Комсомольский, д. 49, кв. 185','+7 384 273 8902','+7 905 069 2727',1437385297,NULL),(4,7,1,2,'Памурзина Елена  Викторовна','газета \"Полет\"',101,'многотиражная газета вуза','http://vk.com/polet_ssau','Памурзина Елена Викторовна','Самарский государственный аэрокосмический университет имени академика С.П. Королева, национальный исследовательский университет','2 раза в месяц','Самарский государственный аэрокосмический университет имени академика С.П. Королева, национальный исследовательский университет','',NULL,'редактор ','Россия, Самара, Московское шоссе, 34, 16 корп., 417 офис, 443086','+7 846 267 4499','+7 906 343 8259',1437390912,NULL),(5,8,1,1,'Шумакова Екатерина Алексеевна','АНО \"Знамя Октября\" (Город Пласт); Учебный интернет-блог \"Порадуй Слона\"',101,NULL,'http://www.plast-gazeta.ru ; http://journ-csu.livejournal.com',NULL,NULL,NULL,NULL,NULL,NULL,'Журналист','Россия, Челябинск, улица 50 лет ВЛКСМ, 8 А, кв. 5 - 3/1,2\r\n454031','+7 919 114 5354','+7 950 738 3368',1437410007,'74'),(6,9,1,2,'Каючкин Александр Васильевич','Учёба.ру',103,'образовательное, профориентационное и карьерное нью-медиа','http://www.ucheba.ru','Каючкин Александр Васильевич','Учредитель: ООО \"Импексхаус\", Издатель: ООО \"РДВ-медиа\".','ежедневно','Сайт+паблики в соцсетях, аудититория -- более 1,5 млн. человек в месяц.','0',NULL,'Руководитель проекта, и.о. главного редактора','Москва, ул. Малый Дровяной переулок 3с1','+7 495 912 6166','+7 915 257 7592',1437472877,'77'),(7,10,1,1,'Тульская Елена  Геннадьевна','Информационный студенческий молодежный журнал «Nota Bene! Обрати внимание»',102,NULL,'http://www.ncfu.ru/index.php?do=static&page=nota_bene',NULL,NULL,NULL,NULL,NULL,NULL,'корреспондент управления по информации и связям с общественностью Северо-Кавказского федерального университета','355003, Россия, г. Ставрополь, ул. Дзержинского, 174, кв. 26','+7 918 778 3505','+7 988 112 4719',1437565788,NULL),(8,11,1,1,'Охрименко Яна Сергеевна','Наша молодежь',102,NULL,'http://nasha-molodezh.ru/',NULL,NULL,NULL,NULL,NULL,NULL,'собкор','Россия, Георгиевск ул. Тронина 12 кв.10, 357821','+7 879 516 8437','+7 928 364 1800',1437951634,'26'),(9,12,1,1,'Смоленцева Анна Витальевна','СТУДЕНЧЕСКАЯ ИНФОРМАЦИОННО-РАЗВЛЕКАТЕЛЬНАЯ ПРОГРАММА «НАШ УНИВЕР»',400,NULL,'http://xn--24-6kcipq7ab4az8c.xn--p1ai/',NULL,NULL,NULL,NULL,NULL,NULL,'журналист','Красноярский край,г.Красноярск, пр.Свободный 76Ж-701, индекс 660041','+7 913 582 7757','+7 913 582 7757',1437968252,'24'),(10,13,1,2,'Морус Елена Владимировна',' \"Тром-Аганские звездочки\"',101,'общественное ','https://vk.com/tromagans_stars','Морус Елена Владимировна','МБОУ \"Русскинская СОШ\"','раз в месяц','печатное, электронное','4',NULL,'руководитель школьного пресс-центра','Россия, 628446, Тюменская область, ХМАО-Югра, Сургуитский район, д. Русскинская, ул. Взлетная, д.12,кв.1','+7 346 273 7086','+7 922 248 0463',1437980572,'86'),(11,14,1,2,'Веселова Анна Николаевна','\"ПРО ШКОЛУ\"',102,'образовательно-просветитетельское','http://vk.com/about_school.journal','Веселова Анна Николаевна','Балашова Ирина Юрьевна','ежеквартально','Вологодская область','2',NULL,'главный редактор','162622 Вологодская область, город Череповец, проспект Победы, 43 Б - 62','+7 820 255 8803','+7 911 507 6335',1437985337,'35'),(12,15,1,1,'Вахитова Лилия Мингалеевна','газета \"Правда Севера\"',101,NULL,'http://minregion.nso.ru/page/3257',NULL,NULL,NULL,NULL,NULL,NULL,'внештатный корреспондент','Новосибирская область село Кыштовка, Россия','+7 383 712 1861','+7 383 712 1861',1437987915,'54'),(13,16,1,2,'Баркова Ирина Алексеевна','\"Учитель\"',101,'отраслевое','https://teachervspu.wordpress.com/','Баркова Ирина Алексеевна','Волгоградский государственный социально-педагогический университет','1 раз в месяц','бесплатное','1',NULL,'редактор','Россия, Волгоград, пр. им. В.И. Ленина, 27, ауд. 909, 400066','+7 905 395 0288','+7 905 395 0288',1437987943,'34');
/*!40000 ALTER TABLE `participant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activation_key` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `group` smallint(6) NOT NULL DEFAULT '2',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'2uoCqfQPKl-j7j5ASPLe0vnIXgKTaavm','$2y$13$D0rI8UZUTlX7Ia/JE43cTO1BPIzAG0RN0llzDqZz.Vn.XUCAaGgTO',NULL,NULL,'anton@elustro.ru',20,0,1436303313,1436303313),(2,'w5KQPQ7vCQT_4h_Nwu7aR9d8gXjenqB-','$2y$13$5e92rRi6SxVxPYXjJ/42Q.uaUaPj5sNm1jBAVAbv/xE6K0WzyYYiS',NULL,NULL,'gpko@elustro.ru',20,2,1436425873,1436425953),(3,'iLRVN-Zv_JR5k6ZN0aR0uSFyN0nQmNTX','$2y$13$Zl5aJCmc2jg6vwhHBCLEgOtFfI8VLzc3jBqHUjgGNiuKXTRWMMUNC',NULL,NULL,'y.kuplinov@agt-agency.ru',20,0,1436426123,1436426123),(4,'V9OC-M95gdDJsULq726UgjJgAPV7h4SI','$2y$13$KlB7TedQEvIt4.Hwe1wcruDuEgmtYaxPRHX0GldCYZb.pKTHBoQOe',NULL,NULL,'fedmix@elustro.ru',20,1,1436426259,1437587486),(5,'8dZnNLPXgNMX6odOuAyZMPX8WH9gtwyo','$2y$13$KOqDPoYuf3fek7S5stj55eivFyr.l16huheS0r6zXUJt4pCTtTBWe',NULL,NULL,'konkurssmi@agt-corp.ru',20,2,1436787145,1436787623),(6,'byC5TVn8aVhXbrfPqVnY5HLithT9nTf8','$2y$13$byyCMJmgWckMa5ZX0xCizOJporuvkdlwTNmQBhWAHr3pp1WXC3XGe',NULL,NULL,'gtrk@elustro.ru',20,2,1437385298,1437385351),(7,'GOHVHzP5Gfqzt4LJlZQdzjL8zv88e8uj','$2y$13$PL5kw5/K9sMbzbMpF9d8Me6AFjg2c1Ue14eKzHRkU8zH4TgkHvXb.',NULL,NULL,'rflew@mail.ru',20,2,1437390913,1437390935),(8,'B4Uvx0gfXUxbcbB7-kRyf1QmDWehdXYQ','$2y$13$cqKBgryc9z1a5bw1YmBm4uVJyY5RTAMP51giBe2MR6TypMvLJC.2a',NULL,NULL,'katyushka-yunkor@mail.ru',20,2,1437410008,1438006701),(9,'TopMCV-uJ0HY4sFpowpuoKfDybvtwMfP','$2y$13$mQRpwI8vLg7E92AQKNvhO.aOOQDbQJv8ap1ZmftrTZAEG8IwgvZ7G',NULL,NULL,'editor@ucheba.ru',20,2,1437472878,1438006688),(10,'Tb19PBuHXbR-cGk0v_cZl-fhWU32mJS6','$2y$13$2yHozfbcTfKROUkaPo/0AeFQRAs4KItNGCecvtHfRjVD5i2unWfey',NULL,NULL,'lenamax7@yandex.ru',20,2,1437565789,1437565812),(11,'nGAKGuffIU03D88WHbzO2bOFrP8-beKB','$2y$13$Tq1C/qXjR3l4o5Ij9mXZOeqOmWa9KtrXhcw.MSlGSFsHnzMgD8Aei',NULL,NULL,'yana_ohrimenko@mail.ru',20,2,1437951637,1438006646),(12,'jmzCeOmBzwDglD7QpaqymYJegHaV_IuH','$2y$13$iSn./3NkLIm0dmLO9awld.LHj1rGnHZzMsM2wGNKwNOCLL66GMn72',NULL,NULL,'anna-smoll@mail.ru',20,2,1437968253,1438006490),(13,'pshS4mwjnoPfO-JZV1fFrLsaTAa8SPcK','$2y$13$fINkSdbBzZVNsCp5Gz5lmO3b7qkelf7YLFkSsR92dgkGhSBf2qQu6',NULL,NULL,'elenamorus@mail.ru',20,2,1437980573,1438006457),(14,'aPqoPWmtaeft_hbdajDflpSyGIPiUvY7','$2y$13$xpOj/rI.ymCW.vvPi43UKuF8EAsMmyauAxXKO8ODfNn.Zjaxqjnkm',NULL,NULL,'vanna_80@mail.ru',20,2,1437985338,1438006432),(15,'-TLlushMqG2OGsh3NxoSGt2kEfD0plhd','$2y$13$30o4F5BZQ9ujkcDfc1Yqt.xzQX5NtfwcT3tNoM4POmUdoDEBdj8EK',NULL,NULL,'liliya__1975@mail.ru',20,2,1437987917,1438006409),(16,'GgahjJTY2cXGJvEHaZ_vsMNXIaniXeIh','$2y$13$UIXgPlVot9Znkb1VxU.sDOvTzKYgwIqnTJhBf9GsUwkiVpglcp6ZG',NULL,'jx32NqaaH7','pressa@vspu.ru',10,2,1437987944,1438006341);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `work`
--

DROP TABLE IF EXISTS `work`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `work` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konkurs_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `file_name` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_type` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `date` date DEFAULT NULL,
  `picture` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `on_remote` tinyint(1) DEFAULT '0',
  `remote_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `download_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomination` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_konkurs_work` (`konkurs_id`),
  KEY `fk_user_work` (`user_id`),
  CONSTRAINT `fk_konkurs_work` FOREIGN KEY (`konkurs_id`) REFERENCES `konkurs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_user_work` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `work`
--

LOCK TABLES `work` WRITE;
/*!40000 ALTER TABLE `work` DISABLE KEYS */;
INSERT INTO `work` VALUES (9,1,12,NULL,NULL,'Вторая ступень высшего образования  (Магистратура)','2015-07-06','IAwQ30Y9-R6kw2J.jpg',0,'','https://cloud.mail.ru/public/JDz5/G9Zks3FKc',101),(10,1,13,NULL,NULL,'Тром-аганские звездочки','2014-01-30','25Tw0yev6xPNZyj.jpg',0,'','https://vk.com/tromagans_stars',101),(11,1,15,'g75YjcQk-bf9arK1wzOjyM3EgUERpzmi','docx','Не ждите, когда вас кто-то куда-то позовет. Идите сами.','2015-04-29','Ava7ER7nz6A5yrx.jpg',0,'','',101),(12,1,11,'FHq1qMIs4-2KVtsSRlnMRbWubl61HXcw','docx','Профессия, без которой не обойтись.','2014-02-16','SxgxAAbZeVYa2SK.jpg',0,'','',101);
/*!40000 ALTER TABLE `work` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `work_mark`
--

DROP TABLE IF EXISTS `work_mark`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `work_mark` (
  `work_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `mark_category_id` int(11) NOT NULL,
  `mark` int(11) NOT NULL,
  PRIMARY KEY (`work_id`,`user_id`,`mark_category_id`),
  KEY `fk_user_work_mark` (`user_id`),
  KEY `fk_mark_category_work_mark` (`mark_category_id`),
  CONSTRAINT `fk_work_work_mark` FOREIGN KEY (`work_id`) REFERENCES `work` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_mark_category_work_mark` FOREIGN KEY (`mark_category_id`) REFERENCES `mark_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_user_work_mark` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `work_mark`
--

LOCK TABLES `work_mark` WRITE;
/*!40000 ALTER TABLE `work_mark` DISABLE KEYS */;
/*!40000 ALTER TABLE `work_mark` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-07-27 18:14:33
