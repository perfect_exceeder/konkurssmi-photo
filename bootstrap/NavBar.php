<?php
/**
 * NavBar.php
 * Created by h8every1 on 26.06.2015 1:05
 */

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\bootstrap;

use Yii;
use yii\bootstrap\BootstrapPluginAsset;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * NavBar renders a navbar HTML component.
 *
 * Any content enclosed between the [[begin()]] and [[end()]] calls of NavBar
 * is treated as the content of the navbar. You may use widgets such as [[Nav]]
 * or [[\yii\widgets\Menu]] to build up such content. For example,
 *
 * ```php
 * use yii\bootstrap\NavBar;
 * use yii\widgets\Menu;
 *
 * NavBar::begin(['brandLabel' => 'NavBar Test']);
 * echo Nav::widget([
 *     'items' => [
 *         ['label' => 'Home', 'url' => ['/site/index']],
 *         ['label' => 'About', 'url' => ['/site/about']],
 *     ],
 * ]);
 * NavBar::end();
 * ```
 *
 * @see http://getbootstrap.com/components/#navbar
 * @author Antonio Ramirez <amigo.cobos@gmail.com>
 * @author Alexander Kochetov <creocoder@gmail.com>
 * @since 2.0
 */
class NavBar extends \yii\bootstrap\NavBar {
    /**
     * @var string text to show for screen readers for the button to toggle the navbar.
     */
    public $screenReaderToggleText = 'Показать меню';
    /**
     * @var boolean whether the navbar content should be included in an inner div container which by default
     * adds left and right padding. Set this to false for a 100% width navbar.
     */
    public $renderInnerContainer = false;
    /**
     * @var array the HTML attributes of the inner container.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $innerContainerOptions = [ ];


    /**
     * Initializes the widget.
     */
    public function init() {
//        parent::init();

        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }

        $this->clientOptions = false;
        if (!isset($this->containerOptions['id'])) {
            $this->containerOptions['id'] = "{$this->options['id']}-collapse";
        }

        if ( empty( $this->options['role'] ) ) {
            $this->options['role'] = 'navigation';
        }

        $options = $this->options;
        echo Html::beginTag( 'nav', $options );
        echo Html::beginTag( 'div', [ 'class' => 'container' ] );
        echo Html::beginTag( 'div', [ 'class' => 'row' ] );
        if ( $this->brandLabel !== false ) {

            if (!is_array($this->brandLabel)) {
                $this->brandLabel = [$this->brandLabel];
            }

            if (!is_array($this->brandUrl)) {
                $this->brandUrl = [$this->brandUrl];
            }

            echo Html::beginTag( 'div', [ 'class' => 'col-lg-3 col-md-3 col-sm-4 col-xs-4 pull-left' ] );

            foreach ( $this->brandLabel as $i=>$label ) {

                $url = !empty($this->brandUrl[$i]) ? $this->brandUrl[$i] : Yii::$app->homeUrl;

                echo Html::beginTag( 'div', [ 'class' => 'logo' ] );
                echo Html::a( $label, $url, $this->brandOptions );
                echo Html::endTag( 'div' );
            }
            echo Html::endTag( 'div' );

            $menu_classes = 'col-lg-9 col-md-9col-sm-8 col-xs-8 pull-right';
        } else {
            $menu_classes = 'col-xs-12';
        }
        echo Html::beginTag( 'div', [ 'class' => $menu_classes ] );
        echo Html::beginTag( 'div', [ 'class' => 'menu dankovteam-menu-wrapper' ] );
        echo $this->renderToggleButton();


//        echo Html::endTag( 'div', [ 'class' => $menu_classes ] );

        if ( ! isset( $this->containerOptions['id'] ) ) {
            $this->containerOptions['id'] = "{$this->options['id']}-collapse";
        }
        Html::addCssClass( $this->containerOptions, 'collapse' );
        Html::addCssClass( $this->containerOptions, 'navbar-collapse' );
        $options = $this->containerOptions;
        echo Html::beginTag( 'div', $options );
        echo Html::beginTag( 'div', [ 'class' => 'menu-main-menu-container' ] );

    }

    /**
     * Renders the widget.
     */
    public function run() {
        echo Html::endTag( 'div');
        echo Html::endTag( 'div');
        echo Html::endTag( 'div');
        echo Html::endTag( 'div');
        echo Html::endTag( 'div');
        echo Html::endTag( 'div');
        echo Html::endTag( 'nav');
        BootstrapPluginAsset::register( $this->getView() );
    }
}
