<?php

namespace app\bootstrap;

/**
 * MyNav.php
 * Created by h8every1 on 26.06.2015 2:42
 */
class Nav extends \yii\bootstrap\Nav {
    /**
     * Renders the given items as a dropdown.
     * This method is called to create sub-menus.
     *
     * @param array $items the given items. Please refer to [[Dropdown::items]] for the array structure.
     * @param array $parentItem the parent item information. Please refer to [[items]] for the structure of this array.
     *
     * @return string the rendering result.
     * @since 2.0.1
     */
    protected function renderDropdown( $items, $parentItem ) {
        return Dropdown::widget( [
            'items'         => $items,
            'encodeLabels'  => $this->encodeLabels,
            'clientOptions' => false,
            'view'          => $this->getView(),
            'options'       => [ 'class' => 'sub-menu' ],
        ] );
    }
}