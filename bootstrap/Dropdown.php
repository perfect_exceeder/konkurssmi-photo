<?php
/**
 * Dropdown.php
 * Created by h8every1 on 26.06.2015 2:49
 */

namespace app\bootstrap;

use yii\helpers\Html;

class Dropdown extends \yii\bootstrap\Dropdown {
    /**
     * Initializes the widget.
     * If you override this method, make sure you call the parent implementation first.
     */
    public function init()
    {
        parent::init();
        Html::removeCssClass($this->options, 'dropdown-menu');
    }

}