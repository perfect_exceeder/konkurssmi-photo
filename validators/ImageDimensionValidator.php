<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\validators;

use Yii;
use yii\validators\ValidationAsset;
use yii\web\UploadedFile;

/**
 * ImageValidator verifies if an attribute is receiving a valid image.
 *
 * @author Anton Syuvaev <h8every1@gmail.com>
 * @since 2.0
 */
class ImageDimensionValidator extends \yii\validators\FileValidator {
    /**
     * @var string the error message used when the uploaded file is not an image.
     * You may use the following tokens in the message:
     *
     * - {attribute}: the attribute name
     * - {file}: the uploaded file name
     */
    public $notImage;
    /**
     * @var integer the minimum image dimension in pixels.
     * Defaults to null, meaning no limit.
     * @see underDimension for the customized message used when image hright/width is too small.
     */
    public $minDimension;
    /**
     * @var integer the maximum image dimension in pixels.
     * Defaults to null, meaning no limit.
     * @see overDimension for the customized message used when image hright/width is too big.
     */
    public $maxDimension;
    /**
     * @var string the error message used when the image is under [[minDimension]].
     * You may use the following tokens in the message:
     *
     * - {attribute}: the attribute name
     * - {file}: the uploaded file name
     * - {limit}: the value of [[minDimension]]
     */
    public $underDimension;
    /**
     * @var string the error message used when the image is over [[maxDimension]].
     * You may use the following tokens in the message:
     *
     * - {attribute}: the attribute name
     * - {file}: the uploaded file name
     * - {limit}: the value of [[maxDimension]]
     */
    public $overDimension;


    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();

        if ( $this->notImage === null ) {
            $this->notImage = Yii::t( 'yii', 'The file "{file}" is not an image.' );
        }

        if ( $this->underDimension === null ) {
            $this->underDimension = Yii::t( 'yii',
                'The image "{file}" is too small. At least one dimension should be greater than {limit, number} {limit, plural, one{pixel} other{pixels}}.' );
        }
        if ( $this->overDimension === null ) {
            $this->overDimension = Yii::t( 'yii',
                'The image "{file}" is too large. The width and height cannot be larger than {limit, number} {limit, plural, one{pixel} other{pixels}} each.' );
        }
    }

    /**
     * @inheritdoc
     */
    protected function validateValue( $file ) {
        $result = parent::validateValue( $file );

        return empty( $result ) ? $this->validateImage( $file ) : $result;
    }

    /**
     * Validates an image file.
     *
     * @param UploadedFile $image uploaded file passed to check against a set of rules
     *
     * @return array|null the error message and the parameters to be inserted into the error message.
     * Null should be returned if the data is valid.
     */
    protected function validateImage( $image ) {

        if ( false === ( $imageInfo = getimagesize( $image->tempName ) ) ) {
            return [ $this->notImage, [ 'file' => $image->name ] ];
        }

        list( $width, $height ) = $imageInfo;

        if ( $width == 0 || $height == 0 ) {
            return [ $this->notImage, [ 'file' => $image->name ] ];
        }

        if ( $this->minDimension !== null && ($width < $this->minDimension && $height < $this->minDimension)) {
            return [ $this->underDimension, [ 'file' => $image->name, 'limit' => $this->minDimension ] ];
        }

        if ( $this->maxDimension !== null && ( $width > $this->maxDimension || $height > $this->maxDimension ) ) {
            return [ $this->overDimension, [ 'file' => $image->name, 'limit' => $this->maxDimension ] ];
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function clientValidateAttribute( $model, $attribute, $view ) {
        ValidationAsset::register( $view );
        $options = $this->getClientOptions( $model, $attribute );

        return 'yii.validation2.imgDim(attribute, messages, ' . json_encode( $options,
            JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE ) . ', deferred);';
    }

    /**
     * @inheritdoc
     */
    protected function getClientOptions( $model, $attribute ) {
        $options = parent::getClientOptions( $model, $attribute );

        $label = $model->getAttributeLabel( $attribute );

        if ( $this->minDimension !== null ) {
            $options['minDimension']   = $this->minDimension;
            $options['underDimension'] = Yii::$app->getI18n()->format( $this->underDimension, [
                'attribute' => $label,
                'limit'     => $this->minDimension
            ], Yii::$app->language );
        }

        if ( $this->maxDimension !== null ) {
            $options['maxDimension']  = $this->maxDimension;
            $options['overDimension'] = Yii::$app->getI18n()->format( $this->overDimension, [
                'attribute' => $label,
                'limit'     => $this->maxDimension
            ], Yii::$app->language );
        }

        return $options;
    }
}
