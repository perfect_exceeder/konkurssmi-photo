<?php

namespace app\controllers;

use app\models\WorkCategory;
use app\models\WorkForm;
use app\models\WorkMark;
use Yii;
use app\models\Work;
use app\models\search\Work as WorkSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;

/**
 * WorkController implements the CRUD actions for Work model.
 */
class WorkController extends Controller {
    public function behaviors() {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => [ 'post' ],
                ],
            ],
        ];
    }

    public function actions() {
        return [
            'error'   => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Lists all Work models.
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionIndex() {

        if (\Yii::$app->user->isGuest) {
            throw new NotFoundHttpException( 'Страница не найдена' );
        }

        $can_edit = \Yii::$app->user->can('editWork');
        $can_rate = \Yii::$app->user->can('rateWork');

        $searchModel  = new WorkSearch();
        $dataProvider = $searchModel->search( Yii::$app->request->queryParams );

        $view = 'index';
        if (!$can_edit && !$can_rate) {
            $view = 'index_user';
        }
        return $this->render( $view, [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'can_edit' => $can_edit,
            'can_rate' => $can_rate
        ] );
    }

    /**
     * Displays a single Work model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView( $id ) {
        $can_edit = \Yii::$app->user->can('editWork');
        $can_rate = \Yii::$app->user->can('rateWork');

        if (!$can_edit && !$can_rate) {
            throw new NotFoundHttpException( 'Страница не найдена' );
        }

        $model = $this->findModel( $id );

        if ($model && ($work_mark_data = \Yii::$app->request->post('WorkMark'))) {

            $user_id = null;
            if ($can_edit && \Yii::$app->request->post('user_id')) {
                $user_id = (int)\Yii::$app->request->post('user_id');
            } else if ($can_rate){
                $user_id = \Yii::$app->user->id;
            }

            if ($user_id) {
                WorkMark::deleteAll( [ 'user_id' => $user_id, 'work_id' => $model->id ] );
                foreach ( $work_mark_data as $mark_cat => $mark ) {
                    $mark_model                   = new WorkMark();
                    $mark_model->mark             = (int) $mark['mark'];
                    $mark_model->mark_category_id = (int) $mark_cat;
                    $mark_model->user_id          = $user_id;
                    $mark_model->work_id          = $model->id;
                    $mark_model->save();
                }
                \Yii::$app->session->setFlash('info', 'Работа №'.$model->id.' оценена!');
                return $this->redirect(['view', 'id'=>$model->id]);
            }
        }

        return $this->render( 'view', [
            'model' => $model,
            'can_rate' => $can_rate,
            'can_edit' => $can_edit
        ] );
    }

    /**
     * Creates a new Work model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionAdd() {

	    if (time() > strtotime('01.09.2015 18:00')) {
		    return $this->render( 'end' );
	    }

        if ( ! Yii::$app->user->can( 'addWork' ) || Yii::$app->user->identity->activation_key ) {
            throw new NotFoundHttpException( 'Страница не найдена.' );
        }

//        if ( ( ! Yii::$app->user->can( 'editWork' ) && Yii::$app->user->identity->countWorks() ) ) {
//            return $this->redirect( [ 'user/profile' ] );
//        }

        $model = new Work( [ 'scenario' => 'add' ] );

        if ($errors = $this->performAjaxValidation($model)) {
            return $errors;
        }


        if ( $model->load( Yii::$app->request->post() ) && $model->save()) {

            if ( $model->sendEmail() ) {
                Yii::$app->session->setFlash( 'success', 'Работа добавлена!' );
            }
            return $this->redirect( [ 'user/profile' ] );
        } else {
            $model->scenario = 'form';
            return $this->render( 'create', [
                'model' => $model,
            ] );
        }
    }

    public function actionAdded() {
        return $this->render( 'added' );
    }

    /**
     * Generates hint for file input field depending on selected category
     *
     * @param $id
     *
     * @return string
     */
    public function actionGetFileInfo( $id = null ) {
        if (!\Yii::$app->request->isAjax) {
            return false;
        }
        if ( ! $id ) {
            $id = 1;
        }
        $cat = WorkCategory::findOne( $id );

        return $this->renderPartial( '_file-info', [
            'size'       => $cat->max_file_size,
            'exts'       => $cat->file_extensions,
            'additional' => $cat->file_text
        ] );

    }

    /**
     * Updates an existing Work model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate( $id ) {


        $model           = $this->findModel( $id );
        $model->scenario = 'update';

        if (! Yii::$app->user->can('editWork',['user'=>$model->user_id]) || $model->getWorkMarks()->count()) {
            throw new NotFoundHttpException( 'Такой страницы не существует' );
        }

        if ($errors = $this->performAjaxValidation($model)) {
            return $errors;
        }

        if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            return $this->redirect( [ 'update', 'id' => $model->id ] );
        } else {
            return $this->render( 'update', [
                'model' => $model,
            ] );
        }
    }

    /**
     * Deletes an existing Work model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionDelete( $id ) {
        if (! Yii::$app->user->can('editWork')) {
            throw new NotFoundHttpException( 'Такой страницы не существует' );
        }

        $model = $this->findModel( $id );
        $model->deleteAllFiles();
        $model->delete();

        return $this->redirect( [ 'index' ] );
    }

    /**
     * Finds the Work model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Work the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel( $id ) {
        if ( ( $model = Work::findOne( $id ) ) !== null ) {
            return $model;
        } else {
            throw new NotFoundHttpException( 'Страница не найдена.' );
        }
    }

    protected function performAjaxValidation($model)
    {
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }
}
