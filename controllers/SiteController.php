<?php

namespace app\controllers;

use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\Work;
use Yii;
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\SignupForm;
use app\models\Article;
use app\models\search\Article as ArticleSearch;
use yii\web\NotFoundHttpException;

class SiteController extends Controller {
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => [ 'logout' ],
                'rules' => [
                    [
                        'actions' => [ 'logout' ],
                        'allow'   => true,
                        'roles'   => [ '@' ],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'logout' => [ 'post' ],
                ],
            ],
        ];
    }

    public function actions() {
        return [
            'error'   => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class'           => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex() {

        $works       = Work::find()->count();
        $polozhenie  = Article::find()->bySlug( 'polozhenie' )->one();
        $main  = Article::find()->bySlug( 'main' )->one();
        $nominations = Article::find()->bySlug( 'nominations' )->one();
        $news = Article::find()->andCategory( 'news' )->orderBy( 'created_at DESC' )->limit( 3 )->all();
        $works_articles = Article::find()->andCategory( 'works' )->orderBy( 'created_at DESC' )->limit( 12 )->all();
//        $expert = Article::find()->andCategory( 'expert' )->orderBy( 'created_at DESC' )->limit( 4 )->all();
//        $howto = Article::find()->bySlug( 'howto' )->limit( 1 )->one();

        return $this->render( 'index',
            [
                'worksCount'  => $works,
                'polozhenie'  => $polozhenie,
                'main'  => $main,
//                'howto'       => $howto,
                'nominations' => $nominations,
                'news'        => $news,
                'works'        => $works_articles,
//                'expert'      => $expert,
            ] );
    }

    public function actionLogin() {
        if ( ! \Yii::$app->user->isGuest ) {
            return $this->redirect( [ 'user/profile' ] );
        }

        $model = new LoginForm();

        if ( $model->load( Yii::$app->request->post() ) && $model->login() ) {
            return $this->redirect( [ 'user/profile' ] );
        } else {
            return $this->render( 'login', [
                'model' => $model,
            ] );
        }
    }

    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact() {
        $model = new ContactForm();
        if ( $model->load( Yii::$app->request->post() ) && $model->contact( Yii::$app->params['adminEmail'] ) ) {
            Yii::$app->session->setFlash( 'contactFormSubmitted' );

            return $this->refresh();
        } else {
            return $this->render( 'contact', [
                'model' => $model,
            ] );
        }
    }

    public function actionAbout() {
        return $this->render( 'about' );
    }

    public function actionVerify() {
        if ( \Yii::$app->user->isGuest ) {
            throw new NotFoundHttpException( 'Страница не найдена.' );
        } elseif ( \Yii::$app->user->identity->isActivated() ) {
            return $this->redirect( [ 'user/profile' ] );
        }

        $model = new SignupForm( [ 'scenario' => 'activate' ] );
        if ( $model->load( Yii::$app->request->get() ) ) {
            if ( $user = $model->activate() ) {
                if ( Yii::$app->getUser()->login( $user ) ) {
                    Yii::$app->session->setFlash( 'success', 'Аккаунт подтвержден!' );

                    return $this->goHome();
                }
            }
        }

        return $this->render( 'verify', [
            'model' => $model,
        ] );
    }

    public function actionResendCode() {
        echo 'Resending the code...';
        if ( ! Yii::$app->user->isGuest && ! Yii::$app->user->identity->isActivated() ) {
            Yii::$app->session->setFlash( 'activationCodeResent' );
            Yii::$app->user->identity->sendConfirmEmail();

            return $this->redirect( [ 'verify' ] );
        } else {
            return $this->redirect( [ 'site/index' ] );
        }
    }

    public function actionNews() {
        $searchModel                                         = new ArticleSearch();
        $searchArray                                         = Yii::$app->request->queryParams;
        $searchArray[ $searchModel->formName() ]['category'] = Article::CATEGORY_NEWS;
        $dataProvider                                        = $searchModel->search( $searchArray );

        return $this->render( '/article/list', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ] );
    }

    public function actionExperts() {
        $searchModel                                         = new ArticleSearch();
        $searchArray                                         = Yii::$app->request->queryParams;
        $searchArray[ $searchModel->formName() ]['category'] = Article::CATEGORY_EXPERT;
        $dataProvider                                        = $searchModel->search( $searchArray );

        return $this->render( '/article/list-detail', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ] );
    }

    public function actionNominationsList() {
        $nominations = Work::allNominations();
        $descriptions = Work::getNominationsDescriptions();
        $data        = [ ];

        foreach ( $nominations as $id => $name ) {
            $data[] = [
                'id'    => $id,
                'name'  => $name,
                'description'  => $descriptions[$id],
                'count' => (int) Work::find()->in_nomination( $id )->count()
            ];
        }

        return $this->render( 'nominations-list', [
            'data'  => $data,
            'total' => (int) Work::find()->count()
        ] );
    }

	public function actionWorks() {

		$works_articles = Article::find()->andCategory( 'works' )->orderBy( 'created_at DESC' )->all();

		return $this->render( 'works',
			[
				'works'        => $works_articles,
			] );
	}

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset() {
        $model = new PasswordResetRequestForm();
        if ( $model->load( Yii::$app->request->post() ) && $model->validate() ) {
            if ( $model->sendEmail() ) {
                Yii::$app->session->setFlash( 'success', 'Проверьте свой e-mail для дальнейших шагов по восстановлению пароля.' );

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash( 'error', 'К сожалению, лдя этого e-mail адреса невозможно восстановить пароль.' );
            }
        }

        return $this->render( 'requestPasswordResetToken', [
            'model' => $model,
        ] );
    }

    /**
     * Resets password.
     *
     * @param string $token
     *
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword( $token ) {
        try {
            $model = new ResetPasswordForm( $token );
        } catch ( InvalidParamException $e ) {
            throw new BadRequestHttpException( $e->getMessage() );
        }
        if ( $model->load( Yii::$app->request->post() ) && $model->validate() && $model->resetPassword() ) {
            Yii::$app->session->setFlash( 'success', 'Новый пароль сохранен.' );

            return $this->goHome();
        }

        return $this->render( 'resetPassword', [
            'model' => $model,
        ] );
    }

}
