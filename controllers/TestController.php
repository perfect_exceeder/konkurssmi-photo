<?php
/**
 * TestController.php
 * Created by h8every1 on 24.06.2015 23:15
 */
/*
 *
 */


namespace app\controllers;

use Yii;
use yii\web\Controller;

class TestController extends Controller {

    public function actionMail() {

        if ( ! Yii::$app->request->get( 'email' ) ) {
            return 'используёте переменную email в адресной строке';
        } else {
            echo 'отправляю письмо на адрес ' . Yii::$app->formatter->asEmail( Yii::$app->request->get( 'email' ) ) . '<br>';
            var_dump( Yii::$app->mailer->compose()
                                       ->setFrom( [Yii::$app->params['siteEmail'] => 'Конкурс СМИ'] )
                                       ->setReplyTo( Yii::$app->params['adminEmail'] )
                                       ->setTo( Yii::$app->request->get( 'email' ) )
                                       ->setSubject( 'тест' )
                                       ->setTextBody( "проверка почты" )
                                       ->send() );

            return true;
        }

    }

    public function actionPhp() {

        return phpinfo();

    }
}