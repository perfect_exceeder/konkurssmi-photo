<?php

namespace app\controllers;

use Yii;
use app\models\Konkurs;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class KonkursController extends Controller {
    public function behaviors() {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => [ 'post' ],
                ],
            ],
        ];
    }

    /**
     * Lists all Konkurs models.
     *
     * @return mixed
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider( [
            'query' => Konkurs::find(),
        ] );

        return $this->render( 'index', [
            'dataProvider' => $dataProvider,
        ] );
    }

    /**
     * Creates a new Konkurs model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionCreate() {
        if ( ! \Yii::$app->user->can( 'manageKonkurs' ) ) {
            throw new NotFoundHttpException( 'Страница не найдена' );
        }
        $model = new Konkurs();

        if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            return $this->redirect( [ 'view' ] );
        } else {
            return $this->render( 'create', [
                'model' => $model,
            ] );
        }
    }

    /**
     * Updates an existing Konkurs model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate( $id ) {
        if ( ! \Yii::$app->user->can( 'manageKonkurs' ) ) {
            throw new NotFoundHttpException( 'Страница не найдена' );
        }
        $model = $this->findModel( $id );

        if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            return $this->redirect( [ 'index' ] );
        } else {
            return $this->render( 'update', [
                'model' => $model,
            ] );
        }
    }

    /**
     * Deletes an existing Konkurs model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionDelete( $id ) {
        if ( ! \Yii::$app->user->can( 'manageKonkurs' ) ) {
            throw new NotFoundHttpException( 'Страница не найдена' );
        }

        $this->findModel( $id )->delete();

        return $this->redirect( [ 'index' ] );
    }

    /**
     * Finds the Konkurs model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Konkurs the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel( $id ) {
        if ( ( $model = Konkurs::findOne( $id ) ) !== null ) {
            return $model;
        } else {
            throw new NotFoundHttpException( 'Страница не найдена.' );
        }
    }

    /**
     * Lists all works in Konkurs
     *
     * @return string
     */
    public function actionList() {
        return $this->render( 'list' );
    }

    public function actionTotals() {
        return $this->render( 'totals' );
    }

    public function actionWork() {
        return $this->render( 'work' );
    }

}
