<?php

namespace app\controllers;

use vova07\imperavi\actions\GetAction;
use Yii;
use app\models\Article;
use app\models\search\Article as ArticleSearch;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends Controller {

    private $_filesPaths = [ ];

    public function init() {
        $this->_filesPaths = [
            'imageUrl'  => Url::to( '@web/uploads/images/', true ),
            'imagePath' => '@webroot/uploads/images',
            'fileUrl'   => Url::to( '@web/uploads/files/', true ),
            'filePath'  => '@webroot/uploads/files',
        ];
        parent::init();
    }

    public function behaviors() {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => [ 'post' ],
                ],
            ],
        ];
    }

    public function actions() {
        return [
            'images-get'   => [
                'class' => 'vova07\imperavi\actions\GetAction',
                'url'   => $this->_filesPaths['imageUrl'], // Directory URL address, where files are stored.
                'path'  => $this->_filesPaths['imagePath'], // Or absolute path to directory where files are stored.
                'type'  => GetAction::TYPE_IMAGES,
            ],
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadAction',
                'url'   => $this->_filesPaths['imageUrl'],
                // URL адрес папки куда будут загружатся изображения.
                'path'  => $this->_filesPaths['imagePath']
                // Или абсолютный путь к папке куда будут загружатся изображения.
            ],
            'files-get'    => [
                'class' => 'vova07\imperavi\actions\GetAction',
                'url'   => $this->_filesPaths['fileUrl'], // Directory URL address, where files are stored.
                'path'  => $this->_filesPaths['filePath'], // Or absolute path to directory where files are stored.
                'type'  => GetAction::TYPE_FILES,
            ],
            'file-upload'  => [
                'class' => 'vova07\imperavi\actions\UploadAction',
                'url'   => $this->_filesPaths['fileUrl'],
                // URL адрес папки куда будут загружатся изображения.
                'path'  => $this->_filesPaths['filePath']
                // Или абсолютный путь к папке куда будут загружатся изображения.
            ],
        ];
    }

    /**
     * Lists all Article models.
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionIndex() {
        if ( ! \Yii::$app->user->can( 'addNews' ) ) {
            throw new NotFoundHttpException( 'Страница не найдена' );
        }
        $searchModel  = new ArticleSearch();
        $dataProvider = $searchModel->search( Yii::$app->request->queryParams );

        return $this->render( 'index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ] );
    }

    /**
     * Displays a single Article model.
     *
     * @param $slug
     * @param string $category
     *
     * @return mixed
     * @throws NotFoundHttpException
     * @internal param int $id
     *
     */
    public function actionView( $slug, $category = 'page' ) {
        return $this->render( 'view', [
            'model' => $this->findModelbySlug( $slug, $category ),
        ] );
    }

    /**
     * Creates a new Article model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionCreate() {
        if ( ! \Yii::$app->user->can( 'addNews' ) ) {
            throw new NotFoundHttpException( 'Страница не найдена' );
        }
        $model = new Article();
        $model->scenario = 'edit';

        if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            return $this->redirect( $model->route );
        } else {
            return $this->render( 'create', [
                'model' => $model,
            ] );
        }
    }

    /**
     * Updates an existing Article model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate( $id ) {
        if ( ! \Yii::$app->user->can( 'addNews' ) ) {
            throw new NotFoundHttpException( 'Страница не найдена' );
        }

        $model = $this->findModel( $id );
        $model->scenario = 'edit';

        if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            return $this->redirect( $model->route );
        } else {
            return $this->render( 'update', [
                'model' => $model,
            ] );
        }
    }

    /**
     * Deletes an existing Article model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionDelete( $id ) {
        if ( ! \Yii::$app->user->can( 'addNews' ) ) {
            throw new NotFoundHttpException( 'Страница не найдена' );
        }
        $this->findModel( $id )->delete();

        return $this->redirect( [ 'index' ] );
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param $slug
     * @param null|string $category Category name (i.e 'news', 'photo' etc.)
     *
     * @return Article the loaded model
     * @throws NotFoundHttpException
     * @internal param int $id
     *
     */
    protected function findModelbySlug( $slug, $category = null ) {
        $model = Article::find()->bySlug( $slug );
        if ( $category ) {
            $model = $model->andCategory( $category );
        }
        $model = $model->one();
        if ( ( $model ) !== null ) {
            return $model;
        } else {
            throw new NotFoundHttpException( 'Страница не найдена' );
        }
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param $id
     *
     * @return Article the loaded model
     * @throws NotFoundHttpException
     * @internal param $slug
     * @internal param null|string $category Category name (i.e 'news', 'photo' etc.)
     *
     * @internal param int $id
     */
    protected function findModel( $id ) {
        $model = Article::findOne( $id );
        if ( ( $model ) !== null ) {
            return $model;
        } else {
            throw new NotFoundHttpException( 'Страница не найдена' );
        }
    }
}
