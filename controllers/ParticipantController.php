<?php

namespace app\controllers;

use app\models\LoginForm;
use app\models\User;
use Yii;
use app\models\Participant;
use app\models\search\Participant as ParticipantSearch;
use app\models\ParticipantForm;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * ParticipantController implements the CRUD actions for Participant model.
 */
class ParticipantController extends Controller {
    public function behaviors() {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => [ 'post' ],
                ],
            ],
        ];
    }

    /**
     * Lists all Participant models.
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionIndex() {

        if (!\Yii::$app->user->can('manageUsers')) {
            throw new NotFoundHttpException( 'Страница не найдена' );
        }

        $searchModel  = new ParticipantSearch();
        $dataProvider = $searchModel->search( Yii::$app->request->queryParams );

        return $this->render( 'index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ] );
    }

    /**
     * Displays a single Participant model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView( $id ) {
        if (!\Yii::$app->user->can('manageUsers')) {
            throw new NotFoundHttpException( 'Страница не найдена' );
        }
        return $this->render( 'view', [
            'model' => $this->findModel( $id ),
        ] );
    }

    /**
     * Creates a new Participant model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionRegister( ) {
        if ( ! \Yii::$app->user->isGuest && ! \Yii::$app->user->can( 'manageUsers' ) ) {
            return $this->redirect( [ 'site/index' ] );
        }

        $model = new Participant();
        $model->setUserType( Participant::TYPE_JOURNALIST );
        $model->scenario = 'register' . $model->type;

        if ( Yii::$app->request->isAjax && $model->load( Yii::$app->request->post() ) ) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate( $model );
        }

        if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            Yii::$app->user->login($model->user, 3600 * 24 * 30);
            return $this->redirect( [ 'site/verify'] );
        } else {


            return $this->render( 'profile', [
                'model' => $model,
            ] );
        }
    }

    /**
     * Updates an existing Participant model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionProfile( $id = null ) {


        if ( ( ! \Yii::$app->user->isGuest && ! \Yii::$app->user->can( 'manageUsers' ) ) ) {
            $id = \Yii::$app->user->identity->id;
        }

        $model           = $this->findModel( (int)$id );
//        $model->scenario = 'update';


        if ( ! \Yii::$app->user->can( 'manageUsers', [ 'user' => $id ] ) ) {
            throw new NotFoundHttpException( 'Страница не найдена.' );
        }

        if (! \Yii::$app->user->can( 'manageUsers') && $model->user->activation_key ) {
            return $this->redirect(['site/verify']);
        }

        if ( Yii::$app->request->isAjax && $model->load( Yii::$app->request->post() ) ) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model->user->scenario = 'update';
            return ActiveForm::validate( $model );
        }

        if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            \Yii::$app->getSession()->setFlash('success', 'Данные сохранены');
            return $this->redirect(['profile','id'=>$model->user_id]);
        }

        return $this->render( 'profile', [
            'model'=>$model,
        ] );
    }

    public function actionChangePassword( $id = null ) {
        if ( \Yii::$app->user->isGuest ) {
            throw new NotFoundHttpException( 'Страница не найдена.' );
        }

        if ( ! \Yii::$app->user->can( 'manageUsers' ) ) {
            $id = \Yii::$app->user->identity->id;
        }

        $model           = $this->findModel( $id );
        $model->scenario = 'change_password';

        if ( Yii::$app->request->isAjax && $model->load( Yii::$app->request->post() ) ) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate( $model );
        }

        if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            \Yii::$app->session->setFlash('info', 'Пароль изменен');
            return $this->redirect( [ 'user/profile','id'=>$id ] );
        } else {
            return $this->render( 'change-password', [
                'model' => $model,
            ] );
        }

    }

    /**
     * Deletes an existing Participant model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionDelete( $id ) {
        if ( ! \Yii::$app->user->can( 'manageUsers' ) ) {
            throw new NotFoundHttpException( 'Страница не найдена.' );
        }

        $this->findModel( $id )->user->delete();

        return $this->redirect( [ 'index' ] );
    }

    /**
     * Finds the Participant model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Participant the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel( $id ) {
        if ( ( $model = User::findOne( $id ) ) !== null && isset( $model->participant ) ) {
            return $model->participant;
        } else {
            throw new NotFoundHttpException( 'Страница не найдена.' );
        }
    }
}
