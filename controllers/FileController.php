<?php
/**
 * FileController.php
 * Created by h8every1 on 04.08.2016 15:29
 */

namespace app\controllers;

use app\models\Participant;
use Yii;
use app\models\Work;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Controller;
use Alchemy\Zippy\Zippy;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class FileController extends Controller {

	private $archive;
	const ARCHIVE_NAME = 'works.zip';

	public function actionIndex() {
		$can_edit = \Yii::$app->user->can('editWork');

		if (!$can_edit) {
			throw new NotFoundHttpException( 'Страница не найдена' );
		}

		$works = Work::find()->count();

		return $this->render( 'index',
			[
				'worksCount' => $works,
			] );
	}

	private function createArchive() {
		$zippy = Zippy::load();

		$this->archive = $zippy->create( $this->getArchivePath(),
			array( 'dummy.txt' => Yii::getAlias( '@webroot/uploads/.gitkeep' ) ) );
	}

	private function loadArchive() {
		$zippy         = Zippy::load();
		$this->archive = $zippy->open( $this->getArchivePath() );
	}

	private function cleanArchive() {
		$this->loadArchive();
		$this->archive->removeMembers( array( 'dummy.txt' ) );
	}

	private function getPath() {
		return Yii::getAlias( '@webroot/uploads/works-sorted/' );
	}

	private function getArchivePath() {
		return Yii::getAlias( '@webroot/uploads/'.self::ARCHIVE_NAME );
	}

	/**
	 * @param int $offset
	 */
	public function actionAdd() {

		if ( ! Yii::$app->request->isAjax ) {
			return 'error';
		}

		$offset = (int) Yii::$app->request->post( 'offset' );

		$works = Work::find()->limit( 3 )->offset( $offset )->all();
		ini_set( 'max_execution_time', 0 );
		$this->loadArchive();


		foreach ( $works as $work ) {
			$nomination_name = $this->sanitize_file_name( $work->nominationName );

			$path      = $nomination_name . '/';
			$info_path = $path . 'info/';


			$info_file = Yii::getAlias( '@webroot/uploads/works-sorted/' ) . 'info.txt';
			$handle = fopen( $info_file, 'w' ) or die( 'Cannot open file:  ' . $info_file );

			$regions = Participant::getRegionsWithCode();

			$index = isset( $regions[ $work->participant->region ] ) ? $work->participant->region : '00';

			$region = $regions[ $index ];

			$file_url = false;

			if ( $work->file_name ) {
				$file_url = $work->fileUrl;
			}


			$work_url = Url::to( [ '/work/view', 'id' => $work->id ], true );

			$profileUrl = Url::to( [ '/participant/profile', 'id' => $work->participant->user_id ], true );

			$data = <<<INFO
Информация об участнике\r\n
========================\r\n
ID участника: {$work->participant->user_id}\r\n
ФИО: {$work->participant->name}\r\n
E-mail: {$work->user->email}\r\n
СМИ: {$work->participant->smi_name}\r\n
Сайт: {$work->participant->smi_url}\r\n
Тип сми: {$work->participant->smi_type_readable}\r\n
Регион: {$region}\r\n
URL участника на сайте конкурса: {$profileUrl}\r\n
URL логотипа издания: {$work->getlogoUrl()}\r\n
URL фото учаcтника: {$work->getPictureUrl()}\r\n
\r\n
Информация о работе\r\n
========================\r\n
Номинация: {$work->nominationName}\r\n
Описание работы: {$work->title}\r\n
\r\n
Оценить работу: {$work_url}\r\n
\r\n
URL файла работы: {$file_url}
INFO;


			fwrite( $handle, $data );
			fclose( $handle );

			$this->archive->addMembers( array(
				$info_path . $work->id . ' - информация.txt' => $info_file
			) );

			if ( $work->file_name && file_exists( $work->getFilePath() ) ) {
				$this->archive->addMembers( array(
					$path . $work->id . '.' . $work->file_type => $work->getFilePath()
				) );
			}
			if ( $work->picture && file_exists( $work->getPicturePath() ) ) {
				$this->archive->addMembers( array(
					$info_path . $work->id . ' - фото участника.' . pathinfo( $work->picture,
						PATHINFO_EXTENSION ) => $work->getPicturePath()
				) );
			}
			if ( $work->logo && file_exists( $work->getLogoPath() ) ) {
				$this->archive->addMembers( array(
					$info_path . $work->id . ' - логотип СМИ.' . pathinfo( $work->logo,
						PATHINFO_EXTENSION ) => $work->getLogoPath()
				) );

			}

			$offset ++;

		}

		Yii::$app->response->format = Response::FORMAT_JSON;

		return [ 'offset' => $offset ];

	}

	public function actionCopy() {

		if ( ! Yii::$app->request->isAjax ) {
			return 'error';
		}

		$offset = (int) Yii::$app->request->post( 'offset' );

		Yii::$app->response->format = Response::FORMAT_JSON;

		if ( $offset == - 1 ) {
			$this->zipFolder();
			return ['offset' => -1];
		} elseif ( $offset == 0 ) {
			$files_path = $this->getPath();
			FileHelper::removeDirectory( $files_path );
			FileHelper::createDirectory( $files_path );
			if (is_file($this->getArchivePath())) {
				unlink($this->getArchivePath());
			}
		}

		$works = Work::find()->limit( 3 )->offset( $offset )->all();
		ini_set( 'max_execution_time', 0 );


		foreach ( $works as $work ) {
			$nomination_name = $this->sanitize_file_name( $work->nominationName );

			$path      = $this->getPath() . $nomination_name . '/';
			$info_path = $path . 'info/';
			FileHelper::createDirectory( $info_path );


			$info_file = $info_path . $work->id . ' - информация.txt';
			$handle = fopen( $info_file, 'w' ) or die( 'Cannot open file:  ' . $info_file );

			$regions = Participant::getRegionsWithCode();

			$index = isset( $regions[ $work->participant->region ] ) ? $work->participant->region : '00';

			$region = $regions[ $index ];

			$file_url = false;

			if ( $work->file_name ) {
				$file_url = $work->fileUrl;
			}


			$work_url = Url::to( [ '/work/view', 'id' => $work->id ], true );

			$profileUrl = Url::to( [ '/participant/profile', 'id' => $work->participant->user_id ], true );

			$data = <<<INFO
Информация об участнике\r\n
========================\r\n
ID участника: {$work->participant->user_id}\r\n
ФИО: {$work->participant->name}\r\n
СМИ: {$work->participant->smi_name}\r\n
Сайт: {$work->participant->smi_url}\r\n
Тип сми: {$work->participant->smi_type_readable}\r\n
Регион: {$region}\r\n
URL участника на сайте конкурса: {$profileUrl}\r\n
URL логотипа издания: {$work->getlogoUrl()}\r\n
URL фото учаcтника: {$work->getPictureUrl()}\r\n
\r\n
Информация о работе\r\n
========================\r\n
Номинация: {$work->nominationName}\r\n
Описание работы: {$work->title}\r\n
\r\n
Оценить работу: {$work_url}\r\n
\r\n
URL файла работы: {$file_url}
INFO;


			fwrite( $handle, $data );
			fclose( $handle );

			if ( $work->file_name && file_exists( $work->getFilePath() ) ) {
				copy( $work->getFilePath(), $path . $work->id . '.' . $work->file_type );
			}
			if ( $work->picture && file_exists( $work->getPicturePath() ) ) {
				copy( $work->getPicturePath(), $info_path . $work->id . ' - фото участника.' . pathinfo( $work->picture,
						PATHINFO_EXTENSION ) );
			}
			if ( $work->logo && file_exists( $work->getLogoPath() ) ) {

				copy( $work->getLogoPath(), $info_path . $work->id . ' - логотип СМИ.' . pathinfo( $work->logo,
						PATHINFO_EXTENSION ) );
			}

			$offset ++;

		}


		return [ 'offset' => $offset ];

	}

	public function zipFolder() {
		$this->createArchive();
		$this->archive->addMembers( array(
			'.' => $this->getPath()
		) );

		$this->cleanArchive();
		FileHelper::removeDirectory( $this->getPath() );
	}

	function sanitize_file_name( $filename ) {
		$filename_raw  = $filename;
		$special_chars = array(
			"?",
			"[",
			"]",
			"/",
			"\\",
			"=",
			"<",
			">",
			":",
			";",
			",",
			"'",
			"\"",
			"&",
			"$",
			"#",
			"*",
			"(",
			")",
			"|",
			"~",
			"`",
			"!",
			"{",
			"}"
		);
		$filename      = str_replace( $special_chars, '', $filename );
		$filename      = preg_replace( '/[\s]+/', ' ', $filename );
		$filename      = preg_replace( '/[-]+/', '-', $filename );
		$filename      = trim( $filename, '.-_' );

		return $filename;
	}
}