<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\search\User as UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller {
    public function behaviors() {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => [ 'post' ],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionIndex() {
        if (!\Yii::$app->user->can('manageUsers')) {
            throw new NotFoundHttpException( 'Страница не найдена' );
        }
        $searchModel  = new UserSearch();
        $dataProvider = $searchModel->search( Yii::$app->request->queryParams );

        return $this->render( 'index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ] );
    }

    /**
     * Displays a single User model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView( $id ) {
        if (!\Yii::$app->user->can('manageUsers')) {
            throw new NotFoundHttpException( 'Страница не найдена' );
        }
        return $this->render( 'view', [
            'model' => $this->findModel( $id ),
        ] );
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionCreate() {
        if (!\Yii::$app->user->can('manageUsers')) {
            throw new NotFoundHttpException( 'Страница не найдена' );
        }
        $model = new User();
        $model->scenario = 'admin';

        if ( $model->load( Yii::$app->request->post() ) &&
             $model->save() ) {
            return $this->redirect( [ 'view', 'id' => $model->id ] );
        } else {
            return $this->render( 'create', [
                'model' => $model,
            ] );
        }
    }

    public function actionProfile( $id = null ) {

        // if guest - redirect to login page
        if ( \Yii::$app->user->isGuest ) {
            return $this->redirect( [ 'site/login' ] );
            // if user not activated, redirect to activation page
        } else if ( ! \Yii::$app->user->identity->isActivated() ) {
            return $this->redirect( [ 'site/verify' ] );
        }

        if ( ! $id || !\Yii::$app->user->can( 'manageUsers', [ 'id' => $id ] ) ) {
            $id = \Yii::$app->user->identity->id;
        }

        $model = self::findModel( $id );

        switch ( $model->group ) {
            case User::GROUP_PARTICIPANT:
                return $this->redirect( [ 'participant/profile', 'id' => $id ] );
                break;
            default:
                return $this->redirect( [ 'site/index' ] );
        }

    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate( $id ) {
        if (!\Yii::$app->user->can('manageUsers')) {
            throw new NotFoundHttpException( 'Страница не найдена' );
        }

        $model             = $this->findModel( $id );
        $model->scenario = 'admin';
        $old_password_hash = $model->password_hash;

        if ( $model->load( Yii::$app->request->post() ) ) {
            if ( ! $model->password ) {
                $model->password_hash = $old_password_hash;
            } else {
                $model->setPassword( $model->password );
            }

            if ( $model->save() ) {
                return $this->redirect( [ 'view', 'id' => $model->id ] );
            }
        }

        return $this->render( 'update', [
            'model' => $model,
        ] );

    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionDelete( $id ) {

        if (!\Yii::$app->user->can('manageUsers')) {
            throw new NotFoundHttpException( 'Страница не найдена' );
        }
        $this->findModel( $id )->delete();

        return $this->redirect( [ 'index' ] );
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel( $id ) {
        if ( ( $model = User::findOne( $id ) ) !== null ) {
            return $model;
        } else {
            throw new NotFoundHttpException( 'Страница не найдена.' );
        }
    }
}
