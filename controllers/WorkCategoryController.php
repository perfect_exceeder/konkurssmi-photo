<?php

namespace app\controllers;

use Yii;
use app\models\WorkCategory;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * WorkCategoryController implements the CRUD actions for WorkCategory model.
 */
class WorkCategoryController extends Controller {
    public function behaviors() {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => [ 'post' ],
                ],
            ],
        ];
    }

    /**
     * Lists all WorkCategory models.
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionIndex() {
        if ( ! \Yii::$app->user->can( 'manageKonkurs' ) ) {
            throw new NotFoundHttpException( 'Страница не найдена' );
        }
        $dataProvider = new ActiveDataProvider( [
            'query' => WorkCategory::find(),
        ] );

        return $this->render( 'index', [
            'dataProvider' => $dataProvider,
        ] );
    }

    /**
     * Displays a single WorkCategory model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView( $id ) {
        if ( ! \Yii::$app->user->can( 'manageKonkurs' ) ) {
            throw new NotFoundHttpException( 'Страница не найдена' );
        }

        return $this->render( 'view', [
            'model' => $this->findModel( $id ),
        ] );
    }

    /**
     * Creates a new WorkCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionCreate() {
        if ( ! \Yii::$app->user->can( 'manageKonkurs' ) ) {
            throw new NotFoundHttpException( 'Страница не найдена' );
        }
        $model = new WorkCategory();

        if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            return $this->redirect( [ 'view', 'id' => $model->id ] );
        } else {
            return $this->render( 'create', [
                'model' => $model,
            ] );
        }
    }

    /**
     * Updates an existing WorkCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate( $id ) {
        if ( ! \Yii::$app->user->can( 'manageKonkurs' ) ) {
            throw new NotFoundHttpException( 'Страница не найдена' );
        }
        $model = $this->findModel( $id );

        if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            return $this->redirect( [ 'view', 'id' => $model->id ] );
        } else {
            return $this->render( 'update', [
                'model' => $model,
            ] );
        }
    }

    /**
     * Deletes an existing WorkCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionDelete( $id ) {
        if ( ! \Yii::$app->user->can( 'manageKonkurs' ) ) {
            throw new NotFoundHttpException( 'Страница не найдена' );
        }
        $this->findModel( $id )->delete();

        return $this->redirect( [ 'index' ] );
    }

    /**
     * Finds the WorkCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return WorkCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel( $id ) {
        if ( ( $model = WorkCategory::findOne( $id ) ) !== null ) {
            return $model;
        } else {
            throw new NotFoundHttpException( 'Страница не найдена.' );
        }
    }
}
