<?php

namespace app\commands;

use Yii;
use yii\console\Controller;

/**
 * This command creates RBAC permissions and groups
 */
class RbacController extends Controller
{
    public function actionInit()
    {
	    $auth = Yii::$app->authManager;
	    $auth->removeAll();

        $userGroupRule = new \app\rbac\UserGroupRule;
        $auth->add($userGroupRule);

	    $addWork = $auth->createPermission('addWork');
        $addWork->description = 'Добавление работы';
	    $auth->add($addWork);

	    $indexOwnWorks = $auth->createPermission('indexOwnWorks');
        $indexOwnWorks->description = 'Просмотр собственных работ';
	    $auth->add($indexOwnWorks);

        $editWork = $auth->createPermission('editWork');
        $editWork->description = 'Редактирование работы';
	    $auth->add($editWork);
        $auth->addChild($editWork,$addWork);

        $rateWork = $auth->createPermission('rateWork');
        $rateWork->description = 'Оценивание работы';
	    $auth->add($rateWork);

        $addNews = $auth->createPermission('addNews');
        $addNews->description = 'Добавление/редактирование новостей';
	    $auth->add($addNews);

        $addKonkurs = $auth->createPermission('manageKonkurs');
        $addKonkurs->description = 'Добавление/редактирование конкурсов';
	    $auth->add($addKonkurs);

        $manageUsers = $auth->createPermission('manageUsers');
        $manageUsers->description = 'Управление пользователями';
	    $auth->add($manageUsers);

        // add the rule
        $ownProfileRule = new \app\rbac\OwnProfileRule();
        $auth->add($ownProfileRule);

        // add the "updateOwnProfile" permission and associate the rule with it.
        $updateOwnProfile = $auth->createPermission('updateOwnProfile');
        $updateOwnProfile->description = 'Update own profile';
        $updateOwnProfile->ruleName = $ownProfileRule->name;
        $auth->add($updateOwnProfile);

        // "updateOwnProfile" will be used from "manageUsers"
        $auth->addChild($updateOwnProfile, $manageUsers);

        // add the rule
        $ownWorkRule = new \app\rbac\OwnWorkRule();
        $auth->add($ownWorkRule);

        // add the "updateOwnWork" permission and associate the rule with it.
        $updateOwnWork = $auth->createPermission('updateOwnWork');
        $updateOwnWork->description = 'Update own Work';
        $updateOwnWork->ruleName = $ownWorkRule->name;
        $auth->add($updateOwnWork);

        // "updateOwnWork" will be used from "manageUsers"
        $auth->addChild($updateOwnWork, $editWork);




	    $participant = $auth->createRole('participant');
        $participant->ruleName = $userGroupRule->name;
	    $auth->add($participant);
	    $auth->addChild($participant, $addWork);
	    $auth->addChild($participant, $indexOwnWorks);
        // allow "author" to update their own posts
        $auth->addChild($participant, $updateOwnProfile);
        $auth->addChild($participant, $updateOwnWork);

	    $jury = $auth->createRole('jury');
        $jury->ruleName = $userGroupRule->name;
	    $auth->add($jury);
	    $auth->addChild($jury, $rateWork);
        $auth->addChild($jury, $updateOwnProfile);

        $contentManager = $auth->createRole('manager');
        $contentManager->ruleName = $userGroupRule->name;
        $auth->add($contentManager);
	    $auth->addChild($contentManager, $addNews);

	    $admin = $auth->createRole('admin');
        $admin->ruleName = $userGroupRule->name;
	    $auth->add($admin);
//        $auth->addChild($admin, $jury);
        $auth->addChild($admin, $contentManager);
        $auth->addChild($admin, $editWork);
        $auth->addChild($admin, $manageUsers);
        $auth->addChild($admin, $addKonkurs);


        \Yii::$app->cache->delete('rbac');

    }

}
