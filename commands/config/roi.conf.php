<?php
/**
 * bp.conf.php
 * Created by h8every1 on 04.07.2016 16:39
 */

return [
	'metrika_id' => 38430980,
	'pages'       => [
		'',
		'/?p=99',
		'/?cat=14',
		'/?page_id=2',
		'/?cat=8',
		'/?cat=15',
		'/?cat=9',
		'/?cat=14',
		'/?p=193',
		'/?p=185',
		'/?p=151',
		'/?p=75',
		'/?p=70',
		'/?p=188',
		'/?p=172',
		'/?p=169',
		'/?p=166',
		'/?p=160',
		'/?p=147',
	],
	'site_prefix' => 'http://rosobrinfo.ru',
	'visitors_count' => 12,
	'min_sleep_time' => 3,
	'max_sleep_time' => 32,
];