<?php
/**
 * bp.conf.php
 * Created by h8every1 on 04.07.2016 16:39
 */

return [
	'metrika_id' => 38445260,
	'pages'       => [
		'',
		'/news/novosti_obrazovaniya/testovoe-ispoljzovanie-gosudarstvennoj-informatsionnoj-sistemy-v-shkolax-novosibirska-na-60--snizilo-nagruzki-na-uchitelej/',
		'/news/novosti_shkol/gibdd-provelo-rejd--snova-v-shkolu--v-preddverii-nachala-uchebnogo-goda/',
		'/news/novosti_obrazovaniya/',
		'/news/',
		'/news/sobytiya/',
		'/photo/',
		'/photo/183/',
		'/photo/182/',
		'/video/',
		'/info/',
		'/info/kak-karapushki-uchilis-ponimat-drug-druga/',
		'/info/istorii-karapushek-kak-zhit-v-mire-s-soboy-i-drugimi/',
		'/news/novosti_obrazovaniya/testovoe-ispoljzovanie-gosudarstvennoj-informatsionnoj-sistemy-v-shkolax-novosibirska-na-60--snizilo-nagruzki-na-uchitelej/',
		'/discuss/The-role-of-the-Director-in-modern-school/',
	],
	'site_prefix' => 'http://новости-школ.рф',
//	'site_prefix' => 'http://xn----ctbsjfhhbd0al8e.xn--p1ai',
	'visitors_count' => 14,
	'min_sleep_time' => 5,
	'max_sleep_time' => 45,
	
];