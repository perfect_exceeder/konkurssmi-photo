<?php
/**
 * bp.conf.php
 * Created by h8every1 on 04.07.2016 16:39
 */

return [
	'metrika_id' => 36765260,
	'pages'       => [
		'',
		'/?page_id=2',
		'/?cat=2',
		'/?p=90',
		'/?p=87',
		'/?cat=3',
		'/?p=81',
		'/?cat=4',
		'/?paged=2',
		'/?cat=5',
		'/?p=68',
		'/?p=71',
		'/?p=51',
		'/?p=56',
	],
	'site_prefix' => 'http://www.znaem-mozhem.ru',
	'visitors_count' => 13,
	'min_sleep_time' => 5,
	'max_sleep_time' => 45,
];