<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\Work;
use app\models\YoutubeUpload;
use yii\console\Controller;


/**
 * This controller handles the youtube uploads
 *
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class YoutubeController extends Controller {

    public function getYoutubeClient() {
        return new YoutubeUpload();
    }

    /**
     * Tries to upload three videos to Youtube
     */
    public function actionUpload() {
        \Yii::setAlias( '@webroot', '@app/web' );

        $works = Work::find()->byWorkType(Work::TYPE_TV)->not_loaded()->limit( 3 )->all();

        if ( count( $works ) ) {
            $youtube = $this->getYoutubeClient();

            foreach ( $works as $work ) {
                echo 'Uploading file "' . $work->title . "\"\n";
                echo 'from ' . $work->getFilePath() . "\n";

                $args  = [
                    'path'        => $work->getFilePath(),
                    'title'       => $work->title,
                    'description' => 'Работа на конкурс ПРО-Образование',
                ];
                $video = $youtube::uploadVideo( $args );

                if ( ! $video['error'] ) {

                    $youtube::addVideoToPlaylist($video['id'],'PLfMbYfPqLwZiwkX9fxENgS0KATf92tUbp');
                    $work->on_remote = true;
                    $work->remote_id = $video['id'];
                    $work->deleteAllFiles();
                    $work->save( false );

                    echo 'File uploaded to youtube. ID = ' . $video['id'] . "\n";
                } else {
                    echo 'Failed to upload file. Error: ' . $video['error_msg'] . "\n";
                }
            }
        }

        return Controller::EXIT_CODE_NORMAL;
    }

    /**
     * Show URL for getting the access code
     */
    public function actionAuthUrl() {
        $youtube = $this->getYoutubeClient();
        echo $youtube::getAuthUrl();
    }

    /**
     * Set token data. Accepts code from Youtube
     *
     * @param string $code Youtube code
     */
    public function actionSetToken( $code = null ) {
        $youtube = $this->getYoutubeClient();
        var_dump( $youtube::getAccessTokenByCode( $code ) );
    }

}
