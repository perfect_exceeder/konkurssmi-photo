<?php
/**
 * MetrikaController.php
 * Created by h8every1 on 16.06.2016 23:33
 */

namespace app\commands;

use linslin\yii2\curl;
use yii\base\ErrorException;
use yii\console\Controller;
use yii\helpers\FileHelper;

class MetrikaController extends Controller {

	const IS_DEBUG = false;

	public $cookie_path;
	public $cookie_file;

	public $site_prefix;
	public $metrika_id;

	public $useragents;
	public $pages;
	public $visitors_count;
	public $min_sleep_time = 7;
	public $max_sleep_time = 52;


	public $is_new_visitor;
	public $user_agent;
	public $current_page;

	public function init() {
		parent::init();
		$this->cookie_path = __DIR__.'/cookies/';
		FileHelper::createDirectory( $this->cookie_path );

	}

	/**
	 * @param mixed|string $args Array of setttings or filename with such an array
	 *
	 * @throws ErrorException
	 */
	public function loadSettings( $args = null ) {

		if ( ! $args ) {
			$args = [ ];
		}

		// load data from filename
		if ( ! is_array( $args )
		     && count( FileHelper::findFiles( __DIR__.'/config/',[ 'only' => [ $args . '.conf.php' ] ] ) ) == 1)
		{
			$args = require( __DIR__ . '/config/'.$args.'.conf.php' );
		}

		if (!is_array($args)) {
			throw new ErrorException('Error in config');
		}

		$defaults = $this->getDefaultSettings();
		$args = array_replace( $defaults, $args );

		$this->site_prefix = $args['site_prefix'];
		$this->pages = $args['pages'];
		$this->metrika_id  = $args['metrika_id'];

		$this->useragents = $args['useragents'];

		$this->visitors_count = $args['visitors_count'];
		$this->min_sleep_time = $args['min_sleep_time'];
		$this->max_sleep_time = $args['max_sleep_time'];

	}

	private function getDefaultSettings() {
		return [
			'useragents'  => [
				'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36',
				'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.84 Safari/537.36',
				'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1',
				'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10; rv:33.0) Gecko/20100101 Firefox/33.0',
				'Opera/9.80 (X11; Linux i686; Ubuntu/14.10) Presto/2.12.388 Version/12.16',
				'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586',
				'Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko',
				'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0',
				'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.84 Safari/537.36 OPR/38.0.2220.31',
			],
			'pages'       => [
				'',
				'/polozhenie.html',
				'/site/news',
				'/site/nominations-list',
				'/site/login',
				'/site/request-password-reset'
			],
			'site_prefix' => 'http://photo.konkurssmi.ru',
			'metrika_id'  => 37999730,
			'visitors_count' => 5,
			'min_sleep_time' => 7,
			'max_sleep_time' => 52,
		];
	}

	/**
	 * Simulates visits to site pages in Yandex.Metrika
	 *
	 * @param int $count Number of virtual visitors
	 * @param int $min_sleep_time Minimum pause between changing addresses in one visit
	 * @param int $max_sleep_time Maximum pause between changing addresses in one visit
	 *
	 * @throws \yii\base\ErrorException
	 * @internal param string $message the message to be echoed.
	 */

	public function actionIndex( $data ) {
		$this->loadSettings( $data );

		$metrika_image = $this->getMetrikaUrl();

		for ( $i = 0; $i < $this->visitors_count; $i ++ ) {

			$this->setUserAgent();
			$this->setCookie();

			$this->delimiter();
			$this->log( 'Visit ' . ( $i + 1 ) . "/" . $this->visitors_count );
			$this->log( 'This is a ' . ( $this->is_new_visitor ? 'NEW' : 'RETURNING' ) . ' visitor' );
			$this->log( 'User-Agent: ' . $this->getUserAgent() );
			$this->nl();

			$should_continue = 1;

			while ( $should_continue ) {

				$this->log( 'Visiting ' . $this->setCurrentPage() );

				$curl = $this->setupCurl();
				$curl->get( $metrika_image );

				$curl_info = $curl->getInfo();

				$this->log( 'Response code ' . $curl->responseCode );

				if ( $curl_info['redirect_count'] ) {
					$this->log( 'Redirect count: ' . $curl_info['redirect_count'] );
				}

				$curl->reset();

				$should_continue = mt_rand( 0, 10 ) <= 6;

				$sleep = mt_rand( $this->min_sleep_time, $this->max_sleep_time );
				if ( $should_continue && $sleep ) {
					$this->log( 'Sleeping for ' . $sleep . ( $sleep > 1 ? " seconds" : " second" ) );
					sleep( $sleep );
				}

				$this->nl();
			}

			$this->nl();
		}

	}

	/**
	 * @return mixed
	 */
	public function getCurrentPage() {
		return $this->current_page;
	}

	/**
	 *
	 */
	public function setCurrentPage() {
		$current = $this->getCurrentPage();

		do {
			$this->current_page = $this->site_prefix . $this->pages[ array_rand( $this->pages ) ];
		} while ( $current == $this->current_page );

		return $this->getCurrentPage();
	}

	/**
	 * @return mixed
	 */
	public function getUserAgent() {
		return $this->user_agent;
	}

	/**
	 *
	 */
	public function setUserAgent() {
		$this->user_agent = $this->useragents[ array_rand( $this->useragents ) ];

		return $this->getUserAgent();
	}

	/**
	 * Prepare alll CURL options
	 *
	 * @return curl\Curl
	 */
	private function setupCurl() {
		$curl = new curl\Curl();

		$header[] = "Connection: Keep-Alive";
		$header[] = "Accept: text/html";
		$curl->setOption( CURLOPT_HTTPHEADER, $header );


		$curl->setOption( CURLOPT_REFERER, $this->getCurrentPage() );
		$curl->setOption( CURLOPT_USERAGENT, $this->getUserAgent() );
		$curl->setOption( CURLOPT_COOKIEFILE, $this->cookie_file );
		$curl->setOption( CURLOPT_COOKIEJAR, $this->cookie_file );
		$curl->setOption( CURLOPT_SSL_VERIFYPEER, false );

		$curl->setOption( CURLOPT_FOLLOWLOCATION, true );
		$curl->setOption( CURLOPT_RETURNTRANSFER, true );

//			$curl->setOption( CURLOPT_AUTOREFERER, true );
		$curl->setOption( CURLOPT_AUTOREFERER, false );

		$curl->setOption( CURLOPT_TIMEOUT, 10 );

//			$curl->setOption( CURLOPT_HEADER, true);
		$curl->setOption( CURLINFO_HEADER_OUT, true );


		return $curl;


	}

	/**
	 * Logs message to console with timestamp appended
	 *
	 * @param $msg string Message
	 */
	private function log( $msg ) {
		if (self::IS_DEBUG) {
			echo '[' . date( 'Y-m-d H:i:s' ) . '] ' . $msg;
			$this->nl();
		}
	}

	private function delimiter() {
		if (self::IS_DEBUG) {
			echo '==================================';
			$this->nl();
		}
	}

	private function nl() {
		if (self::IS_DEBUG) {
			echo "\n";
		}
	}



	public function setCookie() {
		// make ~30% of new visitors
		$this->is_new_visitor = mt_rand( 0, 100 ) < 30;

		if ( ! $this->is_new_visitor ) {
			$all_files = FileHelper::findFiles( $this->cookie_path, [ 'only' => [ 'cookie_*.txt' ] ] );

			if ( count( $all_files ) ) {
				$this->cookie_file = $all_files[ array_rand( $all_files ) ];
			} else {
				$this->is_new_visitor = true;
			}
		}

		if ( $this->is_new_visitor ) {
			$this->cookie_file = $this->cookie_path . '/cookie_' . \Yii::$app->security->generateRandomString( 6 ) . '.txt';;
		}

		return $this->cookie_file;
	}

	/**
	 * @return string
	 */
	public function getMetrikaUrl() {
		return 'https://mc.yandex.ru/watch/' . $this->metrika_id;
	}
}