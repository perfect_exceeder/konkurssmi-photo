<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\models\User */
/* @var $work app\models\Work */

$editLink = Yii::$app->urlManager->createAbsoluteUrl(['work/update', 'id' => $work->id]);
$listLink = Yii::$app->urlManager->createAbsoluteUrl(['work/index']);
?>
<div class="work-added">
    <p>Здравствуйте!</p>

    <p>Работа «<?= $work->title;?>» была добалена на сайт.</p>

    <p>Чтобы изменить работу, перейдите по этой ссылке - <?= Html::a(Html::encode($editLink), $editLink) ?></p>

    <p>Список всех Ваших работ доступен по адресу - <?= Html::a(Html::encode($listLink), $listLink) ?></p>
</div>