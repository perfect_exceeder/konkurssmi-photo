<?php

/* @var $this yii\web\View */
/* @var $work app\models\Work */

$editLink = Yii::$app->urlManager->createAbsoluteUrl( [ 'work/update', 'id' => $work->id ] );
$listLink = Yii::$app->urlManager->createAbsoluteUrl( [ 'work/index' ] );
?>
Здравствуйте!

Работа «<?= $work->title; ?>» была добалена на сайт.

Чтобы изменить работу, перейдите по этой ссылке - <?= $editLink; ?>

Список всех Ваших работ доступен по адресу - <?= $listLink; ?>
