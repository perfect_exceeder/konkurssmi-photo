<?php
/**
 * ViewHelper.php
 * Created by h8every1 on 26.06.2015 3:23
 */

namespace app\helpers;

class ViewHelper {

    public static function title($text) {
        return \Yii::$app->view->renderFile('@partitials/title.php',['title' => $text]);
    }
    public static function breadcrumbs() {
        return \Yii::$app->view->renderFile('@partitials/breadcrumbs.php');
    }
}