<?php
/**
 * ArrayHelper.php
 * Created by h8every1 on 22.06.2015 19:16
 */

namespace app\helpers;

class ArrayHelper {
    /**
     *
     * Makes the array one-dimensional
     *
     * @param array $array
     *
     * @return array
     */
    public static function flatten( $array ) {
        $return = array();
        foreach ( $array as $key => $value ) {
            if ( is_array( $value ) ) {
                $return = $return + static::flatten( $value );
            } else {
                $return[ $key ] = $value;
            }
        }
        return $return;
    }
}