<?php
/**
 * LightboxAsset.php
 * Created by h8every1 on 01.07.2016 22:01
 */

namespace app\assets;


use yii\web\AssetBundle;

class LightboxAsset extends AssetBundle  {
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	public $css = [
//		'css/style.css',
//        'css/style.less',
	];
	public $js = [
		'js/jquery.magnific-popup.min.js',
		'js/magnific-popup.js',
	];
	public $depends = [
		'yii\web\YiiAsset',
	];
}