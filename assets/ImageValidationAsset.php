<?php
/**
 * ImageValidationAsset.php
 * Created by h8every1 on 25.06.2015 21:43
 */

namespace app\assets;

use yii\web\AssetBundle;

class ImageValidationAsset extends AssetBundle {
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    ];
    public $js = [
        'js/image.validation.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
