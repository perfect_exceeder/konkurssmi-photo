<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

class ModernizrAsset extends AssetBundle {
    public $baseUrl = '@web';
    public $js = [
        'js/modernizr/modernizr.js',
    ];
    public $jsOptions = [ 'position' => View::POS_HEAD ];
}
