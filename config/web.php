<?php

$env_params_path = __DIR__ . '/params.' . YII_ENV . '.php';
$env_config      = [ ];
if ( file_exists( $env_params_path ) ) {
    $env_config = require( $env_params_path );
}

$config = [
    'id'             => 'photokonkurs',
    'language'       => 'ru-RU',
    'sourceLanguage' => 'ru-RU',
    'name'           => 'Фотоконкурс «Фото PRO Образование»',
    'basePath'       => dirname( __DIR__ ),
    'bootstrap'      => [ 'log' ],
    'aliases'        => [
        '@partitials' => '@app/views/layouts/partitials'
    ],
    'defaultRoute'   => 'site/index',
    'components'     => [
        'cache'        => [
            'class' => 'yii\caching\FileCache',
        ],
        'user'         => [
            'identityClass'   => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer'       => [
            'class'            => 'yii\swiftmailer\Mailer',
        ],
        'log'          => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => [ 'error', 'warning' ],
                ],
            ],
        ],
        'urlManager'   => [
            'enablePrettyUrl'     => true,
            'showScriptName'      => false,
            'enableStrictParsing' => false,
            'rules'               => [
                '/' => 'site/index',
                [
                    'pattern'  => 'profile/<id:\d+>',
                    'route'    => 'user/profile',
                    'defaults' => [ 'id' => null ],
                ],
                [
                    'pattern'  => 'participant/register/<type:\d+>',
                    'route'    => 'participant/register',
                    'defaults' => [ 'type' => 1 ],
                ],
                [
                    'pattern'  => 'participant/<action:(view|profile|delete)>/<id:\d+>',
                    'route'    => 'participant/<action>',
                    'defaults' => [ 'id' => null ],
                ],
                [
                    'pattern'  => 'news/<slug>',
                    'suffix'   => '.html',
                    'route'    => 'article/view',
                    'defaults' => [ 'slug' => null, 'category' => 'news' ],
                ],
                [
                    'pattern'  => 'expert/<slug>',
                    'suffix'   => '.html',
                    'route'    => 'article/view',
                    'defaults' => [ 'slug' => null, 'category' => 'expert' ],
                ],
                [
                    'pattern'  => '<slug>',
                    'suffix'   => '.html',
                    'route'    => 'article/view',
                    'defaults' => [ 'slug' => null ],
                ],
//                '<action:\w+>' => 'site/<action>',

            ]
        ],
        'authManager'  => [
            'class'        => 'yii\rbac\DbManager',
            'defaultRoles' => [ 'admin', 'manager', 'jury', 'participant' ],
            'cache'        => 'cache',
        ],
        'assetManager' => [
            'converter' => [
                'class'    => 'yii\web\AssetConverter',
                'commands' => [
                    'less' => [ 'css', 'lessc {from} {to} --no-color --compress' ],
                ],
            ],
            'bundles'   => [
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [ ]
                ],
            ],
        ],
        'db'           => require( __DIR__ . '/db.php' ),
    ],
//    'params'         => $params,
];

$config = array_replace_recursive( $config, $env_config );

if ( YII_ENV_DEV ) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][]      = 'debug';
    $config['modules']['debug'] = [
        'class'      => 'yii\debug\Module',
        'allowedIPs' => [ '185.52.142.212','185.52.140.178', '127.0.0.1', '::1', '192.168.0.*', '*' ]
    ];


    $config['bootstrap'][]    = 'gii';
    $config['modules']['gii'] = [
        'class'      => 'yii\gii\Module',
        'allowedIPs' => [ '185.52.142.212','185.52.140.178', '127.0.0.1', '::1', '192.168.0.*', '*' ] // adjust this to your needs
    ];


}


$config['modules']['gridview'] = [
    'class' => '\kartik\grid\Module'
    // enter optional module parameters below - only if you need to
    // use your own export download action or custom translation
    // message source
    // 'downloadAction' => 'gridview/export/download',
    // 'i18n' => []
];

return $config;