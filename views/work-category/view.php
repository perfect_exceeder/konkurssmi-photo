<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\WorkCategory */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Категории конкурсных работ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-category-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы точно хотите удалить эту категорию?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'max_file_size',
            'file_extensions',
            'file_text',
        ],
    ]) ?>

</div>
