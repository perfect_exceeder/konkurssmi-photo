<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\WorkCategory */

$this->title                   = 'Обновлении категории работ «' . $model->title . '»';
$this->params['breadcrumbs'][] = [ 'label' => 'Категории конкурсных работ', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = [ 'label' => $model->title, 'url' => [ 'view', 'id' => $model->id ] ];
$this->params['breadcrumbs'][] = 'Обновление';
?>
<div class="work-category-update">

    <h1><?= Html::encode( $this->title ) ?></h1>

    <?= $this->render( '_form', [
        'model' => $model,
    ] ) ?>

</div>
