<?php
/**
 * _file-extra.php
 * Created by h8every1 on 19.06.2015 18:10
 */
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $size integer */
/* @var $exts string */
/* @var $additional string */
?>Максимальный размер файла - <?= $size; ?>МБ, разрешенные типы файлов - <?= $exts; ?>
<?php if ($additional) { ?>
<div class="additional">
    <strong>Дополнительные ограничения к файлу:</strong><br>
    <?= nl2br($additional); ?>
</div>
<?php }
