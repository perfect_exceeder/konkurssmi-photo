<?php

use kartik\slider\Slider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Work */
/* @var $form yii\widgets\ActiveForm */
/* @var $can_edit bool */

$categories    = ArrayHelper::map( \app\models\MarkCategory::find()->where(['participant_type'=>$model->participant->type])->asArray()->all(), 'id', 'name' );
$already_rated = true;
$user_marks    = $model->getAuthorWorkMarks( \Yii::$app->user->id )->orderBy( 'mark_category_id' )->all();

if ( ! $user_marks ) {
    $already_rated = false;
    $user_marks    = [ ];

    foreach ( $categories as $id => $name ) {
        $work_cat_mark                   = new \app\models\WorkMark();
        $work_cat_mark->mark_category_id = $id;
        $work_cat_mark->work_id          = $model->id;
        $work_cat_mark->user_id          = \Yii::$app->user->id;

        $user_marks[] = $work_cat_mark;
    }
}

?>

<div class="b-rating">
    <?php if ( ! $already_rated || $can_edit ) { ?>
        <h4>Оценить работу</h4>
        <?php $form = ActiveForm::begin(); ?>
        <?php
        if ( $can_edit ) {
            $cat_id = $model->nomination;

            $juries = \app\models\JuryCategory::find()->where( [ 'category_id' => $cat_id ] )
                                              ->with(
                                                  [
                                                      'jury' => function ( $query ) {
                                                          $query->orderBy( 'name ASC' );
                                                      },
                                                  ]
                                              )
                                              ->asArray()->all();

//            $jury_array =

            $juries_array = ArrayHelper::map( $juries, 'jury.user_id', 'jury.name' );
            asort($juries_array);
            echo Html::dropDownList( 'user_id', null,
                $juries_array,
//                [ 'prompt' => '- Член жюри -' ]
                [ ]

            );
        }
        ?>
        <?php
        foreach ( $user_marks as $mark ) {
            echo $form->field( $mark,
                '[' . $mark->mark_category_id . ']mark' )
                      ->widget( Slider::classname(), [
                          'sliderColor'   => Slider::TYPE_GREY,
                          'handleColor'   => Slider::TYPE_INFO,
                          'pluginOptions' => [
                              'min'  => 1,
                              'max'  => 10,
                              'step' => 1,
//                          'handle'  => 'triangle',
//                          'tooltip' => 'always'
                          ],
                          'class'         => 'b-slider',
                      ] )
                      ->label( $categories[ $mark->mark_category_id ], ['class'=>'b-rating--category-desc'] );

        };
        ?>
        <?= Html::submitButton( 'Оценить', [ 'class' => 'btn btn-primary' ] ) ?>
        <?php ActiveForm::end();
    } else {
        ?>
        <h4>Ваши оценке работе:</h4>
        <?php
        foreach ( $user_marks as $mark ) {
            ?>
            <div class="b-rating--category-desc"><?= $categories[ $mark->mark_category_id ]; ?></div>
            <h2 class="b-rating--mark-text"><?= $mark->mark; ?></h2>
        <?php }
    }
    ?>
</div>