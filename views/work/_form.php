<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Work */
/* @var $form yii\widgets\ActiveForm */

$participant = isset( $model->participant ) ? $model->participant : ( \Yii::$app->user->identity->participant ? \Yii::$app->user->identity->participant : false );
?>

<div class="work-form">
	<?php $form = ActiveForm::begin( [
		'options' => [
			'enctype' => 'multipart/form-data',
		]
	] );

	echo $form->errorSummary( $model );

	if ( Yii::$app->user->can( 'editWork' ) ) {
		echo $form->field( $model, 'user_id' )->dropDownList(
			ArrayHelper::map( \app\models\Participant::find()->asArray()->all(), 'user_id', 'name' )
		);
	} ?>

	<?= $form->field( $model, 'file' )->fileInput()->label( 'Загрузите файл' ) ?>
	<?php if ( ! $model->isNewRecord && $model->file_name && $model->file_type ) { ?>
		<div class="b-img-preview">
			<img src="<?= $model->fileUrl; ?>" alt="" width="300">
		</div>
	<?php } ?>

	<?= $form->field( $model, 'nomination' )->dropDownList(
		$model->nominations
	); ?>
	<?= $form->field( $model, 'title', [ 'enableAjaxValidation' => true ] )->textarea( [ 'rows' => 4 ] ) ?>

	<div class="row">
		<?php if ( $participant ) { ?>
			<div class="col-sm-6">
				<?= $form->field( $model, 'picture_file' )->fileInput() ?>
				<?php if ( $model->picture ) { ?>
					<div class="b-img-preview">
						<img src="<?= $model->pictureUrl; ?>" alt="" width="300">
					</div>
				<?php } ?>
			</div>
		<?php } ?>
		<div class="col-sm-6">
			<?= $form->field( $model, 'logo_file' )->fileInput() ?>
			<?php if ( $model->logo ) { ?>
				<div class="b-img-preview">
					<img src="<?= $model->logoUrl; ?>" alt="" width="300">
				</div>
			<?php } ?>
		</div>
	</div>

	<div class="form-group">
		<?= Html::submitButton( $model->isNewRecord ? 'Добавить работу' : 'Изменить работу',
			[ 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary' ] ) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>