<?php
/**
 * end.php
 * Created by h8every1 on 09.10.2015 03:04
 */

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title                   = 'Прием работ завершен';
$this->params['breadcrumbs'][] = [ 'label' => 'Конкурсные работы', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">

    <div class="work-end">

        <h1><?= Html::encode( $this->title ) ?></h1>

        <p>Прием работ на конкурс завершен!</p>

    </div>
</div>