<?php
/**
 * added.php
 * Created by h8every1 on 19.06.2015 18:23
 */

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Работа добавлена';
$this->params['breadcrumbs'][] = ['label' => 'Конкурсные работы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>Ваша работа была успешно добавлена! Желаем удачи!</p>

</div>