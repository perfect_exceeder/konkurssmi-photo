<?php

use app\assets\ImageValidationAsset;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Work */
ImageValidationAsset::register( $this );

$this->title = 'Добавление работы';
?>
<div class="container">
    <div class="work-create">

        <h1><?= Html::encode( $this->title ) ?></h1>

        <?= $this->render( '_form', [
            'model' => $model,
        ] ) ?>

    </div>
</div>