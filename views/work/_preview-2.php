<?php
/**
 * _preview-1.php
 * Created by h8every1 on 29.06.2015 19:57
 */
//use yii\helpers\FileHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Work */

?>
<div class="b-player b-player__audio">
    <audio src="<?= Html::encode( $model->fileUrl ) ?>" controls preload="auto">

    </audio>
</div>