<?php

use app\helpers\ViewHelper;
use app\widgets\Alert;
use yii\base\ViewContextInterface;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Work */
/* @var $can_rate bool */
/* @var $can_edit bool */

$this->title                   = 'Оценка работы';
$this->params['breadcrumbs'][] = [ 'label' => 'Конкурсные работы', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = [ 'label' => $model->participant->smi_type ];

?>
<?= ViewHelper::breadcrumbs(); ?>
<div class="container">
    <?= ViewHelper::title( 'Работа №' . $model->id ); ?>
    <?= Alert::widget(['closeButton'=>false]) ?>
    <div class="row">
        <div class="col-lg-9">
            <div id="main">
                <section class="b-work-preview">
                    <img src="<?= Html::encode( $model->fileUrl ) ?>" alt="">
                </section>
                <strong>Описание:</strong>
                <?= \Yii::$app->getFormatter()->asParagraphs($model->title); ?>

            </div>
        </div>
        <div class="col-lg-3">
            <ul class="portfolio-item">
                <li><strong>Номинация:</strong> <?= $model->nominationName; ?></li>
            </ul>
            <?php if ( $can_edit || $can_rate ) {

                echo $this->render( '_rateForm', [
                    'model'    => $model,
                    'can_edit' => $can_edit
                ] );

            } ?>
        </div>
    </div>
    <?php if ( $can_edit ) { ?>
        <?= $this->render( '_workSummary', [
            'model' => $model
        ] ); ?>
    <?php } ?>
</div>