<?php

use app\models\Participant;
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use yii\helpers\Url;
use app\widgets\Alert;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\Work */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $can_edit bool */
/* @var $can_rate bool */

$this->title                   = 'Конкурсные работы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-index">

    <h1><?= Html::encode( $this->title ) ?></h1>
    <?= Alert::widget( [ 'closeButton' => false ] ) ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php
    if ( $can_edit ) {

        $columns_template = [
            'picture'          => [
                'attribute' => 'picture',
                'value'     => function ( $model ) {
                    /* @var $model \app\models\search\Work */
                    return $model->pictureUrl . ':url';
                },
                'format'    => 'raw',
            ],
        ];

        $excelGridColumns = [
            'id',
            [
                'attribute' => 'participant.region',
                'filter'    => Participant::getRegionsWithCode(),
                'value'     => function ( $model ) {
                    $regions = Participant::getRegionsWithCode();

                    $index = isset( $regions[ $model->participant->region ] ) ? $model->participant->region : '00';

                    return $regions[ $index ];
                },

            ],
            'participant.name:raw',
            [
                'attribute' => 'participant.smi_type',
                'filter'    => Participant::getSmiTypes(),
                'value'     => function ( $model ) {
                    return $model->participant->smi_type_readable;
                },
            ],
            'participant.smi_name:raw',
            $columns_template['picture'],
            [
                'attribute' => 'logo',
                'value'     => function ( $model ) {
                    /* @var $model \app\models\search\Work */
                    return $model->logoUrl . ':url';
                },
                'format'    => 'raw',
            ],
            [
                'attribute' => 'nomination',
                'value'     => 'nominationName'
            ],
            'title:nl2br',
            [
                'label'  => 'Ссылка на файл работы',
                'value'  => function ( $model ) {
                    /* @var $model \app\models\search\Work */
                    $result = false;

                    if ( $model->file_name ) {
                        $result = $model->fileUrl . ':url';
                    }


                    return $result;
                },
                'format' => 'raw'
            ],
            [
                'label'  => 'Оценить работу',
                'value'  =>
                    function ( $model ) {
                        /* @var $model \app\models\search\Work */
                        return Url::to( [ 'work/view', 'id' => $model->id ], true ) . ':url';
                    },
                'format' => 'raw'
            ],
            [
                'attribute' => 'marksSum',
                'label'     => 'Оценка',
            ]
        ];

        // Renders a export dropdown menu
        echo ExportMenu::widget( [
            'dataProvider'     => $dataProvider,
            'columns'          => $excelGridColumns,
            'dropdownOptions'  => [
                'label' => 'Экспорт списка работ',
                'class' => 'btn btn-primary'
            ],
            'filename'         => 'Список_работ',
            'onRenderDataCell' => function (
                PHPExcel_Cell $cell,
                $value,
                $model,
                $key,
                $index,
                \kartik\export\ExportMenu $export
            ) {
                $value = $cell->getValue();
                if ( strpos( $value, ':url' ) !== false ) {
                    $value = str_replace( ':url', '', $value );
                    $cell->getHyperlink()->setUrl( $value );
                    $cell->setValue( $value );
                }

            }
        ] );
    }
    // end EXPORT


    $columns = [ ];

    $columns[] = 'id';

    if ( $can_edit ) {
        $columns[] = [
            'attribute' => 'participant.name',
            'value'     => function ( $model ) {
                /* @var $model \app\models\Work */
                return Html::a( $model->participant->name . ' (' . $model->participant->smi_name . ')',
                    \yii\helpers\Url::to( [ 'participant/profile', 'id' => $model->user_id ] ) ).
                    '<br>'.Html::a( $model->user->email, 'mailto:'.$model->user->email );
            },
//                'filter'=>'participant.name',
            'format'    => 'html'
        ];
        $columns[] = [
            'attribute'     => 'picture',
            'value'         => function ( $model ) {
                /* @var $model \app\models\search\Work */
                return $model->participant->type === Participant::TYPE_JOURNALIST ? '<a href="' . $model->pictureUrl . '" target="_blank">' . Html::img( $model->pictureThumbUrl ) . '</a>' : ' - ';
            },
            'format'        => 'html',
            'enableSorting' => false,
            'filter'        => false
        ];
        $columns[] = [
            'attribute'     => 'logo',
            'value'         => function ( $model ) {
                /* @var $model \app\models\search\Work */
                return '<a href="' . $model->logoUrl . '" target="_blank">' . Html::img( $model->logoThumbUrl ) . '</a>';
            },
            'format'        => 'html',
            'enableSorting' => false,
            'filter'        => false
        ];
    }

    $columns[] = [
        'attribute' => 'title',
        'value'     => function ( $model ) {
            return
                Html::img( $model->fileThumbUrl ).'<br>'.
                \yii\helpers\StringHelper::truncateWords($model->title,'10','...',true) . ' ' . Html::a( '<span class="glyphicon glyphicon-eye-open"></span> Просмотр',
                [ 'view', 'id' => $model->id ], [ 'class' => 'btn btn-xs btn-success' ] );
        },
        'format'    => 'html'

    ];

    $columns[] = [
        'attribute' => 'participant.smi_type',
        'filter'    => Participant::getSmiTypes(),
        'value'     => function ( $model ) {
            return $model->participant->smi_type_readable;
        },
    ];


    $columns[] = [
        'attribute' => 'nomination',
        'filter'    => $searchModel::allNominations(),
        'value'     => function ( $model ) {
            return $model->nominationName;
        }
    ];

    if ( $can_edit ) {
        $columns[] = [
            'attribute' => 'has_marks',
            'label'     => 'Оценено?',
            'filter'    => [ 'Нет', 'Да' ],
            'value'     => function ( $model ) {
                return $model->marked;
            },
            'format'    => 'boolean'
        ];
    }

    if ( $can_edit ) {
        $columns[] = [
            'attribute' => 'marksSum',
            'label'     => 'Оценка',
        ];
        $columns[] = [
            'class' => yii\grid\ActionColumn::className(),
        ];
    }

    if ( $can_edit ) {
        echo Html::a('Скачать архив работ',['file/index'],['class'=>'btn btn-danger']);
    }
    ?>
    <?= GridView::widget( [
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => $columns,
    ] ); ?>

</div>