<?php
/**
 * _preview-3.php
 * Created by h8every1 on 29.06.2015 19:57
 */
use yii\helpers\FileHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Work */

?>
<div class="b-player b-player__video">
    <?php if ( $model->youtubeId ) { ?>
        <div class="embed-responsive embed-responsive-16by9">
            <iframe width="420" height="315" src="https://www.youtube.com/embed/<?= $model->youtubeId;?>" frameborder="0" allowfullscreen></iframe>
        </div>
    <?php } else { ?>
        Видео пока что не загрузилось на Youtube, пожалуйста поробуйте снова через полчаса.
    <?php } ?>
</div>