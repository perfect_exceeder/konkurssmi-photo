<?php

use app\models\Participant;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\Work */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $can_edit bool */
/* @var $can_rate bool */

$this->title                   = 'Ваши конкурсные работы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="work-index">

        <h1><?= Html::encode( $this->title ) ?></h1>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?php
        $columns = [ ];

        $columns[] = [
            'attribute' => 'title',
            'value'     => function ( $model ) {
                $result = \yii\helpers\StringHelper::truncateWords($model->title,'10','...',true);
                if ( ! $model->getWorkMarks()->count() ) {
                    $result .= ' ' . Html::a( 'Редактировать',
                            [ 'update', 'id' => $model->id ], [ 'class' => 'btn btn-sm btn-primary' ] );
                }

                return $result;
            },
            'format'    => 'html'

        ];

        $columns[] = [
            'attribute' => 'nomination',
            'filter'    => $searchModel::allNominations(),
            'value'     => function ( $model ) {
                return $model->nominationName;
            }
        ];

        ?>
        <?= GridView::widget( [
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => $columns,
        ] ); ?>
        <p>Вы не можете изменять работы, за которые уже проголосовали члены жюри</p>

    </div>
</div>