<?php
/**
 * _preview-1.php
 * Created by h8every1 on 29.06.2015 19:57
 */
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Work */

?>
<iframe class="b-work--document" src="http://docs.google.com/viewer?url=<?= Html::encode( $model->fileUrl ) ?>&embedded=true" width="100%"
        height="600"></iframe>