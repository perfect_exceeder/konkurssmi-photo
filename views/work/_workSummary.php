<?php

use kartik\slider\Slider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Work */
/* @var $form yii\widgets\ActiveForm */

$categories = ArrayHelper::map( \app\models\MarkCategory::find()->where(['participant_type'=>$model->participant->type])->asArray()->all(), 'id', 'name' );
$work_marks = ArrayHelper::map( $model->getWorkMarks()->with( 'jury' )->orderBy( 'user_id ASC, mark_category_id ASC' )->asArray()->all(),
    'mark_category_id', 'mark', 'jury.name' );

if ( ! $work_marks ) {
    return false;
}
$total_category = [ ];

?>
<div class="marg30">
    <h3>Сводная таблица</h3>
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>
                ФИО члена жюри
            </th>
            <?php foreach ( $categories as $id => $cat ) {
                $total_category[ $id ] = 0;
                ?>

                <th><?= \yii\helpers\StringHelper::truncateWords( $cat, 5 ); ?></th>
            <?php } ?>
            <th>Сумма по члену жюри</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ( $work_marks as $jury => $marks ) {
            ?>
            <tr>
                <th><?= $jury; ?></th>
                <?php
                $total_jury = 0;
                foreach ( $marks as $cat => $mark ) {
                    $total_jury += $mark;
                    $total_category[ $cat ] += $mark;
                    ?>
                    <td><?= $mark; ?></td>
                <?php
                }
                //$avg = round( $total_jury / count( $marks ), 3 );
                ?>
                <td><?= $total_jury; ?></td>
            </tr>
        <?php } ?>
        </tbody>
        <tfoot>
        <tr>
            <th>Сумма по категориям</th>
            <?php
            $total_work = 0;
            foreach ( $total_category as $total ) {
                $total_work += $total;
                ?>
                <td><?= $total ?></td>
            <?php } ?>
            <td></td>
        </tr>
        </tfoot>
    </table>

    <h2>Суммарная оценка работы: <?= $total_work; ?></h2>
</div>
