<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Work */

$this->title                   = 'Изменить работу';
$this->params['breadcrumbs'][] = [ 'label' => 'Конкурсные работы', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = [ 'label' => $model->title, 'url' => [ 'view', 'id' => $model->id ] ];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="container">
    <div class="work-update">

        <h1><?= Html::encode( $this->title ) ?></h1>

        <?= $this->render( '_form', [
            'model' => $model,
        ] ) ?>

    </div>
</div>