<?php
/* @var $this yii\web\View */
use app\assets\LightboxAsset;
use app\widgets\Alert;
use yii\helpers\Html;
use richardfan\widget\JSRegister;
use yii\helpers\Url;

/* @var $worksCount int */

$this->title                      = 'Экспорт работ';
$this->params['hide_breadcrumbs'] = true;
?>
	<div class="container">
		<div id="download">
			<a href="<?= Url::to( '@web/uploads/works.zip' ); ?>">Скачать архив</a><br><br>
		</div>
		<button id="start_export">Начать создание нового архива</button>
		<div class="msg" id="msg"></div>
	</div>

<?php JSRegister::begin(); ?>
	<script>
		var total_works = <?= $worksCount; ?>,
			$msg = $('#msg'), $dl = $('#download');
		function processWork(offset) {
			$dl.hide();
			if (offset >= total_works) {
				$msg.html('Создаем архив, это может занять несколько минут.');
				offset = -1;
			}

			$.ajax({
				url: '<?= Url::to( [ 'file/copy' ], true )?>',
				type: "post",
				data: {'offset': offset},
				success: function (data) {
					if (data.offset > 0) {
						$msg.html('Обработано работ: ' + data.offset + ' из ' + total_works);
						processWork(data.offset);
					} else {
						$msg.html('Новый архив создан, нажмите ссылку выше, чтобы его скачать');
						$dl.show();
					}
				}
			});
		}

		$('#start_export').on('click', function (e) {
			processWork(0);
			$(this).hide();
		});
	</script>
<?php JSRegister::end();