<?php
use app\assets\FontAwesomeAsset;
use app\assets\ModernizrAsset;
use app\bootstrap\NavBar;
use yii\helpers\Html;

use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

ModernizrAsset::register( $this );
AppAsset::register( $this );
FontAwesomeAsset::register( $this );

$this->params['show_breadcrumbs'] = false;

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="no-js">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode( $this->title ) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrapper">
    <header class="b-header">
        <div id="nav-container" class="nav-container" style="height: auto;">
            <?php
            NavBar::begin(
                [
                    'brandLabel' => [
                        Html::img( \Yii::getAlias( '@web/images/logo.jpg' ) ),
                        Html::img( \Yii::getAlias( '@web/images/logo-photo.png' ) ),
                        Html::img( \Yii::getAlias( '@web/images/minobr.png' ) ),
                    ],
                    'brandUrl'   => [
                        'http://konkurssmi.ru',
                        Yii::$app->homeUrl,
                        'http://xn--80abucjiibhv9a.xn--p1ai/',

                    ],
                ]
            );
            echo $this->render( '@partitials/menu' );

            NavBar::end();
            ?>
        </div>
    </header>
    <?= $content; ?>
    <footer class="footer">
        <div class="soc-media">
            <div class="container">
                <div class="col-xs-4 soc-medeia-footer facebook">
                    <div class="soc-more"><a href="https://www.facebook.com/photoproobr" target="_blank"><i
                                class="fa fa-facebook"></i></a>
                    </div>
                </div>
                <div class="col-xs-4 soc-medeia-footer vk">
                    <div class="soc-more"><a href="http://vk.com/club124088521" target="_blank"><i
                                class="fa fa-vk"></i></a>
                    </div>
                </div>
                <div class="col-xs-4 soc-medeia-footer youtube">
                    <div class="soc-more"><a href="http://www.youtube.com/user/NovostiShkol" target="_blank"><i
                                class="fa fa-youtube"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <div class="widget_text footer-widget">


                        <div class="textwidget">
                            «Фото PRO Образование» - Всероссийский конкурс фотоматериалов СМИ, посвященных теме развития российского образования. Конкурс проводится Министерством образования и науки Российской Федерации.
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="widget_primary-get-in-touch footer-widget">
                        <h4 class="widget-title">Контакты</h4>
                        <ul class="contact-footer contact-composer">
                            <li><i class="fa fa-user"></i> <span>Ярослав Куплинов</span></li>
                            <li><i class="fa fa-phone"></i> <span>+7 (495) 624 0301, доб. 180</span></li>
                            <li><i class="fa fa-envelope"></i> <span><a href="mailto:photo@konkurssmi.ru">photo@konkurssmi.ru</a></span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="copyright">&copy; <?= \Yii::$app->name;?>, <?= date( 'Y' ) ?> г.</div>
                    </div>
                    <div class="col-sm-6">
                        <div class="copyright elustro-copyright">
                            Создание сайта - <?= Html::a( 'дизайн-бюро «Элюстро»', 'http://elustro.net',
                                [ 'target' => '_blank' ] ); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
<?php if ( ! YII_DEBUG ) { ?>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function () {
                try {
                    w.yaCounter37999730 = new Ya.Metrika({
                        id: 37999730,
                        clickmap: true,
                        trackLinks: true,
                        accurateTrackBounce: true,
                        webvisor: true
                    });
                } catch (e) {
                }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () {
                    n.parentNode.insertBefore(s, n);
                };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else {
                f();
            }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript>
        <div><img src="https://mc.yandex.ru/watch/37999730" style="position:absolute; left:-9999px;" alt=""/></div>
    </noscript>
    <!-- /Yandex.Metrika counter -->
<?php } ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
