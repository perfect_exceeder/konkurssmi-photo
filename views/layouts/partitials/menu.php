<?php
/**
 * menu.php
 * Created by h8every1 on 30.06.2015 3:03
 */

use app\bootstrap\Nav;

/* @var $this \yii\web\View */
$menu_items = [
    [ 'label' => 'Новости', 'url' => [ 'site/news' ] ],
    [ 'label' => 'Работы', 'url' => [ 'site/works' ] ],
    [ 'label' => 'Положение', 'url' => [ 'article/view', 'slug' => 'polozhenie' ] ],
    [ 'label' => 'Номинации', 'url' => [ 'site/nominations-list' ] ],
//    [ 'label' => 'Эксперты', 'url' => [ 'site/experts' ] ],
//    [ 'label' => 'Как подать заявку', 'url' => [ 'article/view', 'slug' => 'howto' ] ],
    [
        'label' => 'Победители',
        'url'   => [ 'article/view', 'slug' => 'pobediteli' ],
    ],
    /*[
        'label' => 'Фото',
        'url'   => [ 'article/view', 'slug' => 'foto' ]
    ],*/
    /*[
        'label' => 'Видео',
        'url'   => [ 'article/view', 'slug' => 'video' ]
    ],*/
//    [ 'label' => 'О конкурсе', 'url' => [ 'site/about' ] ],
];

if ( Yii::$app->user->isGuest ) {
    $menu_items[] = [ 'label' => 'Вход/регистрация', 'url' => [ 'site/login' ] ];
} else {
    $profile = [
        'label' => Yii::$app->user->identity->email,
    ];

    $subitems = [ ];

    if ( ! Yii::$app->user->identity->isActivated() ) {
        $subitems[] = [
            'label' => 'Подтвердить регистрацию',
            'url'   => [ 'site/verify' ],
        ];
    } else {

        if ( Yii::$app->user->identity->group == \app\models\User::GROUP_PARTICIPANT ) {
            $subitems[] = [
                'label' => 'Профиль участника',
                'url'   => [ 'participant/profile' ],
            ];

            $subitems[] = [
                'label' => 'Добавить работу',
                'url'   => [ 'work/add' ],
//                'visible' => ! Yii::$app->user->identity->countWorks()
            ];
        }

    }


    $subitems[] = [
        'label' => 'Список работ',
        'url'   => [ 'work/index' ],
//        'visible' => Yii::$app->user->can('editWork') || Yii::$app->user->can('rateWork')
    ];
    $subitems[] = [
        'label'   => 'Список пользователей',
        'url'     => [ 'user/index' ],
        'visible' => Yii::$app->user->can( 'manageUsers' )

    ];
    $subitems[] = [
        'label'   => 'Список участников конкурса',
        'url'     => [ 'participant/index' ],
        'visible' => Yii::$app->user->can( 'manageUsers' )

    ];
    $subitems[] = [
        'label'   => 'Список материалов',
        'url'     => [ 'article/index' ],
        'visible' => Yii::$app->user->can( 'addNews' )

    ];

    $subitems[] = [
        'label'       => 'Выйти',
        'url'         => [ '/site/logout' ],
        'linkOptions' => [ 'data-method' => 'post' ]
    ];


    $profile['items'] = $subitems;
    $menu_items[]     = $profile;
}

echo Nav::widget( [
    'options'         => [ 'class' => '' ],
    'items'           => $menu_items,
    'activateParents' => true,
] );