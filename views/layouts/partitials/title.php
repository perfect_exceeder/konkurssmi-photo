<?php
/**
 * title.php
 * Created by h8every1 on 26.06.2015 3:10
 */
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $title string title to display */

?>
<div class="promo-block">
    <div class="promo-text"><?= Html::encode( $title ) ?></div>
    <div class="center-line"></div>
</div>