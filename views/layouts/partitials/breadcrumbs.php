<?php
/**
 * breadcrumbs.php
 * Created by h8every1 on 26.06.2015 2:52
 */
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

if ( empty( $this->params['page_title'] ) ) {
    $this->params['page_title'] = $this->title;
}

if (!isset($this->params['show_breadcrumbs'])) {
    $this->params['show_breadcrumbs'] = false;
}

$have_2nd_col =  $this->params['show_breadcrumbs'] || ! empty( $this->params['override_breadcrumbs'] );
$cols_width   = $have_2nd_col ? 6 : 12;

?>
<div class="page-in">
    <div class="container">
        <div class="row">
            <div class="col-lg-<?= $cols_width; ?>">
                <div class="page-in-name"><?= Html::encode( $this->params['page_title'] ) ?></div>
            </div>
            <?php if ( $have_2nd_col ) : ?>
                <div class="col-lg-6">
                    <?php if ( $this->params['show_breadcrumbs'] && empty( $this->params['override_breadcrumbs'] ) ) { ?>
                        <div class="page-in-bread">
                            <?= Breadcrumbs::widget( [
                                'links'              => isset( $this->params['breadcrumbs'] ) ? $this->params['breadcrumbs'] : [ ],
                                'tag'                => 'span',
                                'itemTemplate'       => '{link} / ',
                                'activeItemTemplate' => '{link}',
                                'options'            => [ ],
                                'homeLink'           => false,
                            ] ) ?>
                        </div>
                    <?php } else if ( ! empty( $this->params['override_breadcrumbs'] ) ) { ?>
                        <div class="page-in-override">
                            <?= $this->params['override_breadcrumbs']; ?>
                        </div>
                    <?php } ?>

                </div>
            <?php endif; ?>
        </div>
    </div>
</div>