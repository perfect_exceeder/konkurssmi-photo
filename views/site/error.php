<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = 'Ошибка!';
?>
<div class="container marg75">
    <div class="row">
        <div class="col-lg-12">
            <div class="main_pad">
                <strong class="colored oops">Ой, ошибка 404!</strong><br><br>
                <p><?= nl2br( Html::encode( $message ) ) ?></p>
            </div>
        </div>
    </div>
</div>