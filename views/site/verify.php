<?php
use app\helpers\ViewHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\User;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\SignupForm */

$this->title                   = 'Подтверждение регистрации';
$this->params['breadcrumbs'][] = $this->title;
$code_resent                   = Yii::$app->session->hasFlash( 'activationCodeResent' );
?>
<?= ViewHelper::breadcrumbs(); ?>
<div class="container">
    <div class="site-мукшан">
        <?php if ( $code_resent ) { ?>

            <div class="alert alert-info">
                Код выслан на ваш e-mail. Если он не придет в течение двух минут, проверьте папку "Спам".
            </div>

        <?php } ?>

        <p>Благодарим за регистрацию на участие в конкурсе! На указанный электронный адрес отправлено письмо с кодом активации Вашей учетной записи.</p>

        <?php $form = ActiveForm::begin( [
            'id'     => 'form-signup',
            'method' => 'get',
            'action' => [ 'site/verify' ]
        ] ); ?>
        <?= $form->field( $model, 'activation_key' ); ?>
        <div class="form-group">
            <?= Html::submitButton( 'Подтвердить',
                [ 'class' => 'btn btn-primary', 'name' => 'signup-button' ] ) ?>
            <?php if ( ! $code_resent ) { ?>
                <?= Html::a( 'Выслать письмо с кодом подтверждения', [ 'site/resend-code' ],
                    [ 'class' => 'btn btn-success', 'name' => 'resend-button', ] ) ?>
            <?php } ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>