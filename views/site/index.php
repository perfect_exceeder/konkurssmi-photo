<?php
/* @var $this yii\web\View */
use app\assets\LightboxAsset;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $worksCount int */
/* @var $polozhenie \app\models\Article */
/* @var $howto \app\models\Article */
/* @var $main \app\models\Article */
/* @var $nominations \app\models\Article */
/* @var $news \app\models\Article */
/* @var $works \app\models\Article */
/* @var $expert \app\models\Article */

LightboxAsset::register( $this );
$this->title                      = \Yii::$app->name;
$this->params['hide_breadcrumbs'] = true;
?>
<div class="container">
	<div class="site-index">
		<?= Alert::widget( [ 'closeButton' => false ] ) ?>

		<div class="jumbotron b-intro"></div>
		<div class="body-content">
			<h2>О конкурсе</h2>

			<div class="b-news-item__main">
				<div class="b-news-item--excerpt">
					<?= $main->content; ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">

					<h2>Как подать заявку</h2>

					<div class="b-news-item__main">
						<div class="b-news-item--excerpt">
							<iframe src="//www.slideshare.net/slideshow/embed_code/key/7C0iO7LO8RfA1P" width="595"
							        height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no"
							        style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;"
							        allowfullscreen></iframe>
							<div style="margin-bottom:5px"><strong> <a
										href="//www.slideshare.net/Editor-Education/pro-63240064"
										title="Как подать заявку на конкурс &quot;Фото PRO Образование&quot;"
										target="_blank">Как подать заявку на конкурс &quot;Фото PRO
										Образование&quot;</a> </strong>
							</div>
						</div>

					</div>
				</div>
				<div class="col-md-6">
					<h2><?= Html::a( 'Последние новости', Url::to( [ 'site/news' ] ) ) ?></h2>
					<?php
					foreach ( $news as $news_item ) {
						?>
						<div class="b-news-item__main">
							<small class="b-news-item--date"><?= date( 'd.m.Y', $news_item->created_at ); ?></small>
							<h4 class="b-news-item--title"><?= Html::a( $news_item->title,
									Url::to( $news_item->url ) ); ?></h4>
							<?php
							$lead = $news_item->lead;
							if ( $lead ) { ?>
								<div class="b-news-item--excerpt">
									<?= $lead; ?>
								</div>
							<?php } ?>
						</div>
						<?php
					}

					?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php if ( $worksCount > 0 ) { ?>
	<div class="prl-1">
		<div class="prlx">
			<div class="container">
				<div class="stats-primary">
					<div class="stats-block stats-top">
						<div class="aio-icon-top">
							<div class="aio-icon none"><i class="icon-happy"></i></div>
						</div>
						<div class="stats-desc">
							<div class="stats-text">На конкурс <?= \Yii::t( 'app',
									'{n, plural, one{подана} few{подано} other{подано}}',
									[ 'n' => $worksCount ]
								); ?></div>
							<div class="stats-number number-counter" data-number="<?= $worksCount; ?>"><span
									class="number-count"><?= $worksCount; ?></span>
							</div>
							<div class="stats-text"><?= \Yii::t( 'app',
									'{n, plural, one{работа} few{работы} many{работ} other{работы}}',
									[ 'n' => $worksCount ]
								); ?></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php } ?>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<div class="promo-block">
				<div class="promo-text">
					<?= Html::a('Примеры поданых работ', Url::to(['site/works'], true));?></div>
			</div>
		</div>
	</div>


	<div class="marg30">
		<?php foreach ( $works as $works_item ) {
			?><div class="photo-preview">
				<div class="blog-main">
					<div class="blog-images">
						<div class="post-thumbnail">

							<?= Html::a( Html::img( $works_item->getThumbFileUrl( 'image', 'promo' ),
								[ 'class' => 'work-photo' ] ), Url::to( $works_item->getImageFileUrl( 'image' ) ), ['class'=>'image-link','title'=>$works_item->title] ) ?>
						</div>
					</div>
					<div class="blog-name"><?= $works_item->title; ?></div>
				</div>
			</div><?php } ?>
	</div>

</div>
