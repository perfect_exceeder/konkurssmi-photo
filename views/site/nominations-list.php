<?php

use app\helpers\ViewHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $data mixed */
/* @var $total int */

$this->title = 'Номинации конкурса';
?>
<?= ViewHelper::breadcrumbs(); ?>
<div class="container">
    <div class="b-post">
        <div class="b-post--content">
                <ol>
                    <?php

                    foreach ( $data as $item ) {
                        ?>
                        <li><?= $item['name'];?> — <strong><?= $item['count']; ?> <?= \Yii::t( 'app',
                                '{n, plural, one{работа} few{работы} many{работ} other{работы}}',
                                [ 'n' => $item['count'] ]
                            ); ?></strong>
                        <p>
                            <?= $item['description'];?>
                        </p></li>
                        <?php
                    } ?>
                </ol>

            <p>Всего — <?= $total;?> <?= \Yii::t( 'app',
                    '{n, plural, one{работа} few{работы} many{работ} other{работы}}',
                    [ 'n' => $total ]
                ); ?>.</p>
        </div>
    </div>
</div>