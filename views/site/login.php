<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\Participant;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\Participant */

$this->title                   = 'Вход на сайт';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="row">

        <div class="col-sm-5">
            <div class="site-login">
                <h2>Вход</h2>

                <?php $form = ActiveForm::begin( [
                    'id'          => 'login-form',
                    'options'     => [ 'class' => 'form-horizontal' ],
                    'fieldConfig' => [
                        'template'     => "{label}\n<div class=\"col-lg-9\">{input}</div>\n<div class=\"col-lg-9 col-lg-push-3\">{error}</div>",
                        'labelOptions' => [ 'class' => 'col-lg-3 control-label' ],
                    ],
                ] ); ?>

                <?= $form->field( $model, 'email' ) ?>

                <?= $form->field( $model, 'password' )->passwordInput() ?>
                <div class="row">
                    <div class="col-lg-offset-3 col-lg-5">
                        <?= $form->field( $model, 'rememberMe' )->checkbox( [
                            'template' => "{input} {label}",
                        ] ) ?>
                    </div>
                    <div class="col-lg-4">
                        <small><?= Html::a( 'Забыли пароль?', [ 'site/request-password-reset' ] ) ?></small>
                    </div>
                </div>

                <div class="form-group">
                    <?= Html::submitButton( 'Войти', [ 'class' => 'btn btn-primary', 'name' => 'login-button' ] ) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
        <div class="col-sm-offset-2 col-sm-5">
            <div class="site-signup">
                <h2>Регистрацияя</h2>
                <?= Html::a( 'Зарегистрироваться',
                    [ 'participant/register'],
                    [ 'class' => 'btn btn-block btn-primary btn-lg' ] ) ?>
            </div>
        </div>
    </div>
</div>