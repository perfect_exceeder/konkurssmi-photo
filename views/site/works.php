<?php
/**
 * works.php
 * Created by h8every1 on 17.07.2016 23:59
 */

/* @var $this yii\web\View */
use app\assets\LightboxAsset;
use app\helpers\ViewHelper;
use yii\helpers\Html;
use yii\helpers\Url;

LightboxAsset::register( $this );
/* @var $works \app\models\Article */
$this->title = 'Присланные работы';
?>
<?= ViewHelper::breadcrumbs(); ?>
<div class="container">
	<div class="marg30">
		<?php foreach ( $works as $works_item ) {
			?><div class="photo-preview">
			<div class="blog-main">
				<div class="blog-images">
					<div class="post-thumbnail">

						<?= Html::a( Html::img( $works_item->getThumbFileUrl( 'image', 'promo' ),
							[ 'class' => 'work-photo' ] ), Url::to( $works_item->getImageFileUrl( 'image' ) ), ['class'=>'image-link','title'=>$works_item->title] ) ?>
					</div>
				</div>
				<div class="blog-name"><?= $works_item->title; ?></div>
			</div>
			</div><?php } ?>
	</div>
</div>