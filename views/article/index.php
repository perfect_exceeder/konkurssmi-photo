<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\Article */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = $searchModel->categoryName;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="article-index">

        <h1><?= Html::encode( $this->title ) ?></h1>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


        <p>
            <?= Html::a( 'Добавить материал', [ 'create' ], [ 'class' => 'btn btn-success' ] ) ?>
        </p>

        <?= GridView::widget( [
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => [
//                [ 'class' => 'yii\grid\SerialColumn' ],
//                'id',
                'title:ntext',
//                'slug',
//                'content:html',
                'created_at:date',
                // 'created_by',
                [
                    'attribute' => 'category',
                    'value'     => function ( $model ) {
                        return $model->categoryName;
                    },
                    'filter'    => \app\models\search\Article::getCategories()

                ],
                [
                    'class'   => yii\grid\ActionColumn::className(),
                    'buttons' => [
                        'view' => function ( $url, $model, $key ) {
                            return Html::a( '<span class="glyphicon glyphicon-eye-open"></span>', $model->route );
                        }
                    ],

                ],
            ],
        ] ); ?>

    </div>
</div>