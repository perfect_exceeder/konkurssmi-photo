<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget;

/* @var $this yii\web\View */
/* @var $model app\models\Article */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="article-form">

    <?php $form = ActiveForm::begin( [
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ] ); ?>
    <?= $form->errorSummary( $model ); ?>
    <?= $form->field( $model, 'category' )->dropDownList(
        $model->categories
    ) ?>

    <?= $form->field( $model, 'title' )->textInput() ?>

    <?= $form->field( $model, 'slug' )->textInput( [ 'maxlength' => true ] ) ?>

    <?= $form->field( $model, 'image' )->fileInput( [ 'accept' => 'image/*' ] ) ?>
    <?php if ( $model->image ) { ?>
        <div class="image-preview">
            <?= Html::img( $model->getThumbFileUrl( 'image', 'thumb' ),
                [ 'class' => 'img-thumbnail' ] ); ?>
            <?= Html::img( $model->getThumbFileUrl( 'image', 'square' ),
                [ 'class' => 'img-thumbnail' ] ); ?>
            <div class="alert alert-info">
                Если вы загрузите новое изображение, это будет удалено.
            </div>
        </div>
    <?php } ?>

    <?= $form->field( $model, 'excerpt' )->textarea( [ 'rows' => 3 ] ) ?>

    <?= $form->field( $model, 'content' )->widget( Widget::className(), [
        'settings' => [
            'lang'             => 'ru',
            'minHeight'        => 200,
            'imageManagerJson' => Url::to( [ '/article/images-get' ] ),
            'fileManagerJson'  => Url::to( [ '/article/files-get' ] ),
            'imageUpload'      => Url::to( [ '/article/image-upload' ] ),
            'fileUpload'       => Url::to( [ '/article/file-upload' ] ),
            'plugins'          => [
//                'clips',
//                'fullscreen',
                'imagemanager',
                'filemanager',
                'table',
                'counter',
                'video',
            ]
        ]
    ] ); ?>

    <?= $form->field( $model, 'created_at' )->widget( \yii\jui\DatePicker::classname(), [
        'language'   => 'ru',
        'inline'     => false,
        'dateFormat' => 'dd.MM.yyyy',
    ] ) ?>

    <div class="form-group">
        <?= Html::submitButton( $model->isNewRecord ? 'Добавить' : 'Обновить',
            [ 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary' ] ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>