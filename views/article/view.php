<?php

use app\helpers\ViewHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Article */

$this->title = $model->title;
?>
<?= ViewHelper::breadcrumbs(); ?>

<div class="container">
    <div class="b-post">
        <?php if ( \Yii::$app->user->can( 'addNews' ) ) { ?>
            <p>
                <?= Html::a( 'Изменить', [ 'update', 'id' => $model->id ], [ 'class' => 'btn btn-primary' ] ) ?>
                <?= Html::a( 'Удалить', [ 'delete', 'id' => $model->id ], [
                    'class' => 'btn btn-danger',
                    'data'  => [
                        'confirm' => 'Вы уверены, что хотите удалить этот материал?',
                        'method'  => 'post',
                    ],
                ] ) ?>
            </p>
        <?php } ?>
        <div class="b-post--content">
            <?= $model->content; ?>
        </div>
        <div class="b-share-buttons">
            <div class="social-likes">
                <div class="vkontakte" title="Поделиться ссылкой во Вконтакте">ВКонтакте</div>
                <div class="odnoklassniki" title="Поделиться ссылкой в Одноклассниках">Одноклассники</div>
                <div class="twitter" data-via="sibdepo" title="Поделиться ссылкой в Твиттере">Twitter</div>
                <div class="facebook" title="Поделиться ссылкой на Фейсбуке">Facebook</div>
            </div>
        </div>
    </div>
</div>