<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\Article */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = $searchModel->categoryName;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="article-index">

        <h1><?= Html::encode( $this->title ) ?></h1>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= ListView::widget( [
            'dataProvider' => $dataProvider,
            'itemOptions' => ['class' => 'item'],
            'itemView' => '_item',
            'summary' => '',
        ] ); ?>

    </div>
</div>