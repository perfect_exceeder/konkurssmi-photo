<?php

use app\helpers\ViewHelper;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Article */

$this->title                   = 'Добавление текстового материала';
$this->params['breadcrumbs'][] = [ 'label' => 'Материалы', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = [ 'label' => 'Добавление'  ];
$this->params['show_breadcrumbs'] = true;
?>
<?= ViewHelper::breadcrumbs(); ?>
<div class="container">
    <div class="article-create">

        <?= $this->render( '_form', [
            'model' => $model,
        ] ) ?>

    </div>
</div>