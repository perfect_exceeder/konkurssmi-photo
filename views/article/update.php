<?php

use app\helpers\ViewHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Article */

$this->title                   = $model->title;
$this->params['breadcrumbs'][] = [ 'label' => 'Материалы', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = [ 'label' => $model->title, 'url' => [ 'view', 'id' => $model->id ] ];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<?= ViewHelper::breadcrumbs(); ?>
<div class="container">
    <div class="article-update">

        <h1><?= Html::encode( $this->title ) ?></h1>

        <?= $this->render( '_form', [
            'model' => $model,
        ] ) ?>

    </div>
</div>