<?php
/**
 * _item.php
 * Created by h8every1 on 07.07.2015 1:52
 */

/* @var $this yii\web\View */
/* @var $model app\models\Article */
/* @var $key mixed the key value associated with the data item */
/* @var $index integer the zero-based index of the data item in the items array returned by [[dataProvider]] */
/* @var $widget yii\widgets\ListView, this widget instance */


use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="b-post">
    <h2 class="b-post--title">
        <?php echo Html::a( Html::encode( $model->title ), $model->route ); ?>
    </h2>

    <div class="b-post--meta">
        <div class="b-post--metaitem b-post--metaitem__date">
            <?= \Yii::$app->formatter->asDate( $model->created_at, 'medium' ); ?>
        </div>
        <?php if ( \Yii::$app->user->can( 'addNews' ) ) { ?>
            <div class="b-post--metaitem b-post--metaitem__edit">
                <?= Html::a( 'Изменить', [ 'article/update', 'id' => $model->id ], [ 'class' => 'btn btn-primary' ] ) ?>
            </div>
        <?php } ?>
    </div>

    <div class="b-post--content">
        <?php echo $model->lead; ?>
    </div>
    <div class="b-share-buttons">
        <div class="social-likes social-likes_notext" data-url="<?= Url::to($model->route, true); ?>" data-title="<?= $model->title; ?>">
            <div class="vkontakte" title="Поделиться ссылкой во Вконтакте"></div>
            <div class="odnoklassniki" title="Поделиться ссылкой в Одноклассниках"></div>
            <div class="twitter" title="Поделиться ссылкой в Твиттере"></div>
            <div class="facebook" title="Поделиться ссылкой на Фейсбуке"></div>
        </div>
    </div>
</div>