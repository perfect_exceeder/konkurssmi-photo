<?php
/**
 * reg_data.php
 * Created by h8every1 on 22.06.2015 16:15
 */

use app\helpers\ViewHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Participant */
/* @var $form yii\widgets\ActiveForm */
?>
<?= ViewHelper::title('Регистрационные данные');?>
<?php

if ( $model->isNewRecord ) {
    echo Html::activeHiddenInput( $model, 'type' );
}


$emailField = $form->field( $model, 'email', [ 'enableAjaxValidation' => true ] )->textInput();

if ( $model->isNewRecord ) {
    $emailField->hint( 'Этот адрес вы будете указывать в качестве логина. Также на него придет письмо с подтверждением регистрации в конкурсе.' );
}

echo $emailField;
?>
<?php
if ( $model->isNewRecord ) {
    ?>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field( $model, 'password' )->passwordInput() ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field( $model, 'password2',
                [ 'enableAjaxValidation' => true ] )->passwordInput() ?>
        </div>
    </div>
<?php } else {
    $url = ['change-password'];
    if (\Yii::$app->user->can( 'manageUsers')) {
        $url['id'] = $model->user_id;
    }
    echo Html::a( 'Изменить пароль', $url );
}