<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Participant */

$this->title                   = 'Изменение пароля';
$this->params['breadcrumbs'][] = [ 'label' => 'Профиль участника', 'url' => [ 'profile' ] ];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="container">
    <div class="participant-change-password">

        <h1><?= Html::encode( $this->title ) ?> пользователя <?= Html::encode( $model->name ); ?></h1>

        <div class="participant-form">
            <?php $form = ActiveForm::begin( [ 'enableAjaxValidation' => true ] ); ?>
            <?= $form->field( $model, 'password' )->passwordInput()->label( 'Введите новый пароль' ) ?>
            <?= $form->field( $model,
                'password2' )->passwordInput()->label( 'Повторите новый пароль ещё раз' )->hint( '' ) ?>

            <div class="form-group">
                <?= Html::submitButton( 'Изменить пароль', [ 'class' => 'btn btn-primary' ] ) ?>
                <?= Html::a( 'Вернуться в профиль', ['user/profile'], [ 'class' => 'btn btn-success' ] ) ?>
            </div>
            <?php ActiveForm::end(); ?>

        </div>

    </div>
</div>