<?php

use app\models\Participant;
use app\models\Work;
use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel app\models\search\Participant */

$this->title                   = 'Участники конкурса';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="participant-index">

    <h1><?= Html::encode( $this->title ) ?></h1>

    <?php

    $columns_template = [
        'name'       => [
            'attribute' => 'name',
            'value'     => function ( $model ) {
                /* @var $model app\models\Participant */
                return $model->name . '<br>' . Html::a( 'Подробнее', [ 'profile', 'id' => $model->user_id ] );
            },
            'format'    => 'html'
        ],
        'region'     => [
            'attribute' => 'region',
            'filter'    => Participant::getRegionsWithCode(),
            'value'     => function ( $model ) {
                $regions = Participant::getRegionsWithCode();

                $index = isset( $regions[ $model->region ] ) ? $model->region : '00';

                return $regions[ $index ];
            },

        ],
        'smi_type'   => [
            'attribute' => 'smi_type',
            'filter'    => Participant::getSmiTypes(),
            'value'     => function ( $model ) {
                return $model->smi_type_readable;
            },
        ],
        'smi_status' => [
            'attribute' => 'smi_status',
            'filter'    => Participant::getSmiStatuses(),
            'value'     => function ( $model ) {
                if ( $model->smi_status ) {
                    return $model::getSmiStatuses()[ $model->smi_status ];
                }
            },
        ],

    ];


    $excelGridColumns = [
        'name',
        'occupation',
        $columns_template['region'],
        'city_phone',
        'mobile_phone',
        'user.email',
        'post_address:ntext',
        'smi_name',
        'smi_type_readable',
        'smi_url:url',
        $columns_template['smi_status'],
        'smi_periodity',
        'smi_area:ntext',
        'smi_editor:ntext',
        'smi_head',
        'smi_jur_address:ntext',
    ];

    // Renders a export dropdown menu
    echo ExportMenu::widget( [
        'dataProvider'    => $dataProvider,
        'columns'         => $excelGridColumns,
        'dropdownOptions' => [
            'label' => 'Экспорт данных',
            'class' => 'btn btn-primary'
        ],
        'filename'        => 'Список_участников'
    ] );

    $gridColumns = [
        $columns_template['name'],
        [
            'attribute' => 'smi_name',
            'value'     => function ( $model ) {
                /* @vat $model app\models\Participant */
                return Html::a( $model->smi_name, $model->smi_url, [ 'target' => '_blank' ] );
            },
            'format'    => 'html'
        ],
        $columns_template['region'],
        $columns_template['smi_type'],
        [
            'attribute' => 'has_works',
            'label'     => 'Есть работы?',
            'filter'    => [ 'Нет', 'Да' ],
            'format'    => 'boolean',
            'value'     => function ( $model ) {
                return $model->user->countWorks();
            }
        ],
        'mobile_phone',
        'city_phone',
        'user.email',
        // 'add_time:date',
        // [ 'class' => 'yii\grid\ActionColumn' ],
    ];

    // You can choose to render your own GridView separately
    echo \kartik\grid\GridView::widget( [
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => $gridColumns
    ] );

    ?>


</div>