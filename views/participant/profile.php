<?php

use app\helpers\ViewHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Participant */
if ( $model->isNewRecord ) {
    $this->title                   = 'Регистрация участника';
    $this->params['breadcrumbs'][] = $this->title;
} else {
    $this->title                   = $model->name;
    $this->params['breadcrumbs'][] = [ 'label' => 'Профиль участника' ];

    $this->params['override_breadcrumbs'] = Html::a( 'Добавить работу', [ 'work/add' ],
        [ 'class' => 'btn btn-primary' ] );
}

?>
<?= ViewHelper::breadcrumbs(); ?>
<div class="container">
    <div class="participant-profile">
        <?php

        foreach ( Yii::$app->session->getAllFlashes() as $key => $message ) {
            echo '<div class="alert alert-' . $key . '">' . $message . '</div>';
        } ?>
        <div class="participant-form">

            <?php $form = ActiveForm::begin();
            echo $form->errorSummary($model);

            echo $this->render( '_form/reg_data', [
                'model' => $model,
                'form'  => $form,
            ] )
            ?>

            <?= $this->render( '_form_journalist', [
                'model' => $model,
                'form'  => $form,
            ] ) ?>
            <?php ActiveForm::end(); ?>

        </div>

    </div>
</div>