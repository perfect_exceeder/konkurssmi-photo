<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Participant */

//var_dump($model);
//die();
$this->title                   = $model->name;
$this->params['breadcrumbs'][] = [ 'label' => 'Participants', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="participant-view">
    <h1><?= Html::encode( $this->title ) ?></h1>

    <p>
        <?= Html::a( 'Изменить', [ 'profile', 'id' => $model->user_id ], [ 'class' => 'btn btn-primary' ] ) ?>
        <?php if ( ! \Yii::$app->user->can( 'manageUsers' ) ) { ?>
            <?= Html::a( 'Удалить', [ 'delete', 'id' => $model->user_id ], [
                'class' => 'btn btn-danger',
                'data'  => [
                    'confirm' => 'Вы уверены, что хотите удалить этого пользователя?',
                    'method'  => 'post',
                ],
            ] ) ?>
        <?php } ?>
    </p>

    <?= DetailView::widget( [
        'model'      => $model,
        'attributes' => [
            [
                'attribute' => 'id',
                'value'     => $model->user->id,
            ],
            'user.email',
            'name',
            'smi_name',
            'smi_type_readable',
            'participant_type',
            'smi_url:url',
            'smi_editor:ntext',
            'smi_periodity',
            'smi_area:ntext',
            'smi_status',
            'smi_head',
            'smi_jur_address:ntext',
            'occupation',
            'post_address:ntext',
            'city_phone',
            'mobile_phone',
//            'add_time:datetime',
        ],
    ] ) ?>

</div>
