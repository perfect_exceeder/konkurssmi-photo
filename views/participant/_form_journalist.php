<?php

use app\helpers\ViewHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Participant */
/* @var $form yii\widgets\ActiveForm */
?>

<?= ViewHelper::title( 'Информация о Вас' ); ?>

<div class="row">
    <div class="col-sm-4"><?= $form->field( $model, 'name1' )->textInput( [ 'maxlength' => true ] ) ?></div>
    <div class="col-sm-4"><?= $form->field( $model, 'name2' )->textInput( [ 'maxlength' => true ] ) ?></div>
    <div class="col-sm-4"><?= $form->field( $model, 'name3' )->textInput( [ 'maxlength' => true ] ) ?></div>
</div>
<?= $form->field( $model, 'occupation' )->textInput( [ 'maxlength' => true ] ) ?>
<div class="row">
    <div class="col-xs-6"><?= $form->field( $model, 'mobile_phone',
            [ 'enableAjaxValidation' => true ] )->textInput( [ 'maxlength' => true ] ) ?></div>
    <div class="col-xs-6"><?= $form->field( $model, 'city_phone',
            [ 'enableAjaxValidation' => true ] )->textInput( [ 'maxlength' => true ] ) ?></div>
</div>
<?= $form->field( $model, 'region' )->dropDownList(
    \app\models\Participant::getRegions()
) ?>
<?= $form->field( $model, 'post_address' )->textarea( [ 'rows' => 3 ] ) ?>

<?= ViewHelper::title( 'Информация о СМИ' ); ?>
<?= $form->field( $model, 'smi_name' )->textInput( [ 'maxlength' => true ] ) ?>
<?= $form->field( $model, 'smi_type' )->dropDownList( $model->getSmiTypes(),
    array( 'prompt' => '- Выберите тип СМИ -' ) ) ?>
<?= $form->field( $model, 'smi_url' )->textInput( [ 'maxlength' => true ] ) ?>

<div class="form-group">
    <?= Html::submitButton( $model->isNewRecord ? 'Зарегистрироваться в конкурсе' : 'Сохранить изменения',
        [ 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary' ] ) ?>
</div>