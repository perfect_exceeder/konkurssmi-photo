<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title                   = $model->name;
$this->params['breadcrumbs'][] = [ 'label' => 'Users', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="user-view">

        <h1><?= Html::encode( $this->title ) ?></h1>

        <p>
            <?= Html::a( 'Изменить', [ 'update', 'id' => $model->id ], [ 'class' => 'btn btn-primary' ] ) ?>
            <?= Html::a( 'Удалить', [ 'delete', 'id' => $model->id ], [
                'class' => 'btn btn-danger',
                'data'  => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method'  => 'post',
                ],
            ] ) ?>
        </p>

        <?= DetailView::widget( [
            'model'      => $model,
            'attributes' => [
                'id',
                [
                    'attribute' => 'name',
                    'label' => 'Имя',
                ],
//                'auth_key',
//                'password_hash',
//                'password_reset_token',
                'activation_key',
                'email:email',
                [
                    'attribute' => 'status',
                    'value' => $model->statusName,
                ],
                [
                    'attribute' => 'group',
                    'value' => $model->groupName,
                ],
                'created_at:datetime',
                'updated_at:datetime',
            ],
        ] ) ?>

    </div>
</div>