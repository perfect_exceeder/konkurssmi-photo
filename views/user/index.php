<?php

use yii\grid\DataColumn;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\User */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="user-index">

        <h1><?= Html::encode( $this->title ) ?></h1>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?= Html::a( 'Добавить пользователя', [ 'create' ], [ 'class' => 'btn btn-success' ] ) ?>
        </p>

        <?= GridView::widget( [
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => [
//            ['class' => 'yii\grid\SerialColumn'],

                'id',
//            'auth_key',
//            'password_hash',
//            'password_reset_token',
//                'activation_key',
                'email:email',
                [
                    'class' => DataColumn::className(), // this line is optional
                    'attribute' => 'status',
                    'value'     => function ( $model ) {
                        return $model->statusName;
                    },
                    'filter' => $searchModel->statuses
                ],
                [
                    'class' => DataColumn::className(), // this line is optional
                    'attribute' => 'group',
                    'value'     => function ( $model ) {
                        return $model->groupName;
                    },
                    'filter' => $searchModel->groups
                ],
//                'statusName',
//                'groupName',
                'created_at:datetime',
                'updated_at:datetime',
                [ 'class' => 'yii\grid\ActionColumn' ],
            ],
        ] ); ?>

    </div>
</div>