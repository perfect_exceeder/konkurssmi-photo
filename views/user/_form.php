<?php

use app\models\Work;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->errorSummary( $model ); ?>
    <?= $form->field( $model, 'name' )->textInput() ?>

    <?= $form->field( $model, 'email' )->textInput( [ 'maxlength' => true ] ) ?>

    <?= $form->field( $model, 'password' )->passwordInput() ?>

    <?= $form->field( $model, 'status' )->dropDownList( $model->statuses ); ?>

    <?= $form->field( $model, 'group' )->dropDownList( $model->groups ); ?>

    <?php
    if ( $model->group == $model::GROUP_JURY ) {
        ?>
        <hr>
        <?php
        $jury_categories = ArrayHelper::map( $model->jury->getCategories()->asArray()->all(), 'category_id',
            'category_id' );

        $all_cats_temp = Work::allNominations();
        $all_cats = [];

        /*

        TYPE_JOURNALIST:
            101 - nomination 1
            102 - nomination 3
            103 - nomination 3
            ...
        TYPE_COMPANY:
            101 - nomination 1
            102 - nomination 3
            103 - nomination 3
            ...

        */

        foreach ($all_cats_temp as $id=>$nominations) {
            $all_cats[$id] = [];

            foreach ( $nominations as $nom_id => $nom_title ) {
                $all_cats[$id]['{"jury_id":'.$model->jury->id.',"category_id":'.$nom_id.'}'] = $nom_title;
            }
        }
        echo $form->field( $model->jury, 'categories' )->dropDownList(
            $all_cats,
            [ 'multiple' => true, 'size'=>18 ]
        )->label( 'Доступ к работам по категориям' )->hint( 'Чтобы выбрать несколько категорий, кликните по ним с зажатой клавишей Ctrl' );
    }
    ?>

    <div class="form-group">
        <?= Html::submitButton( $model->isNewRecord ? 'Добавить' : 'Обновить',
            [ 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary' ] ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
