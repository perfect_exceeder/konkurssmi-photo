<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "jury".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $konkurs_id
 * @property string $name
 * @property string $activity
 *
 * @property Konkurs $konkurs
 * @property User $user
 * @property JuryCategory[] $categories
 * @property WorkMark[] $workMarks
 */
class Jury extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jury';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'konkurs_id'], 'integer'],
            [['konkurs_id', 'name'], 'required'],
            [['name', 'activity'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'konkurs_id' => 'Konkurs ID',
            'name' => 'Name',
            'activity' => 'Activity',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKonkurs()
    {
        return $this->hasOne(Konkurs::className(), ['id' => 'konkurs_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(JuryCategory::className(), ['jury_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkMarks()
    {
        return $this->hasMany(WorkMark::className(), ['jury_id' => 'id']);
    }

    public function afterSave($insert, $changedAttributes) {
        if (($data = \Yii::$app->request->post(self::formName())) && is_array($data['categories'])) {


            // remove old cats
            JuryCategory::deleteAll(['jury_id'=>$this->id]);

            foreach ($data['categories'] as $category) {
                $cat = new JuryCategory();
                $data_decoded = json_decode($category);
                $cat->jury_id = $this->id;
                $cat->category_id = $data_decoded->category_id;
                $cat->save();
            }
        }

        return parent::afterSave($insert,$changedAttributes);
    }

    /**
     * @inheritdoc
     * @return JuryQuery the active query used by this AR class.
     */
    public static function find() {
        return new JuryQuery( get_called_class() );
    }
}
