<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Url;

/**
 * This is the model class for table "article".
 *
 * @property integer $id
 * @property string $title
 * @property string $slug
 * @property string $content
 * @property string $excerpt
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $category
 * @property string $image
 *
 * @property User $createdBy
 */
class Article extends \yii\db\ActiveRecord {

    const CATEGORY_PAGES = 0;
    const CATEGORY_NEWS = 1;
    const CATEGORY_EXPERT = 2;
    const CATEGORY_WORKS = 3;

    public function behaviors() {
        return [
            [
                'class'        => SluggableBehavior::className(),
                'attribute'    => 'title',
                'ensureUnique' => true,
                'immutable'    => true,
                // 'slugAttribute' => 'slug',
            ],
            /*[
                'class'              => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => false,
//                'value' => new Expression('NOW()'),
            ],*/
            [
                'class'              => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => false,
            ],
            [
                'class' => \yiidreamteam\upload\ImageUploadBehavior::className(),
                'attribute' => 'image',
                'thumbs' => [
                    'thumb' => ['width' => 300, 'height' => 400],
                    'promo' => ['width' => 360, 'height' => 250],
                    'square' => ['width' => 150, 'height' => 150],
                ],
                'filePath' => '@webroot/uploads/images/articles/[[pk]].[[extension]]',
                'fileUrl' => '/uploads/images/articles/[[pk]].[[extension]]',
                'thumbPath' => '@webroot/uploads/images/articles/[[pk]]_[[profile]].[[extension]]',
                'thumbUrl' => '/uploads/images/articles/[[pk]]_[[profile]].[[extension]]',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'article';
    }

    public function scenarios() {
        $scenarios = parent::scenarios();

        $scenarios['edit'] = [ 'title', 'content', 'excerpt', 'slug', 'created_at', 'category' ];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [ [ 'title', 'content' ], 'required' ],
            [ [ 'title', 'content', 'excerpt' ], 'string' ],
            [ [ 'category' ], 'integer' ],
            [ [ 'slug' ], 'string', 'max' => 150 ],
//            [ 'created_at', 'safe' ],
            [ 'created_at', 'date', 'format' => 'dd.MM.yyyy' ],
            [ 'image', 'file', 'extensions' => 'jpeg, gif, png' ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id'         => 'ID',
            'title'      => 'Название',
            'slug'       => 'URL',
            'content'    => 'Текст',
            'excerpt'    => 'Вступление',
            'created_at' => 'Время создания',
            'created_by' => 'Автор',
            'category'   => 'Категория',
            'image'   => 'Изображение',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeHints() {
        return [
            'excerpt' => 'Отображается только на главной странице и в списке новостей',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy() {
        return $this->hasOne( User::className(), [ 'id' => 'created_by' ] );
    }

    /**
     * @inheritdoc
     * @return ArticleQuery the active query used by this AR class.
     */
    public static function find() {
        return new ArticleQuery( get_called_class() );
    }

    public function afterValidate() {
        if ( $this->scenario == 'edit' ) {
            if ( $this->created_at ) {
                $this->created_at = strtotime( $this->created_at );
            } else {
                $this->created_at = time();
            }
        }
        parent::afterValidate();
    }

    public static function getCategories() {
        return [
            self::CATEGORY_PAGES => 'Страницы',
            self::CATEGORY_NEWS  => 'Новости',
            self::CATEGORY_EXPERT  => 'Эксперты',
            self::CATEGORY_WORKS  => 'Работы',
        ];
    }

    public static function getCategoryCodes() {
        return [
            self::CATEGORY_PAGES => 'page',
            self::CATEGORY_NEWS  => 'news',
            self::CATEGORY_EXPERT  => 'expert',
            self::CATEGORY_WORKS  => 'works',
        ];
    }

    public function getCategoryName( $id = null ) {
        if ( ! $id ) {
            $id = $this->category;
        }
        if ( ! isset( $this->categories[ $id ] ) ) {
            $id = self::CATEGORY_PAGES;
        }

        return $this->categories[ $id ];
    }

    public function getCategoryCode( $id ) {
        if ( ! isset( $this->categoryCodes[ $id ] ) ) {
            $id = self::CATEGORY_PAGES;
        }

        return $this->categoryCodes[ $id ];
    }

    public function getRoute() {
        $result = [ 'article/view', 'slug' => $this->slug ];

        if ( $this->category !== self::CATEGORY_PAGES ) {
            $result['category'] = $this->getCategoryCode( $this->category );
        }

        return $result;
    }

    public function getUrl() {
        return Url::to( $this->getRoute() );
    }

    public function getLead() {
        return nl2br( trim( strip_tags( $this->excerpt ) ) );
    }
}
