<?php

namespace app\models\search;

use app\models\JuryCategory;
use app\models\Participant;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Work as WorkModel;
use yii\db\Query;

/**
 * Work represents the model behind the search form about `app\models\Work`.
 */
class Work extends WorkModel {

    public $has_marks;
    public $marksSum;

    public function attributes() {
        // add related fields to searchable attributes
        return array_merge( parent::attributes(),
            [ 'participant.smi_type', 'participant.type', 'participant.smi_name', 'participant.name',
            'participant.region'] );
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [ [ 'id', 'konkurs_id', 'user_id' ], 'integer' ],
            [
                [
                    'title',
                    'participant.smi_type',
                    'participant.type',
                    'participant.name',
                    'participant.region',
                    'has_marks',
                    'nomination',
                    'marksSum'
                ],
                'safe'
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        $result = [
            'id'           => 'ID',
            'user_id'      => 'Пользователь',
            'file_type'    => 'Тип файла',
            'title'        => 'Название',
            'picture'      => 'Логотип/Фото',
            'file'         => 'Файл',
            'picture_file' => 'Ваше фото',
        ];

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search( $params ) {
        $query = WorkModel::find();


        $query->select( [
            '{{work}}.*', // select all participant fields
            'SUM({{work_mark}}.mark) AS marksSum' // calculate works count
        ] )->joinWith( 'workMarks' )// ensure table junction
              ->groupBy( '{{work}}.id' ); // group the result to ensure aggregation function works

        $query->joinWith( 'participant' );

        $dataProvider = new ActiveDataProvider( [
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ] );


        $dataProvider->sort->attributes['marksSum'] = [
            'asc' => ['marksSum' => SORT_ASC],
            'desc' => ['marksSum' => SORT_DESC],
        ];

        $this->load( $params );

        if ( ! $this->validate() ) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $marks_query =
            ( new Query() )->select( 'work_id' )->distinct()->from( 'work_mark' );
        if ( \Yii::$app->user->can( 'jury' ) && \Yii::$app->user->identity->jury ) {
            $jury_cats = JuryCategory::find()->select( 'category_id AS id' )->where( [ 'jury_id' => \Yii::$app->user->identity->jury->id ] )->asArray()->all();

            $all_cats = [ ];
            foreach ( $jury_cats as $cat ) {
                $all_cats[] = $cat['id'];
            }
            $query->andWhere( [ 'work.nomination' => $all_cats ] );
/*            $query->andWhere( [
                'not in',
                'work.id',
                $marks_query
            ] );*/
        }

        if ( ! is_null( $this->has_marks ) ) {

            if ( $this->has_marks === "1" ) {
                $query->andWhere( [
                    'in',
                    'work.id',
                    $marks_query
                ] );
            } else if ( $this->has_marks === "0" ) {
                $query->andWhere( [
                    'not in',
                    'work.id',
                    $marks_query
                ] );
            }
        }

        if ( \Yii::$app->user->can( 'indexOwnWorks' ) ) {
            $query->andWhere( [ 'work.user_id' => \Yii::$app->user->id ] );
        }


        $query->andFilterWhere( [
            'work.id'                   => $this->id,
            'konkurs_id'           => $this->konkurs_id,
            'user_id'              => $this->user_id,
            'nomination'           => $this->nomination,
            'participant.smi_type' => $this['participant.smi_type'],
            'participant.type'     => $this['participant.type'],
        ] );
        if ($this->marksSum ) {
            $query->andHaving( [ '=', 'marksSum', $this->marksSum ] );
        } elseif ($this->marksSum === '0') {
            $query
                ->andHaving( [ '=', 'marksSum', $this->marksSum ] )
                ->orHaving([ 'is', 'marksSum', null]);
        }

        $query->andFilterWhere( [ 'like', 'file_name', $this->file_name ] )
              ->andFilterWhere( [ 'like', 'file_type', $this->file_type ] )
              ->andFilterWhere( [ 'like', 'title', $this->title ] )
              ->andFilterWhere( [ 'like', 'picture', $this->picture ] );

        $query
            ->andFilterWhere( [ 'like', 'participant.name', $this['participant.name'] ] )
            ->orFilterWhere( [ 'like', 'participant.smi_name', $this['participant.name'] ] );

/*        if (!$this->marksSum ) {
            $query->orderBy( 'id DESC' );
        }*/
        return $dataProvider;
    }
}
