<?php

namespace app\models\search;

use app\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Participant as ParticipantModel;

/**
 * User represents the model behind the search form about `app\models\User`.
 */
class Participant extends ParticipantModel {

    public $has_works;

    public function attributes() {
        // add related fields to searchable attributes
        return array_merge( parent::attributes(),
            [ 'user.email', ] );
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [ [ 'smi_type', 'type' ], 'integer' ],
            [ [ 'type', 'user.email', 'name', 'smi_name', 'region', 'has_works', ], 'safe' ],
            [ 'has_works', 'boolean' ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search( $params ) {
        $query = ParticipantModel::find();

        $dataProvider = new ActiveDataProvider( [
            'query' => $query,
        ] );

        //		 join with relation `author` that is a relation to the table `users`
        // and set the table alias to be `author`
//        $query->joinWith( 'user' );

        $query->select( [
            '{{participant}}.*', // select all participant fields
            'COUNT({{work}}.id) AS `worksCount`' // calculate works count
        ] )
              ->joinWith( [ 'works' ] )// ensure table junction
              ->groupBy( '{{participant}}.id' ); // group the result to ensure aggregation function works

        $this->load( $params );

        if ( ! $this->validate() ) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        /*
         *  * @property integer $id
 * @property integer $user_id
 * @property integer $konkurs_id
 * @property integer $type
 * @property string $name
 * @property string $smi_name
 * @property integer $smi_type
 * @property string $smi_url
 * @property string $smi_editor
 * @property string $smi_periodity
 * @property string $smi_field
 * @property string $smi_area
 * @property string $smi_status
 * @property string $smi_head
 * @property string $smi_jur_address
 * @property string $occupation
 * @property string $post_address
 * @property string $city_phone
 * @property string $mobile_phone
 * @property integer $add_time
         *
         */

        if ( $this->has_works === "1" ) {
            $query->andHaving( [ '>', 'worksCount', 0 ] );
        } else if ( $this->has_works === "0" ) {
            $query->andHaving( [ 'worksCount' => 0 ] );
        }

        $query->andFilterWhere( [
            'id'       => $this->id,
            'type'     => $this->type,
            'smi_type' => $this->smi_type,
            'region'   => $this->region,
        ] );

        $query->andWhere( [ 'user.status' => User::STATUS_ACTIVE ] );

        $query->andFilterWhere( [ 'like', 'smi_name', $this->smi_name ] );
        $query->andFilterWhere( [ 'like', 'user.email', $this['user.email'] ] );

//var_dump($query->sql);
        return $dataProvider;
    }
}
