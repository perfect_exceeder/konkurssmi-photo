<?php
/**
 * WorkForm.php
 * Created by h8every1 on 19.06.2015 13:45
 */

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Work form
 */
class WorkForm extends Model {

    public $title;
    public $date;
    public $file;
    public $picture;
    public $category_id;

    /**
     * @param null $category
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategory( $category = null ) {
        if ( ! $category ) {
            $category = $this->category_id ? $this->category_id : 1;
        }

        return WorkCategory::findOne( $category );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'file'        => 'Файл',
            'category_id' => 'Категория работы',
            'title'       => 'Название работы',
            'date'        => 'Дата выхода',
            'picture'     => 'Логотип/Фото',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [ [ 'title', 'date', 'file', 'picture', 'category_id' ], 'required' ],
            [ [ 'category_id' ], 'number', 'integerOnly' => true ],
            [
                [ 'file', 'picture' ],
                'file',
            ],
            [ [ 'file' ], 'file', ],
            [
                [ 'file' ],
                'file',
                'maxSize'    => $this->category->max_file_size * 1024 * 1024,
                'extensions' => $this->category->getClearFileExtensions(),
            ],
            [
                [ 'picture' ],
                'image',
                'extensions' => 'jpg, jpeg, jpe',
                'minSize'    => 1024 * 1024,
                'tooSmall'   => 'Размер файла должен быть не меньше 1 Мбайт.'
            ],
            [
                [ 'picture' ],
                \app\validators\ImageDimensionValidator::className(),
                'minDimension'   => 1000,
                'underDimension' => 'Одна из сторон изображения должна быть не меньше 1000 пикселей.'
            ]
        ];
    }


    /**
     * Save data from user
     */
    public function save() {
        if ( $this->validate() ) {

            $work                 = new Work();
            $work->konkurs_id     = 1;
            $work->participant_id = 2;
            $work->category_id    = $this->category_id;
            $work->file_name      = 'test';
            $work->file_type      = 'pdf';
            $work->date           = $this->date;
            $work->title          = $this->title;
            $work->picture        = 'test.jpg';

            return $work->save();
        }

        return false;
    }

}