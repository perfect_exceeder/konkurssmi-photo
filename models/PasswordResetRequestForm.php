<?php
namespace app\models;

use app\models\User;
use yii\base\Model;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model {
    public $email;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [ 'email', 'filter', 'filter' => 'trim' ],
            [ 'email', 'required' ],
            [ 'email', 'email' ],
            [
                'email',
                'exist',
                'targetClass' => '\app\models\User',
                'filter'      => [ 'status' => User::STATUS_ACTIVE ],
                'message'     => 'Пользователя с таким адресом у нас нет.'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => 'Адрес электронной почты',
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail() {
        /* @var $user User */
        $user = User::findOne( [
            'status' => User::STATUS_ACTIVE,
            'email'  => $this->email,
        ] );

        if ( $user ) {
            if ( ! User::isPasswordResetTokenValid( $user->password_reset_token ) ) {
                $user->generatePasswordResetToken();
            }

            if ( $user->save() ) {
                return \Yii::$app->mailer->compose( [
                    'html' => 'passwordResetToken-html',
                    'text' => 'passwordResetToken-text'
                ], [ 'user' => $user ] )
                                         ->setFrom( [ \Yii::$app->params['siteEmail'] => \Yii::$app->name ] )
                                         ->setReplyTo( \Yii::$app->params['adminEmail'] )
                                         ->setTo( $this->email )
                                         ->setSubject( 'Восстановление пароля на сайте «' . \Yii::$app->name. '»' )
                                         ->send();
            }
        }

        return false;
    }
}