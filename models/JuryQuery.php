<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Jury]].
 *
 * @see Jury
 */
class JuryQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Jury[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Jury|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function byUserId($user_id) {
        $this->andWhere(['user_id'=>$user_id])->limit(1);

        Return $this;
    }

}