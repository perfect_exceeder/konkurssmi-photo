<?php
namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $activation_key
 * @property string $email
 * @property integer $status
 * @property integer $group
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Jury[] $jury
 * @property Article[] $articles
 * @property Participant[] $participant
 */
class User extends ActiveRecord implements IdentityInterface {


    const STATUS_BANNED = 0;
    const STATUS_INACTIVE = 10;
    const STATUS_ACTIVE = 20;

    const GROUP_ADMIN = 0;
    const GROUP_JURY = 1;
    const GROUP_PARTICIPANT = 2;
    const GROUP_MANAGER = 3;

    private $_statuses = [ ];
    private $_groups = [ ];

    private $_name = '';

    public function init() {
        $this->_statuses = [
            self::STATUS_ACTIVE   => 'Активен',
            self::STATUS_INACTIVE => 'Неактивен (требуется подтверждение почтового адреса)',
            self::STATUS_BANNED   => 'Забанен',
        ];
        $this->_groups   = [
            self::GROUP_ADMIN       => 'Администратор',
            self::GROUP_MANAGER     => 'Контент-менеджер',
            self::GROUP_JURY        => 'Член жюри',
            self::GROUP_PARTICIPANT => 'Участник конкурса',
        ];

        parent::init();
    }


    public function getName() {

        if ( $this->_name == '' ) {
            switch ( $this->group ) {
                case self::GROUP_PARTICIPANT:
                    $this->_name = $this->participant->name;
                    break;
                case self::GROUP_JURY:
                    $this->_name = $this->jury->name;
                    break;
                default:
                    $this->name = null;
            }
        }

        return $this->_name;
    }

    public function setName( $name ) {
        $this->_name = $name;
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [ 'status', 'default', 'value' => self::STATUS_ACTIVE ],
            //[ 'status', 'in', 'range' => [ self::STATUS_BANNED, self::STATUS_ACTIVE ] ],
            [ [ 'auth_key', 'password_hash', 'email' ], 'required' ],
            [ [ 'status', 'group' ], 'integer' ],
            [ [ 'auth_key', 'activation_key' ], 'string', 'max' => 32 ],
            [ [ 'activation_key' ], 'default', 'value' => null ],
            [ [ 'password_hash', 'password_reset_token', 'email' ], 'string', 'max' => 255 ],
            [ 'name', 'string', 'on' => 'admin' ],
            [ 'email', 'email' ],
            [
                'email',
                'unique',
            ],
            [ 'email', 'checkSelfEmail' ],
        ];
    }

    public function checkSelfEmail( $attribute, $params ) {
        $model = User::findByEmail( $this->$attribute );

        if ( $model && $model->id !== $this->id ) {
            $this->addError( $attribute, 'Этот e-mail уже используется.' );
        };
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity( $id ) {
        return static::findOne( [ 'id' => $id, 'status' => [ self::STATUS_ACTIVE, self::STATUS_INACTIVE ] ] );
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken( $token, $type = null ) {
        throw new NotSupportedException( '"findIdentityByAccessToken" is not implemented.' );
    }

    /**
     * Finds user by email address
     *
     * @param string $email
     *
     * @return static|null
     */
    public static function findByEmail( $email ) {
        return static::findOne( [ 'email' => $email, 'status' => [ self::STATUS_ACTIVE, self::STATUS_INACTIVE ] ] );
    }

    /**
     * Finds user by activation_key
     *
     * @param string $key
     *
     * @return static|null
     */
    public static function findByActivationKey( $key ) {
        return static::findOne( [ 'activation_key' => $key, 'status' => self::STATUS_INACTIVE ] );
    }

    /**
     * Makes user active and allows to use the site
     */
    public function activateUser() {
        $this->activation_key = null;
        $this->status         = self::STATUS_ACTIVE;
    }

    /**
     * Marks user as banned
     */
    public function banUser() {
        $this->status = self::STATUS_BANNED;
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     *
     * @return static|null
     */
    public static function findByPasswordResetToken( $token ) {
        if ( ! static::isPasswordResetTokenValid( $token ) ) {
            return null;
        }

        return static::findOne( [
            'password_reset_token' => $token,
            'status'               => self::STATUS_ACTIVE,
        ] );
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     *
     * @return boolean
     */
    public static function isPasswordResetTokenValid( $token ) {
        if ( empty( $token ) ) {
            return false;
        }
        $expire    = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts     = explode( '_', $token );
        $timestamp = (int) end( $parts );

        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId() {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey() {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey( $authKey ) {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     *
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword( $password ) {
        return Yii::$app->security->validatePassword( $password, $this->password_hash );
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword( $password ) {
        $this->password_hash = Yii::$app->security->generatePasswordHash( $password );
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey() {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates activationc code which is sent in emai
     */
    public function setUserInactive() {
        $this->activation_key = Yii::$app->security->generateRandomString( 10 );
        $this->status         = self::STATUS_INACTIVE;
        $this->sendConfirmEmail();
    }

    public function sendConfirmEmail() {

        $msg_data = [
            'site_name'   => \Yii::$app->name,
            'site_url'   => Url::to( [ 'site/index' ], true ),
            'code'       => $this->activation_key,
            'verify_url' => Url::to( [ 'site/verify', 'activation_key' => $this->activation_key ], true ),
        ];

        $msg = <<<HTML
Здравствуйте!
Ваш e-mail был указан при регистрации на сайте конкурса "{$msg_data['site_name']}" ({$msg_data['site_url']})
Чтобы активировать аккаунт, введите код <strong>{$msg_data['code']}</strong> на странице для ввода кода или просто перейдите по этой ссылке:
<a href="{$msg_data['verify_url']}">{$msg_data['verify_url']}</a>.
Если вы не регистрировались на сайте конкурса, просто проигнорируйте это сообщение.
HTML;


        return Yii::$app->mailer->compose()
                                ->setFrom( [ Yii::$app->params['siteEmail'] => Yii::$app->name ] )
                                ->setReplyTo( Yii::$app->params['adminEmail'] )
                                ->setTo( $this->email )
                                ->setSubject( 'Подтверждение регистрации аккаунта на сайте конкурса ПРО-Образование' )
                                ->setHtmlBody( nl2br( $msg ) )
                                ->send();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken() {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken() {
        $this->password_reset_token = null;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id'             => 'ID',
            'password'       => 'Пароль',
            'activation_key' => 'Ключ активации',
            'email'          => 'E-mail',
            'status'         => 'Статус',
            'group'          => 'Группа',
            'created_at'     => 'Дата регистрации',
            'updated_at'     => 'Дата изменения',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJury() {
        return $this->hasOne( Jury::className(), [ 'user_id' => 'id' ] )->inverseOf( 'user' );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles() {
        return $this->hasMany( Article::className(), [ 'created_by' => 'id' ] );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParticipant() {
        return $this->hasOne( Participant::className(), [ 'user_id' => 'id' ] );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorks() {
        return $this->hasMany( Work::className(), [ 'user_id' => 'id' ] );
    }

    public function countWorks() {
        return $this->getWorks()->count();
    }

    /**
     * Password field for editing users
     *
     * @return string
     */
    public function getPassword() {
        $password  = '';
        $post_data = Yii::$app->request->post( $this->formName() );
        if ( isset( $post_data['password'] ) ) {
            $password = $post_data['password'];
        }

        return $password;
    }

    public function isActivated() {
        return $this->status == self::STATUS_ACTIVE;
    }

    public function beforeValidate() {
        if ( $this->isNewRecord ) {
            $this->generateAuthKey();
//            $this->setUserInactive();
        }

        if ( $this->password ) {
            $this->setPassword( $this->password );
        }

        if ( $this->status == self::STATUS_ACTIVE ) {
            $this->activation_key = null;
        }

        return parent::beforeValidate();
    }

    public function afterSave( $insert, $changedAttributes ) {

        if ( $this->scenario == 'admin' ) {
            // create participant or jury depending on $this->group

            switch ( $this->group ) {
                case self::GROUP_PARTICIPANT:
                    if ( $insert ) {
                        $participant               = new Participant();
                        $participant->user_id      = $this->id;
                        $participant->konkurs_id   = 1;
                        $participant->type         = Participant::TYPE_JOURNALIST;
                        $participant->smi_name     = '';
                        $participant->smi_type     = 101;
                        $participant->smi_url      = '';
                        $participant->occupation   = '';
                        $participant->post_address = '';
                        $participant->city_phone   = '';
                        $participant->mobile_phone = '';
                    } else {
                        $participant = $this->participant;
                    }
                    break;
                case self::GROUP_JURY:
                    if ( $insert ) {
                        $jury             = new Jury();
                        $jury->user_id    = $this->id;
                        $jury->konkurs_id = 1;
                    } else {
                        $jury = $this->jury;
                    }
                    break;
            }


            switch ( $this->group ) {
                case self::GROUP_JURY:
                    $jury->name = $this->name;
                    $jury->save( false );
                    break;
                case self::GROUP_PARTICIPANT:
                    $participant->scenario = 'admin';
                    $participant->name     = $this->name;
                    $participant->getNameParts();
                    $participant->save( false );
                    break;
            }
        }


        return parent::afterSave( $insert, $changedAttributes );
    }


    public function getGroups() {
        return $this->_groups;
    }

    public function getStatuses() {
        return $this->_statuses;
    }

    public function getGroupName() {
        return $this->_groups[ $this->group ];
    }

    public function getStatusName() {
        return $this->_statuses[ $this->status ];
    }

}