<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Article]].
 *
 * @see Article
 */
class ArticleQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    public function bySlug($slug) {
        $this->andWhere('[[slug]]=:slug', [':slug' => $slug]);
        return $this;
    }

    public function andCategory($category) {
        switch ($category) {
            case 'works':
                $cat_id = Article::CATEGORY_WORKS;
                break;
            case 'news':
                $cat_id = Article::CATEGORY_NEWS;
                break;
            case 'expert':
                $cat_id = Article::CATEGORY_EXPERT;
                break;
            default:
                $cat_id = Article::CATEGORY_PAGES;

        }

        $this->andWhere('[[category]]=:cat_id', [':cat_id' => $cat_id]);
        return $this;
    }

    /**
     * @inheritdoc
     * @return Article[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Article|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}