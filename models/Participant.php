<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "participant".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $konkurs_id
 * @property integer $type
 * @property string $name
 * @property string $smi_name
 * @property integer $smi_type
 * @property string $smi_url
 * @property string $smi_editor
 * @property string $smi_periodity
 * @property string $smi_field
 * @property string $smi_area
 * @property string $smi_status
 * @property string $smi_head
 * @property integer $region
 * @property string $smi_jur_address
 * @property string $occupation
 * @property string $post_address
 * @property string $city_phone
 * @property string $mobile_phone
 * @property integer $add_time
 *
 * @property Konkurs $konkurs
 * @property User $user
 */
class Participant extends \yii\db\ActiveRecord {

    const TYPE_JOURNALIST = 1;
    const TYPE_COMPANY = 2;


    public $name1;
    public $name2;
    public $name3;
    public $password;
    public $password2;
    public $rememberMe = true;

    private $_email;

    public function init() {
        parent::init();
    }

    public function behaviors() {
        return [
            [
                'class'              => TimestampBehavior::className(),
                'createdAtAttribute' => 'add_time',
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'participant';
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        $scenarios                    = parent::scenarios();
        $scenarios['change_password'] = [ 'password', 'password2' ];
        $scenarios['admin']           = [ 'name' ];

        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [
                [
                    'name1',
                    'name2',
                    'name3',
                    'smi_name',
                    'smi_type',
                    'smi_url',
                    'occupation',
                    'post_address',
                    'city_phone',
                    'mobile_phone',
                    'region'
                ],
                'required'
            ],
            [
                [
                    'smi_area',
                    'smi_editor',
                    'smi_head',
                    'smi_jur_address',
                ],
                'required',
                'when'       => function ( $model ) {
                    /* @var $model Participant */
                    return $model->type == Participant::TYPE_COMPANY;

                },
                'whenClient' => "function (attribute, value) {
	return $('#participant_type').val() == " . Participant::TYPE_COMPANY . ";
}"
            ],
            [ [ 'smi_type', 'type', 'region' ], 'integer' ],
            [ [ 'smi_editor', 'smi_area', 'smi_jur_address', 'post_address', 'smi_field' ], 'string' ],
            [
                [ 'name1', 'name2', 'name3', 'smi_name', 'smi_periodity', 'smi_status', 'smi_head', 'occupation' ],
                'string',
                'max' => 200
            ],
            [ 'smi_url', 'string', 'max' => 255 ],
            [ 'smi_url', 'url', 'defaultScheme' => 'http', 'enableIDN' => true ],
            [ [ 'city_phone', 'mobile_phone' ], 'string', 'max' => 50 ],
            [ 'password', 'string', 'min' => 6 ],
            [
                [ 'password', 'password2' ],
                'required',
                'on' => $this->registrationScenarios()
            ],
            [ [ 'mobile_phone', 'city_phone' ], 'checkPhone' ],
            [ 'password2', 'compare', 'compareAttribute' => 'password' ],
            [ 'email', 'email' ],
            [
                'email',
                'unique',
                'targetClass'     => User::className(),
                'targetAttribute' => 'email',
                'on'              => $this->registrationScenarios()
            ],
            [ 'region', 'compare', 'compareValue' => '00', 'operator' => '!=', 'message'=>'Пожалуйста, укажите регион.' ],
        ];
    }

    private function registrationScenarios() {
        return [ 'register' . self::TYPE_COMPANY, 'register' . self::TYPE_JOURNALIST ];
    }

    public function checkPhone( $attribute, $params ) {
        $phone = $this->clearPhone( $this->$attribute );
        if ( strlen( $phone ) !== 11 ) {
            $this->addError( $attribute, 'Некорректный формат номера телефона.' );
        };
    }

    public function checkSelfEmail( $attribute, $params ) {
        $model = User::findByEmail( $this->$attribute );

        if ( $model && $model->id !== $this->user->id ) {
            $this->addError( $attribute, 'Этот e-mail уже используется.' );
        };
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        $result = [
            'name1'             => 'Фамилия',
            'name2'             => 'Имя',
            'name3'             => 'Отчество',
            'name'              => 'ФИО',
            'smi_name'          => 'Название СМИ',
            'type'              => 'Тип участника',
            'smi_type'          => 'Тип СМИ',
            'smi_type_readable' => 'Тип СМИ',
            'smi_url'           => 'Адрес СМИ в Интернете',
            'occupation'        => 'Должность в СМИ',
            'post_address'      => 'Почтовый адрес',
            'mobile_phone'      => 'Мобильный телефон',
            'city_phone'        => 'Городской телефон',
            'email'             => 'Электронная почта',
            'region'            => 'Регион',
            'password'          => 'Пароль',
            'password2'         => 'Повтор пароля',
            // company fields
            'smi_field'         => 'Направленность издания',
            'smi_periodity'     => 'Периодичность выхода',
            'smi_area'          => 'Распространение (вещание)',
            'smi_status'        => 'Статус',
            'smi_head'          => 'Учредитель',
            'smi_editor'        => 'Главный редактор (ФИО)',
            'smi_jur_address'   => 'Юридический адрес издания',
            'add_time'          => 'Дата регистрации',
        ];

        if ( $this->type == self::TYPE_COMPANY ) {
            $result['name']         = 'ФИО представителя';
            $result['name1']        = 'Фамилия представителя';
            $result['occupation']   = 'Должность в СМИ делегированного представителя';
            $result['mobile_phone'] = 'Мобильный телефон представителя';
            $result['city_phone']   = 'Городской телефон издания';

        }

        return $result;
    }

    /**
     * @return array
     */
    public function attributeHints() {
        return [
            "post_address"    => "страна, город, улица, номер дома, номер корпуса, номер строения, номер офиса, номер квартиры/кабинета, почтовый индекс.",
            "smi_jur_address" => "страна, город, улица, номер дома, номер корпуса, номер строения, номер офиса, номер квартиры/кабинета, почтовый индекс.",
            "city_phone"      => "+7 XXX XXX XXXX",
            "mobile_phone"    => "+7 9XX XXX XXXX",
            'smi_field'       => 'например общественно-политическое, отраслевое и т.п.',
            'smi_head'        => 'Название или ФИО',
            'password2'       => 'для безопасности',
        ];
    }

    /**
     * @return array
     */
    public static function getSmiTypes() {
        return [
            101 => 'газета',
            102 => 'журнал',
            103 => 'интернет-издание',
            200 => 'информационное агентство',
            300 => 'радиостанция',
            400 => 'ТВ-канал'
        ];
    }

    /**
     * @return array
     */
    public static function getSmiStatuses() {
        return [
            'федеральное',
            'региональное',
            'областное',
            'городское',
            'районное'
        ];
    }

    /**
     * @param mixed $user_type
     */
    public function setUserType( $user_type = null ) {
        $user_type = (int) $user_type;
        if ( ! in_array( $user_type, [ self::TYPE_COMPANY, self::TYPE_JOURNALIST ] ) ) {
            $user_type = self::TYPE_JOURNALIST;
        }
        $this->type = $user_type;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKonkurs() {
        return $this->hasOne( Konkurs::className(), [ 'id' => 'konkurs_id' ] );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne( User::className(), [ 'id' => 'user_id' ] );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorks() {
        return $this->hasMany( Work::className(), [ 'user_id' => 'id' ] )->viaTable( '{{%user}}',
            [ 'id' => 'user_id' ] );
    }

    /**
     * @inheritdoc
     * @return ParticipantQuery the active query used by this AR class.
     */
    public static function find() {
        return new ParticipantQuery( get_called_class() );
    }

    /**
     * @inheritdoc
     */
    public function afterFind() {
//        var_dump($this);
        if ( $this->name ) {
            $this->getNameParts();
        }

        if ( ! $this->smi_url ) {
            $this->smi_url = 'http://';
        }

        parent::afterFind();

    }

    public function beforeSave( $insert ) {


        if ( parent::beforeSave( $insert ) ) {

            if ( $this->scenario == 'admin' ) {
                return true;
            }
            // registering a user
            if ( $insert ) {
                $user = new User();
            } else {
                $user = $this->user;
            }

//            $user->load( Yii::$app->request->post() );

            if ( \Yii::$app->request->post( $this->formName() )['email'] ) {
                $user->email = \Yii::$app->request->post( $this->formName() )['email'];
            }

            if ( $this->password ) {
                $user->setPassword( $this->password );
            }


            if ( $insert ) {
                $user->group = $user::GROUP_PARTICIPANT;
                $user->generateAuthKey();
                $user->setUserInactive();

                #TODO: query active konkurs
                $this->konkurs_id = 1;
            }

            if ( ! $user->validate() ) {
                return false;
            }
            $user->save();

            if ( $insert ) {
                $this->user_id = $user->id;
            }


            $this->mobile_phone = $this->formatPhone( $this->clearPhone( $this->mobile_phone ) );
            $this->city_phone   = $this->formatPhone( $this->clearPhone( $this->city_phone ) );
            $this->setNameFromParts();

            return true;
        }

        return false;

    }

    public function setNameFromParts() {
        $this->name = $this->name1 . ' ' . $this->name2 . ' ' . $this->name3;
    }

    public function getNameParts() {
        $parts       = explode( ' ', $this->name );
        $this->name1 = $parts[0];
        $this->name2 = $parts[1];
        $this->name3 = $parts[2];
    }

    public function getEmail() {
        $email = '';
        if ( $this->user_id ) {
            $email = $this->user->email;
        } else if ( $this->_email ) {
            $email = $this->_email;
        }

        return $email;
    }

    public function setEmail( $email ) {
        $this->_email = $email;

        return $email;
    }


    public function clearPhone( $phone ) {
        return preg_replace( '/[^0-9]/', '', $phone );
    }

    private function formatPhone( $phone ) {
        return '+7 ' . $phone[1] . $phone[2] . $phone[3] . ' ' . $phone[4] . $phone[5] . $phone[6] .
               ' ' . $phone[7] . $phone[8] . $phone[9] . $phone[10];

    }

    public function getParticipant_Type() {
        return $this->participantTypes()[ $this->type ];
    }

    public static function participantTypes() {
        return [
            self::TYPE_JOURNALIST => 'Журналист',
            self::TYPE_COMPANY    => 'СМИ',
        ];
    }

    public function getSmi_type_readable() {
        $types = \app\helpers\ArrayHelper::flatten( self::getSmiTypes() );

        return isset ( $types[ $this->smi_type ] ) ? $types[ $this->smi_type ] : '---';
    }

    public function getWork_category() {
        return - $this->smi_type[0];
    }

    public static function getRegions() {
        return [
            '00' => 'Не указан',
            '01' => '01 - Республика Адыгея',
            '02' => '02 - Республика Башкортостан',
            '03' => '03 - Республика Бурятия',
            '04' => '04 - Республика Алтай',
            '05' => '05 - Республика Дагестан',
            '06' => '06 - Республика Ингушетия',
            '07' => '07 - Кабардино-Балкарская Республика',
            '08' => '08 - Республика Калмыкия',
            '09' => '09 - Карачаево-Черкесская Республика',
            '10' => '10 - Республика Карелия',
            '11' => '11 - Республика Коми',
            '12' => '12 - Республика Марий Эл',
            '13' => '13 - Республика Мордовия',
            '14' => '14 - Республика Саха (Якутия)',
            '15' => '15 - Республика Северная Осетия - Алания',
            '16' => '16 - Республика Татарстан',
            '17' => '17 - Республика Тыва',
            '18' => '18 - Удмуртская Республика',
            '19' => '19 - Республика Хакасия',
            '21' => '21 - Чувашская Республика',
            '22' => '22 - Алтайский край',
            '23' => '23 - Краснодарский край',
            '24' => '24 - Красноярский край',
            '25' => '25 - Приморский край',
            '26' => '26 - Ставропольский край',
            '27' => '27 - Хабаровский край',
            '28' => '28 - Амурская область',
            '29' => '29 - Архангельская область',
            '30' => '30 - Астраханская область',
            '31' => '31 - Белгородская область',
            '32' => '32 - Брянская область',
            '33' => '33 - Владимирская область',
            '34' => '34 - Волгоградская область',
            '35' => '35 - Вологодская область',
            '36' => '36 - Воронежская область',
            '37' => '37 - Ивановская область',
            '38' => '38 - Иркутская область',
            '39' => '39 - Калининградская область',
            '40' => '40 - Калужская область',
            '41' => '41 - Камчатский край',
            '42' => '42 - Кемеровская область',
            '43' => '43 - Кировская область',
            '44' => '44 - Костромская область',
            '45' => '45 - Курганская область',
            '46' => '46 - Курская область',
            '47' => '46 - Ленинградская область',
            '48' => '47 - Липецкая область',
            '49' => '48 - Магаданская область',
            '50' => '50 - Московская область',
            '51' => '51 - Мурманская область',
            '52' => '52 - Нижегородская область',
            '53' => '53 - Новгородская область',
            '54' => '54 - Новосибирская область',
            '55' => '55 - Омская область',
            '56' => '56 - Оренбургская область',
            '57' => '57 - Орловская область',
            '58' => '58 - Пензенская область',
            '59' => '59 - Пермский край',
            '60' => '60 - Псковская область',
            '61' => '61 - Ростовская область',
            '62' => '62 - Рязанская область',
            '63' => '63 - Самарская область',
            '64' => '64 - Саратовская область',
            '65' => '65 - Сахалинская область',
            '66' => '66 - Свердловская область',
            '67' => '67 - Смоленская область',
            '68' => '68 - Тамбовская область',
            '69' => '69 - Тверская область',
            '70' => '70 - Томская область',
            '71' => '71 - Тульская область',
            '72' => '72 - Тюменская область',
            '73' => '73 - Ульяновская область',
            '74' => '74 - Челябинская область',
            '75' => '75 - Забайкальский край',
            '76' => '76 - Ярославская область',
            '77' => '77 - Москва',
            '78' => '78 - Санкт-Петербург',
            '79' => '79 - Еврейская автономная область',
            '82' => '82 - Республика Крым',
            '83' => '83 - Ненецкий автономный округ',
            '86' => '86 - Ханты-Мансийский автономный округ - Югра',
            '87' => '87 - Чукотский автономный округ',
            '89' => '89 - Ямало-Ненецкий автономный округ',
            '92' => '92 - город Севастополь',
            '95' => '95 - Чеченская Республика',
        ];
    }

    public static function getRegionsWithCode() {
        $result = [ ];

        foreach ( self::getRegions() as $code => $name ) {
            $result[ $code ] = $name;
        }

        return $result;
    }

}
