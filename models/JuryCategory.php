<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jury_category".
 *
 * @property integer $jury_id
 * @property integer $category_id
 *
 * @property Jury $jury
 */
class JuryCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jury_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jury_id', 'category_id'], 'required'],
            [['jury_id', 'category_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'jury_id' => 'Jury ID',
            'category_id' => 'Category ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJury()
    {
        return $this->hasOne(Jury::className(), ['id' => 'jury_id']);
    }
}
