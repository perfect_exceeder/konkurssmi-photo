<?php
namespace app\models;

/**
 * This is the ActiveQuery class for [[Work]].
 *
 * @see Work
 */
class WorkQuery extends \yii\db\ActiveQuery {
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Work[]|array
     */
    public function all( $db = null ) {
        return parent::all( $db );
    }

    /**
     * @inheritdoc
     * @return Work|array|null
     */
    public function one( $db = null ) {
        return parent::one( $db );
    }

    public function not_loaded() {
        $this->andWhere( '[[on_remote]]=0' );

        return $this;
    }

    public function byWorkType($type) {
        $all_types = Work::getWorkTypes();
        if (!isset($all_types[$type])) {
            $type = Work::TYPE_PRINT;
        }

        $this->andWhere(['in', 'file_type', $all_types[$type]]);
        return $this;
    }

    public function in_nomination($nomination_id = 0) {
        $this->andWhere( '[[nomination]]=:nomination', [':nomination' => $nomination_id]);

        return $this;
    }

}