<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "work_mark".
 *
 * @property integer $work_id
 * @property integer $user_id
 * @property integer $mark_category_id
 * @property integer $mark
 *
 * @property User $user
 * @property MarkCategory $markCategory
 * @property Work $work
 */
class WorkMark extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'work_mark';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['work_id', 'user_id', 'mark_category_id', 'mark'], 'required'],
            [['work_id', 'user_id', 'mark_category_id', 'mark'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'work_id' => 'Work ID',
            'user_id' => 'User ID',
            'mark_category_id' => 'Mark Category ID',
            'mark' => 'Mark',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJury()
    {
        return $this->hasOne(Jury::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarkCategory()
    {
        return $this->hasOne(MarkCategory::className(), ['id' => 'mark_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWork()
    {
        return $this->hasOne(Work::className(), ['id' => 'work_id']);
    }
}
