<?php

namespace app\models;

use app\models\search\Participant;
//use Imagine\Exception\Exception;
use Exception;
use Yii;
use yii\base\DynamicModel;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\imagine\Image;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * This is the model class for table "work".
 *
 * @property integer $id
 * @property integer $konkurs_id
 * @property integer $user_id
 * @property integer $nomination
 * @property string $file_name
 * @property string $file_type
 * @property string $title
 * @property string $picture
 * @property string $logo
 *
 * @property Konkurs $konkurs
 * @property Participant $participant
 * @property User $user
 * @property WorkMark[] $workMarks
 */
class Work extends \yii\db\ActiveRecord {

    const TYPE_PRINT = 1;
    const TYPE_RADIO = 2;
    const TYPE_TV = 3;

    public $file;
    public $picture_file;
    public $logo_file;


    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'work';
    }


    /**
     * @inheritdoc
     */

    public function scenarios() {
        $scenarios           = parent::scenarios();
        $scenarios['add']    = [ 'title', 'file', 'picture_file', 'logo_file', 'nomination', ];
        $scenarios['update'] = [ 'title', 'file', 'picture_file', 'logo_file', 'nomination', ];

        return $scenarios;
    }

    public function rules() {
        $rules = [
            [
                [ 'title', 'nomination' ],
                'required'
            ],
            [ [ 'konkurs_id', 'user_id', 'nomination' ], 'integer' ],
            [
                [ 'file' ],
                'image',
                'skipOnEmpty' => false,
                'maxSize'     => 8 * 1024 * 1024, // 8 Mb
                'extensions' => 'jpg, jpeg, jpe',
                'when'        => function ( $model ) {
                    return empty( $model->file_name );
                },
            ],
            [
                [ 'picture_file', 'logo_file' ],
                'image',
                'extensions' => 'jpg, jpeg, jpe',
                'maxSize'    => 3 * 1024 * 1024,
            ],
            [
                'logo_file',
                'image',
                'skipOnEmpty' => false,
                'when'        => function ( $model ) {
                    return empty( $model->logo );
                },
            ],
            [
                'picture_file',
                'image',
                'skipOnEmpty' => false,
                'when'        =>
                    function ( $model ) {
                        /** @var $model Work */
                        return  empty( $model->picture );
                    }
            ],
            [
                [ 'picture_file', 'logo_file' ],
                \app\validators\ImageDimensionValidator::className(),
                'minDimension'   => 500,
                'underDimension' => 'Одна из сторон изображения должна быть не меньше 500 пикселей.',
            ],
            [
                [ 'file' ],
                \app\validators\ImageDimensionValidator::className(),
                'minDimension'   => 1000,
                'underDimension' => 'Одна из сторон изображения должна быть не меньше 3000 пикселей.',
            ],
            [ [ 'title' ], 'string' ]
        ];


        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        $result = [
            'id'                   => 'ID',
            'konkurs_id'           => 'Конкурс',
            'user_id'              => 'Участник',
            'nomination'           => 'Номинация',
            'file_name'            => 'Файл работы',
            'file_type'            => 'Тип файла',
            'title'                => 'Описание',
            'file'                 => 'Файл',
            'picture'              => 'Фото',
            'logo'                 => 'Лого',
            'picture_file'         => 'Ваша фотография',
            'logo_file'            => 'Логотип СМИ',
        ];

        if ( Yii::$app->user->identity && Yii::$app->user->identity->participant !== Participant::TYPE_JOURNALIST ) {
            $result['picture_file'] = 'Фотография заявителя';
        }

        return $result;
    }

    public function attributeHints() {
        return [
            'title' => 'Укажите название СМИ, в котором было опубликовано фото и дату публикации. Для интернет-СМИ укажите адресс на ссылку с материалом',
            'file'         => 'Формат JPG, размер файла не более 8 Мбайт, разрешение не менее 300 dpi, размер по длинной стороне не менее 1000 пикселей',
            'picture_file' => 'Формат JPG, размер файла не более 3 Мбайт, размер по длинной стороне не менее 500 пикселей',
            'logo_file'    => 'Формат JPG, размер файла не более 3 Мбайт, размер по длинной стороне не менее 500 пикселей',
        ];
    }


    public function beforeValidate() {

        if ( $this->scenario == 'add' ) {

            if ( ! $this->user_id ) {
                $user = new User;
                if ( Yii::$app->user->identity->group == User::GROUP_PARTICIPANT ) {
                    $user = $user->findIdentity( Yii::$app->user->id );
                } else if ( Yii::$app->user->can( 'editWork' ) && Yii::$app->request->get( 'user_id' ) ) {
                    $user = $user->findIdentity( (int) Yii::$app->request->get( 'user_id' ) );
                }

                if ( ! $user ) {
                    throw new NotFoundHttpException( 'The requested page does not exist.' );
                }
                $this->user_id = $user->id;
            }

            #TODO: populate with active Konkurs ID
            $this->konkurs_id = 1;

        }

            $this->file = UploadedFile::getInstances( $this, 'file' );

            if ( isset( $this->file[0] ) ) {
                $this->file = $this->file[0];
                if ( ! $this->file_name ) {
                    $this->file_name = trim( Yii::$app->security->generateRandomString(), '_-' );
                }
                $this->file_type = $this->file->extension;
            } else {
                $this->file = null;
            }

        $this->createFileName( 'picture' );
        $this->createFileName( 'logo' );


        return parent::beforeValidate();
    }

    /**
     * Creates a filename for uploaded file and populates the field.
     *
     * @param $attr
     *
     * @return bool
     */
    private function createFileName( $attr ) {
        $attr_file = $attr . '_file';

        $this->$attr_file = UploadedFile::getInstances( $this, $attr_file );

        if ( is_array( $this->$attr_file ) && isset( $this->{$attr_file}[0] ) ) {
            $this->$attr_file = $this->{$attr_file}[0];
            $this->$attr      = Yii::$app->security->generateRandomString( 15 ) . '.' . $this->$attr_file->extension;

            return true;
        } else {
            $this->$attr_file = null;
        }

        return false;
    }

    public function afterValidate() {
        parent::afterValidate();
    }

    public function afterSave( $insert, $changedAttributes ) {
        $path = Yii::getAlias( '@webroot/uploads/works/' . $this->user_id . '/' );
        FileHelper::createDirectory( $path );
        FileHelper::createDirectory( $path. '/thumb/' );

        if ( $this->file ) {
            $file_path = $path . $this->file_name . '.' . $this->file_type;
            $thumb_path = $path .'/thumb/'. $this->file_name . '.' . $this->file_type;

            $this->file->saveAs( $file_path );

            try {
                $image = Image::thumbnail( $file_path, 200, 200, 'inset' )
                              ->save( $thumb_path, [ 'quality' => 51 ] );
            } catch ( Exception $e ) {
                $image = false;
            }



            if ( isset( $changedAttributes['file_type'] ) ) {
                $oldFilePath = $path . $this->file_name . '.' . $changedAttributes['file_type'];
                if ( file_exists( $oldFilePath ) ) {
                    unlink( $oldFilePath );
                }
            }
        }

        if ( $this->picture_file ) {
            $path = Yii::getAlias( '@webroot/uploads/picture/' );
            FileHelper::createDirectory( $path );
            FileHelper::createDirectory( $path . '/thumb/' );

            $this->picture_file->saveAs( $path . $this->picture );

            if ( isset( $changedAttributes['picture'] ) ) {
                $oldFilePath = $path . $changedAttributes['picture'];
                if ( file_exists( $oldFilePath ) ) {
                    unlink( $oldFilePath );
                }
            }
        }

        if ( $this->logo_file ) {
            $path = Yii::getAlias( '@webroot/uploads/logo/' );
            FileHelper::createDirectory( $path );
            FileHelper::createDirectory( $path . '/thumb/' );

            $this->logo_file->saveAs( $path . $this->logo );
            if ( isset( $changedAttributes['logo'] ) ) {
                $oldFilePath = $path . $changedAttributes['logo'];
                if ( file_exists( $oldFilePath ) ) {
                    unlink( $oldFilePath );
                }
            }
        }


        return parent::afterSave( $insert, $changedAttributes );

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKonkurs() {
        return $this->hasOne( Konkurs::className(), [ 'id' => 'konkurs_id' ] );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne( User::className(), [ 'id' => 'user_id' ] );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParticipant() {
        return $this->hasOne( Participant::className(), [ 'user_id' => 'user_id' ] );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory() {
        $this->participant->smi_type;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkMarks() {
        return $this->hasMany( WorkMark::className(), [ 'work_id' => 'id' ] );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthorWorkMarks( $user_id ) {
        return $this->hasMany( WorkMark::className(), [ 'work_id' => 'id' ] )
                    ->where( 'user_id = :user_id', [ ':user_id' => $user_id ] );
    }

    public function deleteAllFiles() {
        $files = [
            $this->getFilePath(),
            $this->getFileThumbPath(),
            $this->_filePath('picture'),
            $this->_filePath('logo'),
            $this->_thumbPath('picture'),
            $this->_thumbPath('logo'),

        ];

        foreach ($files as $file) {
            if (file_exists($file) && is_writeable($file)) {
                unlink( $file );
            }
        }

        $this->file_name = '';

        return true;

    }

    /**
     * @inheritdoc
     * @return WorkQuery the active query used by this AR class.
     */
    public static function find() {
        return new WorkQuery( get_called_class() );
    }

    public function getMarked() {
//        $marks = $this->getAuthorWorkMarks( \Yii::$app->user->id )->count();
        $marks = $this->getWorkMarks()->count();

        return $marks;
    }

    public function getMarksSum() {
//        $marks = $this->getAuthorWorkMarks( \Yii::$app->user->id )->count();
        $marks = $this->getWorkMarks()->sum('mark');
        if (!$marks) {
            $marks = 0;
        }

        return $marks;
    }

    public function getFileUrl() {
        return Url::to( Yii::getAlias( '@web/uploads/works/' . $this->user_id . '/' . $this->file_name . '.' . $this->file_type ),
            true );
    }


    public function getFileThumbUrl() {
        return Url::to( Yii::getAlias( '@web/uploads/works/' . $this->user_id . '/thumb/' . $this->file_name . '.' . $this->file_type ),
            true );
    }

    public function getAdditionalFileUrl($type) {
        return Url::to( Yii::getAlias( '@web/uploads/works/' . $this->user_id . '/' . $this->$type ),
            true );
    }

    public function getAdditionalFilePath($type) {
        return Url::to( Yii::getAlias( '@webroot/uploads/works/' . $this->user_id . '/' . $this->$type ),
            true );
    }

    public function getFilePath() {
        return Yii::getAlias( '@webroot/uploads/works/' . $this->user_id . '/' . $this->file_name . '.' . $this->file_type );
    }

    public function getFileThumbPath() {
        return Yii::getAlias( '@webroot/uploads/works/' . $this->user_id . '/thumb/' . $this->file_name . '.' . $this->file_type );
    }

    public function getPictureUrl() {
        return $this->_fileUrl( 'picture' );
    }

    public function getPicturePath() {
        return $this->_filePath( 'picture' );
    }

    public function getLogoUrl() {
        return $this->_fileUrl( 'logo' );
    }

    public function getLogoPath() {
        return $this->_filePath( 'logo' );
    }

    private function _fileUrl( $attr ) {
        return Url::to( Yii::getAlias( '@web/uploads/' . $attr . '/' . $this->$attr ), true );
    }

    private function _filePath( $attr ) {
        return Yii::getAlias( '@webroot/uploads/' . $attr . '/' . $this->$attr );
    }

    private function _thumbPath( $attr ) {
        return Yii::getAlias( '@webroot/uploads/' . $attr . '/thumb/' . $this->$attr );
    }

    private function _thumbUrl( $attr ) {

        if ( ! $this->$attr ) {
            return Url::to( Yii::getAlias( '@web/images/blank.gif' ), true );
        }

        if ( ! file_exists( $this->_thumbPath( $attr ) ) ) {
            $this->_createThumb( $attr );
        }

        return
            Url::to( Yii::getAlias( '@web/uploads/' . $attr . '/thumb/' . $this->$attr ), true );
    }

    public function getPictureThumbUrl() {
        return $this->_thumbUrl( 'picture' );
    }

    public function getLogoThumbUrl() {
        return $this->_thumbUrl( 'logo' );
    }

    private function _createThumb( $attr ) {
        if ( ! file_exists( $this->_filePath( $attr ) ) ) {
            return false;
        }

        try {
            $image = Image::thumbnail( $this->_filePath( $attr ), 200, 200, 'inset' )
                          ->save( $this->_thumbPath( $attr ), [ 'quality' => 51 ] );
        } catch ( Exception $e ) {
//            echo 'Выброшено исключение: ', $e->getMessage(), "\n";
            $image = false;
        }


        return $image;

    }

    public function getRemoteLink() {
        $method = null;
        $result = '#';

        switch ( $this->workType ) {
            case self::TYPE_PRINT:
            case self::TYPE_RADIO:
                break;
            case self::TYPE_TV:
                $method = 'youtube';
                break;
        }

        if ( $method == 'youtube' ) {
            $result = 'http://www.youtube.com/watch?v=' . $this->remote_id;
        }

        return $result;
    }

    public function getWorkType() {
        foreach ( self::getWorkTypes() as $type => $extensions ) {
            if ( in_array( $this->file_type, $extensions ) ) {
                return $type;
            }
        }

        return self::TYPE_PRINT;
    }

    public static function getWorkTypes() {
        return [
            self::TYPE_PRINT => [ 'doc', 'docx', 'pdf', 'odt', 'rtf', ],
            self::TYPE_RADIO => [ 'wav', 'mp3', 'wav', 'ogg' ],
            self::TYPE_TV    => [ 'avi', 'flv', 'mp4', 'wmv', 'mpg' ],
        ];
    }

    public static function getWorkTypesDropdown() {
        return [
            self::TYPE_PRINT => 'Печатные',
            self::TYPE_RADIO => 'Звук',
            self::TYPE_TV    => 'Видео',
        ];
    }

    public function getNominations() {

        $nominations_type = 'all';
        if ( ! $this->isNewRecord ) {
            $user_id          = $this->user_id;
            $nominations_type = $this->participant->type;
            $smi_type = $this->participant->smi_type;
        } elseif ( ! Yii::$app->user->can( 'editWork' ) ) {
            $user_id          = Yii::$app->user->id;
            $nominations_type = Yii::$app->user->identity->participant->type;
            $smi_type = Yii::$app->user->identity->participant->smi_type;

        }

        $nominations = static::allNominations();

        if ( $nominations_type != 'all' ) {

            $previous_works = Work::find()->select(['nomination',"count( 'nomination' ) as nom_count"])->where( [ 'user_id' => $user_id])->having(['>=','nom_count', 10 ] )->groupBy('nomination');

            if ( ! $this->isNewRecord ) {
                $previous_works = $previous_works->andWhere( 'id != ' . $this->id );
            }
            $previous_works = $previous_works->asArray()->all();

            foreach ( $previous_works as $previous_work ) {
                $limited = (int) $previous_work['nomination'];
                if ($limited !== $this->nomination && !empty($nominations[$limited])) {
                    unset( $nominations[ $limited ] );
                }
            }
        }

        return $nominations;
    }

    public static function allNominations() {
        $nominations = [
            101 => 'Дошкольное образование',
            102 => 'Общее образование',
            103 => 'Среднее профессиональное образование',
            104 => 'Высшее образование',
            105 => 'Дополнительное образование',
            106 => 'Технологии и механизмы образования',
            107 => 'Подготовка кадров и повышение квалификации',
            108 => 'Оценка качества образования',
        ];

        return $nominations;
    }

    public static function allNominationsCombined() {
        return self::allNominations();
    }

    public function getNominationName() {
        return self::allNominationsCombined()[ $this->nomination ];
    }

    public static function getNominationsDescriptions() {
        $nominations = [
            101 => 'Фотографии, посвященные жизни детских садов и других дошкольных образовательных учреждений',
            102 => 'Фото о жизни школы, образовательном процессе, учениках и преподавателях',
            103 => 'Учебный процесс в колледжах: получение профессиональных навыков, практика на производстве',
            104 => 'Жизнь вузов: учеба и внеучебная деятельность, студенческие праздники, конкурсы, исследования, научные конференции',
            105 => 'Работа преподавателей и наставников, успехи и достижения учащихся кружков и специальных курсов',
            106 => 'Образовательная инфраструктура: здания, интерьеры, учебное оборудование, лаборатории, методические комплексы',
            107 => 'Самосовершенствование и переподготовка специалистов всех уровней: фото учебного процесса, портреты участников',
            108 => 'Подготовка и проведение тестов, контрольных работ, экзаменов',
        ];

        return $nominations;
    }

    public function nominationsBySmiType() {
        /** @see Participant::getSmiTypes() */
        /*return [
            101 => 'газета',
            102 => 'журнал',
            103 => 'интернет-издание',
            200 => 'информационное агентство',
            300 => 'радиостанция',
            400 => 'ТВ-канал'
        ];*/

        return [
            101 => [
                101,
                105,
                108,
                109,
                201,
                205,
                206
            ],
            102 => [
                101,
                105,
                108,
                109,
                201,
                205,
                206
            ],
            103 => [
                101,
                105,
                108,
                109,
                201,
                205,
                206
            ],
            200 => [
                102,
                108,
                109,
                202,
                205,
                206
            ],
            300 => [
                104,
                107,
                108,
                109,
                204,
                205,
                206,
            ],
            400 => [
                103,
                106,
                108,
                109,
                203,
                205,
                206,
            ],
        ];
    }

    public function getYoutubeId() {
        $result = $this->remote_id;

        if ( ! $result ) {
            $regExp  = "/v=([a-zA-Z0-9\_\-]+)&?/";
            $matches = [ ];
            preg_match( $regExp, $this->download_url, $matches );
            if ( count( $matches ) && ! empty( $matches[1] ) ) {
                $result = $matches[1];
            }
        }

        return $result;
    }

    /**
     * Sends an email when a work is published.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail() {
        /* @var $user User */
        $user = $this->user;

        if ( $user ) {
            return \Yii::$app->mailer->compose( [
                'html' => 'workAdd-html',
                'text' => 'workAdd-text'
            ], [ 'work'=>$this ] )
                                     ->setFrom( [ \Yii::$app->params['siteEmail'] => \Yii::$app->name ] )
                                     ->setReplyTo( \Yii::$app->params['adminEmail'] )
                                     ->setTo( $user->email )
                                     ->setSubject( '«' . \Yii::$app->name . '»: Работа добавлена' )
                                     ->send();
        }

        return false;
    }

    public function getActualParticipant() {
        return !empty( $this->participant ) ? $this->participant : ( \Yii::$app->user->identity->participant ? \Yii::$app->user->identity->participant : false );
    }
}
