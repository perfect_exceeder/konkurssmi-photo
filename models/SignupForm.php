<?php
namespace app\models;

use app\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model {
    public $email;
    public $group;
    public $password;
    public $activation_key;

    public function scenarios() {
        return [
            'activate' => [ 'activation_key' ],
            'signup'   => [ 'email', 'group', 'password' ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [ [ 'email', 'group', 'password', 'activation_key' ], 'required' ],
            [ 'email', 'filter', 'filter' => 'trim' ],
            [ 'email', 'email', ],
            [
                'email',
                'unique',
                'targetClass' => '\app\models\User',
                'message'     => 'Пользователь с таким адресом e-mail уже существует'
            ],
            [ 'password', 'string', 'min' => 6 ],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup() {
        if ( $this->validate() ) {
            $user        = new User();
            $user->email = $this->email;
            $user->group = $this->group;
            $user->setPassword( $this->password );
            $user->generateAuthKey();
            $user->setUserInactive();

            $user->save( false );

            return $user;
        }

        return null;
    }


    /**
     * Activates user.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function activate() {
        if ( $this->validate() && ( $user = User::findByActivationKey( $this->activation_key ) ) ) {
            $user->activateUser();
            $user->save( false );

            return $user;
        }

        return null;
    }

    public function attributeLabels()
    {
        return [
            'email' => 'E-mail',
            'password' => 'Пароль',
            'group' => 'Группа',
            'activation_key' => 'Ключ активации',
        ];
    }

    public function formName() {
        return '';
    }
}