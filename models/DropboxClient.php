<?php
/**
 * Dropbox.php
 * Created by h8every1 on 20.06.2015 13:33
 */


namespace app\models;

use Yii;
use \Dropbox as dbx;
use yii\base\Object;

class DropboxClient extends Object {

    public $app_secret;
    public $app_token;
    public $link;
    private $_client;

    public function __construct( $app_secret = null, $app_token = null, $config = [] ) {
        parent::__construct($config);

        if (!$app_secret && !empty(Yii::$app->params['dropbox_secret'])) {
            $app_secret = Yii::$app->params['dropbox_secret'];
        }
        $this->app_secret = $app_secret;

        if (!$app_token && !empty(Yii::$app->params['dropbox_token'])) {
            $app_token = Yii::$app->params['dropbox_token'];
        }
        $this->app_token = $app_token;

        $this->_client= new dbx\Client($this->app_token, "PRO-Education/1.0", 'ru');
    }

    public function getToken() {
        return $this-_app_token;
    }

    public function getAccountInfo() {
        return $this->_client->getAccountInfo();
    }

    public function saveFile($file, $filename) {
        $f = fopen($file, "rb");

        $filename = $this->normalizeFilename($filename);

        $result = $this->_client->uploadFile($filename, dbx\WriteMode::add(), $f);
        fclose($f);
        return $result;
    }

    public function getMedia($filename) {
        return $this->_client->createTemporaryDirectLink($this->normalizeFilename($filename));
    }

    private function normalizeFilename($filename) {
        return '/'.rtrim($filename,'/');
    }
}