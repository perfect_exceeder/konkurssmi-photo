<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "konkurs".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 *
 * @property Jury[] $juries
 * @property Participant[] $participants
 * @property Work[] $works
 */
class Konkurs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'konkurs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'description' => 'Описание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJuries()
    {
        return $this->hasMany(Jury::className(), ['konkurs_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParticipants()
    {
        return $this->hasMany(Participant::className(), ['konkurs_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorks()
    {
        return $this->hasMany(Work::className(), ['konkurs_id' => 'id']);
    }
}
