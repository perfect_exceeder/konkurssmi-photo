<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mark_category".
 *
 * @property integer $id
 * @property string $name
 * @property integer $participant_type
 *
 * @property WorkMark[] $workMarks
 */
class MarkCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mark_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'participant_type' => 'Participant Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkMarks()
    {
        return $this->hasMany(WorkMark::className(), ['mark_category_id' => 'id']);
    }
}
