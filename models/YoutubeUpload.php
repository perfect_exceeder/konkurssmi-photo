<?php
/**
 * YoutubeUpload.php
 * Created by h8every1 on 30.06.2015 0:37
 */

namespace app\models;

use Google_Service_YouTube_PlaylistItem;
use Google_Service_YouTube_PlaylistItemSnippet;
use Google_Service_YouTube_ResourceId;
use Yii;
use Google_Client;
use Google_Service_YouTube;
use Google_Exception;
use Google_Http_MediaFileUpload;
use Google_Service_Exception;
use Google_Service_YouTube_Video;
use Google_Service_YouTube_VideoSnippet;
use Google_Service_YouTube_VideoStatus;
use yii\base\Object;


class YoutubeUpload extends Object {

    private static $_client_id;
    private static $_client_secret;
    private static $_token_path;
    private static $_redirect;
    private static $_client;
    private static $_youtube;

    public function __construct($options=[], $args=[]) {

        if (empty($options['client_id']) && !empty(\Yii::$app->params['youtube']['client_id'])) {
            $options['client_id'] = \Yii::$app->params['youtube']['client_id'];
        }
        if (empty($options['client_secret']) && !empty(\Yii::$app->params['youtube']['client_secret'])) {
            $options['client_secret'] = \Yii::$app->params['youtube']['client_secret'];
        }
        if (empty($options['token_path']) && !empty(\Yii::$app->params['youtube']['token_path'])) {
            $options['token_path'] = \Yii::$app->params['youtube']['token_path'];
        }
        if (empty($options['redirect']) && !empty(\Yii::$app->params['youtube']['redirect'])) {
            $options['redirect'] = \Yii::$app->params['youtube']['redirect'];
        }

        self::$_client_id = $options['client_id'];
        self::$_client_secret = $options['client_secret'];
        self::$_token_path = $options['token_path'];
        self::$_redirect = $options['redirect'];

        return parent::__construct($args);
    }

    public function init() {
        self::getClient();
        self::setAccessToken();

        return self::$_client;
    }

    public static function getClient() {
        if ( self::$_client === null ) {
            self::$_client = new Google_Client();
            self::$_client->setClientId( self::$_client_id );
            self::$_client->setClientSecret( self::$_client_secret );
            self::$_client->setScopes( 'https://www.googleapis.com/auth/youtube' );
            $redirect = filter_var( self::$_redirect, FILTER_SANITIZE_URL );
            self::$_client->setRedirectUri( $redirect );
            self::$_client->setAccessType( 'offline' );
            self::$_youtube = new Google_Service_YouTube( self::$_client );
        }

        return self::$_client;
    }

    public static function getAuthUrl() {
        $client = self::getClient();

        return $client->createAuthUrl();
    }

    public static function getAccessTokenByCode( $code = null ) {
        if ( ! $code ) {
            return false;
        }
        $client = self::getClient();
        $client->authenticate( $code );
        $token = $client->getAccessToken();
        self::saveAccessToken();

        return $token;
    }

    private static function setAccessToken() {
        $client = self::getClient();
        $token  = self::loadAccessToken();
        $client->setAccessToken( $token );
        if ( $client->isAccessTokenExpired() ) {
            $token_arr = get_object_vars( json_decode( $token ) );
            $client->refreshToken( $token_arr['refresh_token'] );
            self::saveAccessToken();
        }
    }

    public static function showAccessToken() {
        $client = self::getClient();
        var_dump( $client->getAccessToken() );
    }

    public static function saveAccessToken() {
        return file_put_contents( Yii::getAlias( self::$_token_path ), self::$_client->getAccessToken() );
    }

    public static function loadAccessToken() {
        return file_get_contents( Yii::getAlias( self::$_token_path ) );
    }

    public static function uploadVideo( $data ) {
        $client = self::getClient();
        // Define an object that will be used to make all API requests.
        $youtube = self::$_youtube;
        $result = ['error'=>0];
        try {
            $videoPath = $data['path'];
            // Create a snippet with title, description, tags and category ID
            // Create an asset resource and set its snippet metadata and type.
            $snippet = new Google_Service_YouTube_VideoSnippet();
            $snippet->setTitle( $data['title'] );
            $snippet->setDescription( $data['description'] );
// $snippet->setTags(array("tag1", "tag2"));

            // Numeric video category. See
            // https://developers.google.com/youtube/v3/docs/videoCategories/list
            $snippet->setCategoryId( "22" );
            // Set the video's status to "public". Valid statuses are "public",
            // "private" and "unlisted".
            $status                = new Google_Service_YouTube_VideoStatus();
            $status->privacyStatus = "unlisted";
            // Associate the snippet and status objects with a new video resource.
            $video = new Google_Service_YouTube_Video();
            $video->setSnippet( $snippet );
            $video->setStatus( $status );
            // Specify the size of each chunk of data, in bytes. Set a higher value for
            // reliable connection as fewer chunks lead to faster uploads. Set a lower
            // value for better recovery on less reliable connections.
            $chunkSizeBytes = 2 * 1024 * 1024;
            // Setting the defer flag to true tells the client to return a request which can be called
            // with ->execute(); instead of making the API call immediately.
            $client->setDefer( true );
            // Create a request for the API's videos.insert method to create and upload the video.
            $insertRequest = $youtube->videos->insert( "status,snippet", $video );
            // Create a MediaFileUpload object for resumable uploads.
            $media = new Google_Http_MediaFileUpload(
                $client,
                $insertRequest,
                'video/*',
                null,
                true,
                $chunkSizeBytes
            );
            $media->setFileSize( filesize( $videoPath ) );
            // Read the media file and upload it chunk by chunk.
            $status = false;
            $handle = fopen( $videoPath, "rb" );
            while ( ! $status && ! feof( $handle ) ) {
                $chunk  = fread( $handle, $chunkSizeBytes );
                $status = $media->nextChunk( $chunk );
            }
            fclose( $handle );
            // If you want to make other calls after the file upload, set setDefer back to false
            $client->setDefer( false );
            $result['id'] = $status['id'];
        } catch ( Google_Service_Exception $e ) {
            $result['error'] = 1;
            $result['error_msg'] = htmlspecialchars( $e->getMessage() );
        } catch ( Google_Exception $e ) {
            $result['error'] = 2;
            $result['error_msg'] = htmlspecialchars( $e->getMessage() );
        }

        return $result;
    }



    public static function addVideoToPlaylist($video_id, $playlist_id) {
        $client = self::getClient();
        // Define an object that will be used to make all API requests.
        $youtube = self::$_youtube;
        $result = ['error'=>0];
        try {
            // 5. Add a video to the playlist. First, define the resource being added
            // to the playlist by setting its video ID and kind.
            $resourceId = new Google_Service_YouTube_ResourceId();
            $resourceId->setVideoId($video_id);
            $resourceId->setKind('youtube#video');
            // Then define a snippet for the playlist item. Set the playlist item's
            // title if you want to display a different value than the title of the
            // video being added. Add the resource ID and the playlist ID retrieved
            // in step 4 to the snippet as well.
            $playlistItemSnippet = new Google_Service_YouTube_PlaylistItemSnippet();
            $playlistItemSnippet->setTitle('First video in the test playlist');
            $playlistItemSnippet->setPlaylistId($playlist_id);
            $playlistItemSnippet->setResourceId($resourceId);
            // Finally, create a playlistItem resource and add the snippet to the
            // resource, then call the playlistItems.insert method to add the playlist
            // item.
            $playlistItem = new Google_Service_YouTube_PlaylistItem();
            $playlistItem->setSnippet($playlistItemSnippet);
            $playlistItemResponse = $youtube->playlistItems->insert(
                'snippet,contentDetails', $playlistItem, array());
            $result['id'] = $playlistItemResponse['id'];
        } catch (Google_Service_Exception $e) {
            $result['error'] = 1;
            $result['error_msg'] = htmlspecialchars( $e->getMessage() );
        } catch (Google_Exception $e) {
            $result['error'] = 2;
            $result['error_msg'] = htmlspecialchars( $e->getMessage() );
        }

        return $result;
    }

}