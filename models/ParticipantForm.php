<?php
/**
 * ParticipantForm.php
 * Created by h8every1 on 21.06.2015 1:18
 */

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Work form
 */
class ParticipantForm extends Model {

    const TYPE_JOURNALIST = 1;
    const TYPE_COMPANY = 2;

    /**
     * Type of registered user
     *
     * @var integer
     */
    private $_user_type;

    // used both for journalist and companies
    public $id;
    /**
     * @var string
     */
    public $name1;
    /**
     * @var string
     */
    public $name2;
    /**
     * @var string
     */
    public $name3;
    /**
     * @var string
     */
    public $smi_name;
    /**
     * @var integer
     */
    public $smi_type;
    /**
     * @var string
     */
    public $smi_url;
    /**
     * @var string
     */
    public $occupation;
    /**
     * @var string
     */
    public $post_address;
    /**
     * @var string
     */
    public $city_phone;
    /**
     * @var string
     */
    public $mobile_phone;
    /**
     * @var string
     */
    public $email;
    /**
     * @var string
     */
    public $password;
    /**
     * @var string
     */
    public $password2;

    // used only for companies

    /**
     * @var string
     */
    public $smi_field;
    /**
     * @var string
     */
    public $smi_periodity;
    /**
     * @var string
     */
    public $smi_area;
    /**
     * @var string
     */
    public $smi_jur_address;
    /**
     * @var integer
     */
    public $smi_status;
    /**
     * @var string
     */
    public $smi_head;
    /**
     * @var string
     */
    public $smi_editor;

    /**
     * @inheritdoc
     */
    public function scenarios() {
        $scenarios          = parent::scenarios();
        $scenarios['login'] = [ 'email', 'password' ];

        return $scenarios;
    }

    public function init() {
        $this->smi_url = 'http://';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [
                [
                    'name1',
                    'name2',
                    'name3',
                    'smi_name',
                    'smi_type',
//                    'smi_url',
                    'occupation',
                    'post_address',
                    'city_phone',
                    'mobile_phone',
                ],
                'required'
            ],
            [ [ 'smi_type' ], 'integer' ],
            [ [ 'smi_editor', 'smi_area', 'smi_jur_address', 'post_address', 'smi_field' ], 'string' ],
            [
                [ 'name1', 'name2', 'name3', 'smi_name', 'smi_periodity', 'smi_status', 'smi_head', 'occupation' ],
                'string',
                'max' => 200
            ],
            [ 'smi_url', 'string', 'max' => 255 ],
            [ 'smi_url', 'url' ],
            [ 'email', 'email' ],
            ['email','unique','targetClass'=>User::className(), 'targetAttribute'=>'email'],
            [ [ 'city_phone', 'mobile_phone' ], 'string', 'max' => 50 ],
            [ 'password', 'string', 'min' => 6 ],
            [
                [ 'password', 'password2' ],
                'required',
                'on' => [ 'register' . self::TYPE_COMPANY, 'register' . self::TYPE_JOURNALIST ]
            ],
            [['mobile_phone','city_phone'],'checkPhone'],
            [ 'password2', 'compare', 'compareAttribute' => 'password' ],
        ];
    }

    public function checkPhone($attribute, $params) {
        $phone = $this->clearPhone($this->$attribute);
        if (strlen($phone) !== 11) {
            $this->addError($attribute, 'Некорректный формат номера телефона.');
        };
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        $result = [
            'name1'           => 'Фамилия',
            'name2'           => 'Имя',
            'name3'           => 'Отчество',
            'smi_name'        => 'Название СМИ',
            'smi_type'        => 'Тип СМИ',
            'smi_url'         => 'Адрес СМИ в Интернете',
            'occupation'      => 'Должность в СМИ',
            'post_address'    => 'Почтовый адрес',
            'mobile_phone'    => 'Мобильный телефон',
            'city_phone'      => 'Городской телефон',
            'email'           => 'E-mail',
            'password'        => 'Пароль',
            'password2'       => 'Повтор пароля',
            // company fields
            'smi_field'       => 'Направленность издания',
            'smi_periodity'   => 'Периодичность выхода',
            'smi_area'        => 'Распространение (вещание)',
            'smi_status'      => 'Статус',
            'smi_head'        => 'Учредитель',
            'smi_editor'      => 'Главный редактор (ФИО)',
            'smi_jur_address' => 'Юридический адрес издания',
        ];

        if ( $this->getUserType() == self::TYPE_COMPANY ) {
            $result['name1']        = 'Фамилия представителя';
            $result['occupation']   = 'Должность в СМИ делегированного представителя';
            $result['mobile_phone'] = 'Мобильный телефон представителя';
            $result['city_phone']   = 'Городской телефон издания';

        }

        return $result;
    }

    /**
     * @return array
     */
    public function attributeHints() {
        return [
            "post_address"    => "страна, город, улица, номер дома, номер корпуса, номер строения, номер офиса, номер квартиры/кабинета, почтовый индекс.",
            "smi_jur_address" => "страна, город, улица, номер дома, номер корпуса, номер строения, номер офиса, номер квартиры/кабинета, почтовый индекс.",
            "city_phone"    => "+7 9XX XXX XXXX",
            "mobile_phone"    => "+7 9XX XXX XXXX",
            'smi_field'       => 'например общественно-политическое, отраслевое и т.п.',
            'smi_head'        => 'Название или ФИО',
            'password2'       => 'для безопасности',
        ];
    }

    /**
     * @return array
     */
    public function getSmiTypes() {
        return [
            1 => 'печатное (газета, журнал, интернет-издание)',
            2 => 'информационное агентство',
            3 => 'радиостанция',
            4 => 'ТВ-канал'
        ];
    }

    /**
     * @return array
     */
    public function getSmiStatuses() {
        return [
            'федеральное',
            'региональное',
            'областное',
            'городское',
            'районное'
        ];
    }

    /**
     * @return mixed
     */
    public function getUserType() {
        if ( ! $this->_user_type ) {
            $this->setUserType();
        }

        return $this->_user_type;
    }

    /**
     * @param mixed $user_type
     */
    public function setUserType( $user_type = null ) {
        $user_type = (int) $user_type;
        if ( ! in_array( $user_type, [ self::TYPE_COMPANY, self::TYPE_JOURNALIST ] ) ) {
            $user_type = self::TYPE_JOURNALIST;
        }
        $this->_user_type = $user_type;
    }

    public function save() {
        if ( $this->validate() ) {

            $user        = new User();
            $participant = new Participant();


            $user->email = $this->email;
            $user->group = $user::GROUP_PARTICIPANT;

            $user->setPassword( $this->password );
            $user->generateAuthKey();
            $user->setUserInactive();

            $user->save( false );

            $participant->user_id = $user->id;
            #TODO: query active konkurs
            $participant->konkurs_id = 1;
            $participant->type = $this->getUserType();
            $participant->name = $this->makeNameForParticipant();
            $participant->smi_name = $this->smi_name;
            $participant->smi_type = $this->smi_type;
            $participant->smi_url = $this->smi_url;
            $participant->smi_editor = $this->smi_editor;
            $participant->smi_periodity = $this->smi_periodity;
            $participant->smi_field = $this->smi_field;
            $participant->smi_area = $this->smi_area;
            $participant->smi_status = $this->smi_status;
            $participant->smi_head = $this->smi_head;
            $participant->smi_jur_address = $this->smi_jur_address;
            $participant->occupation = $this->occupation;
            $participant->post_address = $this->post_address;
            $participant->city_phone = $this->clearPhone($this->city_phone);
            $participant->mobile_phone = $this->clearPhone($this->mobile_phone);

            $participant->save(false);
            $this->id = $participant->id;

            return $participant;
        }

        return false;
    }

    public function makeNameForParticipant() {
        return $this->name1.' '.$this->name2.' '.$this->name3;
    }

    private function makeNameFromParticipant($name) {
        $parts = explode(' ',$name);
        $this->name1 = $parts[0];
        $this->name2 = $parts[1];
        $this->name3 = $parts[2];
    }




    public function clearPhone($phone) {
        return preg_replace('/[^0-9]/','',$phone);
    }

    public function findOne($id) {

        if (($participant = Participant::findOne($id)) !== null) {
            $this->id = $participant->id;
            $this->makeNameFromParticipant($participant->name);
            $this->setUserType($participant->type);

            $this->smi_name = $participant->smi_name;
            $this->smi_type = $participant->smi_type;
            $this->smi_url = $participant->smi_url;
            $this->smi_editor = $participant->smi_editor;
            $this->smi_periodity = $participant->smi_periodity;
            $this->smi_field = $participant->smi_field;
            $this->smi_area = $participant->smi_area;
            $this->smi_status = $participant->smi_status;
            $this->smi_head = $participant->smi_head;
            $this->smi_jur_address = $participant->smi_jur_address;
            $this->occupation = $participant->occupation;
            $this->post_address = $participant->post_address;
            $this->city_phone = $participant->city_phone;
            $this->mobile_phone = $participant->mobile_phone;

            return $this;
        }

        return null;
    }


}